from invoke import task


@task
def smoke_tests(c):
    c.run("pytest -v -m smoke --alluredir=test_result_62_release")


@task
def delete(c):
    c.run("pytest tests/delete_advert_test_data.py")


@task
def vouchers(c):
    c.run("pytest tests/create_vouchers_api.py")


@task
def redeem(c):
    c.run("pytest redeem_all_vouchers_in_store.py")
