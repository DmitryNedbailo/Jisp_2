from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class RetailerBundleListPage(BasePage):
    def open_create_bundle_page(self):
        self.click_create_button()

    def click_create_button(self):
        create_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(locators.CREATE_BUNDLE_BUTTON))
        create_button.click()
