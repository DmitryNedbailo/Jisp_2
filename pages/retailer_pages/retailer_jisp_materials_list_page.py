from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class RetailerJispMaterialsListPage(BasePage):
    def click_first_print_materials_button(self):
        first_print_materials_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.FIRST_PRINT_MATERIALS_BUTTON))
        first_print_materials_button.click()
