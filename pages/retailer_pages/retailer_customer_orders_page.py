import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class RetailerCustomerOrdersPage(BasePage):
    def redeem_the_voucher(self):
        self.click_scan_save_tab()
        self.input_scan_save_voucher_code_field()
        self.click_scan_save_validate_code_button()

    def click_scan_save_tab(self):
        scan_save_tab = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CUSTOMER_ORDERS_SCAN_SAVE_TAB))
        scan_save_tab.click()

    def input_scan_save_voucher_code_field(self):
        scan_save_voucher_code_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CUSTOMER_ORDERS_SCAN_SAVE_VOUCHER_CODE_INPUT_FIELD))
        scan_save_voucher_code_field.send_keys()

    def click_scan_save_validate_code_button(self):
        scan_save_validate_code_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CUSTOMER_ORDERS_SCAN_SAVE_VALIDATE_CODE_BUTTON))
        scan_save_validate_code_button.click()
        time.sleep(15)
