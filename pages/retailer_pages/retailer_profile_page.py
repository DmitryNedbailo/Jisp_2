import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class RetailerProfilePage(BasePage):
    def open_customer_orders_tab(self):
        self.click_customer_orders_tab()

    def open_retailer_offer_list_page(self):
        self.click_my_store_tab()
        self.click_my_store_offers_button()

    def open_retailer_bundle_list_page(self):
        self.click_my_store_tab()
        self.click_my_store_bundle_button()

    def open_retailer_voucher_list_page(self):
        self.click_my_store_tab()
        self.click_my_store_voucher_button()

    def open_jisp_materials_tab(self):
        self.click_jisp_materials_tab()

    def open_billing_methods_page(self):
        self.click_billing_methods_button()

    def open_retailer_campaign_list_page(self):
        self.click_my_store_tab()
        self.click_my_store_campaign_button()

    def open_retailer_printer_page(self):
        self.click_my_store_tab()
        self.click_my_store_printer_button()

    def open_retailer_sales_settings_page(self):
        self.click_my_store_tab()
        self.click_my_store_sales_settings()

    def open_edit_profile_page(self):
        self.click_edit_retailer_profile_button()

    def open_basket_page(self):
        self.click_basket_button()

    def open_transaction_invoices_page(self):
        self.click_profile_button()
        self.click_invoicing_and_billing_button()
        time.sleep(0.5)

    def open_ar_invoices_page(self):
        self.click_ar_invoices_button()
        time.sleep(0.5)

    def click_my_store_tab(self):
        my_store_tab = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(locators.MY_STORE_TAB))
        my_store_tab.click()

    def click_profile_button(self):
        profile_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(locators.PROFILE_BUTTON))
        profile_button.click()

    def click_my_store_voucher_button(self):
        my_store_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.MY_STORE_VOUCHER_BUTTON))
        my_store_voucher_button.click()

    def click_jisp_materials_tab(self):
        jisp_materials_tab = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.JISP_MATERIALS_TAB))
        jisp_materials_tab.click()

    def click_billing_methods_button(self):
        billing_methods_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.BILLING_METHODS_BUTTON))
        billing_methods_button.click()

    def click_my_store_offers_button(self):
        my_store_offers_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.MY_STORE_OFFERS_BUTTON))
        my_store_offers_button.click()

    def click_customer_orders_tab(self):
        customer_orders_tab = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CUSTOMER_ORDERS_TAB))
        customer_orders_tab.click()
        time.sleep(5)

    def click_my_store_campaign_button(self):
        my_store_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.MY_STORE_CAMPAIGN_BUTTON))
        my_store_campaign_button.click()

    def click_edit_retailer_profile_button(self):
        edit_retailer_profile_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.EDIT_PROFILE_BUTTON))
        edit_retailer_profile_button.click()

    def click_my_store_bundle_button(self):
        my_store_bundle_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.MY_STORE_BUNDLE_BUTTON))
        my_store_bundle_button.click()

    def click_my_store_printer_button(self):
        my_store_printer_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.MY_STORE_PRINTER_BUTTON))
        my_store_printer_button.click()

    def click_my_store_sales_settings(self):
        my_store_sales_settings = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.MY_STORE_SALES_SETTINGS_BUTTON))
        my_store_sales_settings.click()

    def click_basket_button(self):
        basket_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(locators.BASKET_BUTTON))
        basket_button.click()
        time.sleep(0.1)

    def click_invoicing_and_billing_button(self):
        invoicing_and_billing_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.INVOICING_AND_BILLING_BUTTON))
        invoicing_and_billing_button.click()

    def click_ar_invoices_button(self):
        ar_invoices_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.AR_INVOICES_BUTTON))
        ar_invoices_button.click()
