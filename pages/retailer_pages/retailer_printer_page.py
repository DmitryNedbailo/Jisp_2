from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class RetailerPrinterPage(BasePage):
    def open_add_printer_page(self):
        self.click_add_printer_button()

    def click_add_printer_button(self):
        add_printer_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADD_PRINTER_BUTTON))
        add_printer_button.click()
