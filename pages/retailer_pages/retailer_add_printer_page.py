from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class RetailerAddPrinterPage(BasePage):
    def add_printer(self, path=None, access_id=cs.PRINTER_ACCESS_ID):
        self.input_printer_path_field(path)
        self.input_printer_access_id_field(access_id)
        self.click_printer_save_button()

    def input_printer_path_field(self, path):
        printer_path_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.PRINTER_PATH_FIELD))
        printer_path_field.send_keys(path)

    def input_printer_access_id_field(self, access_id):
        printer_access_id_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.PRINTER_ACCESS_ID_FIELD))
        printer_access_id_field.send_keys(access_id)

    def click_printer_save_button(self):
        printer_save_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.PRINTER_SAVE_BUTTON))
        printer_save_button.click()
