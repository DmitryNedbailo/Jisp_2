import configparser

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import constants as cs
from tests import locators

config = configparser.ConfigParser()
config.read('config.ini')


class RetailerMakeACampaignPage(BasePage):
    def create_new_retailer_offer_campaign(
            self,
            barcode=config.get(cs.ENVIRONMENT, 'BARCODE_1'),
            product_name=cs.CAMPAIGN_PRODUCT_NAME,
            offer_text=cs.DESCRIPTION,
            original_price=cs.CAMPAIGN_ORIGINAL_PRICE,
            offer_price=cs.CAMPAIGN_OFFER_PRICE
    ):
        self.click_specify_your_order_button()
        self.click_save_xxp_button()
        self.input_campaign_product_name_field(product_name)
        self.input_campaign_custom_offer_text_field(offer_text)
        self.input_campaign_original_price_field(original_price)
        self.input_campaign_offer_price_field(offer_price)
        self.input_campaign_barcode_field(barcode)
        self.click_campaign_continue_button()
        self.click_launch_campaign_button()
        self.click_back_to_my_campaign_button()

    def create_new_retailer_event_campaign(
            self,
            product_name=cs.CAMPAIGN_PRODUCT_NAME,
            start_date=cs.CAMPAIGN_EVENT_START_DATE,
            end_date=cs.CAMPAIGN_EVENT_END_DATE,
            offer_text=cs.DESCRIPTION,
    ):
        self.click_type_campaign_button()
        self.click_event_type_campaign_button()
        self.input_event_campaign_name_field(product_name)
        self.input_event_campaign_custom_offer_text_field(offer_text)
        self.input_event_campaign_start_date_field(start_date)
        self.input_event_campaign_end_date_field(end_date)
        self.input_event_campaign_details_field(offer_text)
        self.click_campaign_continue_button()
        self.click_launch_campaign_button()
        self.click_back_to_my_campaign_button()

    def click_type_campaign_button(self):
        type_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.TYPE_CAMPAIGN_BUTTON))
        type_campaign_button.click()

    def click_event_type_campaign_button(self):
        event_type_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.EVENT_TYPE_CAMPAIGN_BUTTON))
        event_type_campaign_button.click()

    def click_specify_your_order_button(self):
        specify_your_order_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SPECIFY_YOUR_ORDER_FIELD))
        specify_your_order_button.click()

    def click_save_xxp_button(self):
        save_xxp_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.RETAILER_CAMPAIGN_SAVE_XXP_BUTTON))
        save_xxp_button.click()

    def input_campaign_product_name_field(self, product_name):
        campaign_product_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_CAMPAIGN_PRODUCT_NAME_FIELD))
        campaign_product_name_field.send_keys(product_name)

    def input_event_campaign_name_field(self, product_name):
        event_campaign_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_EVENT_CAMPAIGN_NAME_FIELD))
        event_campaign_name_field.send_keys(product_name)

    def input_campaign_custom_offer_text_field(self, offer_text):
        campaign_custom_offer_text_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_CAMPAIGN_CUSTOM_OFFER_TEXT_FIELD))
        campaign_custom_offer_text_field.send_keys(offer_text)

    def input_event_campaign_custom_offer_text_field(self, offer_text):
        event_campaign_custom_offer_text_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_EVENT_CAMPAIGN_CUSTOM_CAMPAIGN_TEXT_FIELD))
        event_campaign_custom_offer_text_field.send_keys(offer_text)

    def input_event_campaign_start_date_field(self, start_date):
        event_campaign_start_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_EVENT_CAMPAIGN_START_DATE))
        event_campaign_start_date_field.send_keys(start_date)

    def input_event_campaign_end_date_field(self, end_date):
        event_campaign_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_EVENT_CAMPAIGN_END_DATE))
        event_campaign_end_date_field.send_keys(end_date)

    def input_campaign_original_price_field(self, original_price):
        campaign_original_price_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_CAMPAIGN_ORIGINAL_PRICE_FIELD))
        campaign_original_price_field.send_keys(original_price)

    def input_campaign_offer_price_field(self, offer_price):
        campaign_offer_price_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_CAMPAIGN_OFFER_PRICE_FIELD))
        campaign_offer_price_field.send_keys(offer_price)

    def input_campaign_barcode_field(self, barcode):
        campaign_barcode_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_CAMPAIGN_BARCODE_FIELD))
        campaign_barcode_field.send_keys(barcode)

    def click_campaign_continue_button(self):
        campaign_continue_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.RETAILER_CAMPAIGN_CONTINUE_BUTTON))
        campaign_continue_button.click()

    def click_launch_campaign_button(self):
        launch_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.LAUNCH_CAMPAIGN_BUTTON))
        launch_campaign_button.click()

    def click_back_to_my_campaign_button(self):
        back_to_my_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.BACK_TO_MY_CAMPAIGN_BUTTON))
        back_to_my_campaign_button.click()

    def input_event_campaign_details_field(self, offer_text):
        event_campaign_details_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_EVENT_CAMPAIGN_DETAILS_FIELD))
        event_campaign_details_field.send_keys(offer_text)
