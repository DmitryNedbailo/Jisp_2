import configparser
import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import constants as cs
from tests import locators

config = configparser.ConfigParser()
config.read('config.ini')


class RetailerMyProductPage(BasePage):
    def create_new_retailer_offer(
            self,
            offer_start_day=cs.OFFER_START_DATE,
            offer_end_date=cs.OFFER_END_DATE,
            offer_price=cs.OFFER_PRICE,
            offer_product_barcode=config.get(cs.ENVIRONMENT, 'BARCODE_7')
    ):
        self.click_create_new_offer_button()
        self.click_select_offer_product_button()
        self.input_offer_product_barcode_field(offer_product_barcode)
        self.click_confirm_select_offer_product_button()
        self.input_offer_start_date_field(offer_start_day)
        self.input_offer_end_date_field(offer_end_date)
        self.input_offer_price_field(offer_price)
        self.click_offer_save_button()

    def click_create_new_offer_button(self):
        create_new_offer_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_NEW_OFFER_BUTTON))
        create_new_offer_button.click()

    def click_select_offer_product_button(self):
        select_offer_product_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_OFFER_PRODUCT_BUTTON))
        select_offer_product_button.click()
        time.sleep(1)

    def input_offer_product_barcode_field(self, offer_product_barcode):
        offer_product_barcode_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.SEARCH_OFFER_PRODUCT_FIELD))
        offer_product_barcode_field.send_keys(offer_product_barcode)

    def click_confirm_select_offer_product_button(self):
        confirm_select_offer_product_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONFIRM_SELECT_OFFER_PRODUCT))
        confirm_select_offer_product_button.click()

    def input_offer_start_date_field(self, offer_start_day):
        offer_start_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.OFFER_START_DATE_FIELD))
        offer_start_date_field.send_keys(offer_start_day)

    def input_offer_end_date_field(self, offer_end_date):
        offer_start_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.OFFER_END_DATE_FIELD))
        offer_start_date_field.send_keys(offer_end_date)

    def input_offer_price_field(self, offer_price):
        offer_price_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.OFFER_PRICE_FIELD))
        offer_price_field.send_keys(offer_price)

    def click_offer_checkbox_offer(self):
        offer_checkbox_offer = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.OFFER_CHECKBOX_OFFER))
        offer_checkbox_offer.click()

    def click_offer_save_button(self):
        offer_save_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(locators.OFFER_SAVE_BUTTON))
        offer_save_button.click()
