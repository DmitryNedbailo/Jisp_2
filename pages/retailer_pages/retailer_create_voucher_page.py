from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class RetailerCreateVoucherPage(BasePage):
    def create_voucher(
            self,
            min_order_price=cs.MIN_ORDER_PRICE,
            voucher_start_date=cs.VOUCHER_START_DATE,
            voucher_end_date=cs.VOUCHER_END_DATE,
            voucher_code=cs.VOUCHER_CODE
    ):
        self.input_min_order_price_field(min_order_price)
        self.input_voucher_start_date_field(voucher_start_date)
        self.input_voucher_end_date_field(voucher_end_date)
        self.input_voucher_code_field(voucher_code)
        self.click_voucher_create_draft_button()

    def input_min_order_price_field(self, min_order_price):
        min_order_price_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.VOUCHER_MIN_ORDER_PRICE_FIELD))
        min_order_price_field.send_keys(min_order_price)

    def input_voucher_start_date_field(self, voucher_start_date):
        voucher_start_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.VOUCHER_START_DATE_FIELD))
        voucher_start_date_field.send_keys(voucher_start_date)

    def input_voucher_end_date_field(self, voucher_end_date):
        voucher_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.VOUCHER_END_DATE_FIELD))
        voucher_end_date_field.send_keys(voucher_end_date)

    def input_voucher_code_field(self, voucher_code):
        voucher_code_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.VOUCHER_CODE_FIELD))
        voucher_code_field.send_keys(voucher_code)

    def click_voucher_create_draft_button(self):
        voucher_create_draft_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHER_CREATE_DRAFT_BUTTON))
        voucher_create_draft_button.click()
