import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class RetailerCreateBundlePage(BasePage):
    def create_bundle(
            self,
            bundle_name=None,
            bundle_price=cs.BUNDLE_PRICE,
            bundle_description=cs.DESCRIPTION,
            bundle_component_name=cs.BUNDLE_COMPONENT_NAME
    ):
        self.input_bundle_name_field(bundle_name)
        self.input_bundle_price_field(bundle_price)
        self.click_bundle_create_cover_image_button()
        self.click_bundle_create_cover_image_save_button()
        self.input_bundle_description_field(bundle_description)
        self.input_bundle_component_name_field(bundle_component_name)
        self.click_bundle_add_items_button()
        self.click_add_items_first_product_list()
        self.click_add_items_second_product_list()
        self.click_select_all_product_checkbox()
        self.click_add_items_save_button()
        self.click_create_bundle_button()

    def input_bundle_name_field(self, bundle_name):
        bundle_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.BUNDLE_NAME_FIELD))
        bundle_name_field.send_keys(bundle_name)

    def input_bundle_price_field(self, bundle_price):
        bundle_price_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.BUNDLE_PRICE_FIELD))
        bundle_price_field.send_keys(bundle_price)

    def click_bundle_create_cover_image_button(self):
        bundle_create_cover_image_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.BUNDLE_CREATE_COVER_IMAGE_BUTTON))
        bundle_create_cover_image_button.click()

    def click_bundle_create_cover_image_save_button(self):
        bundle_create_cover_image_save_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.BUNDLE_CREATE_COVER_IMAGE_SAVE_BUTTON))
        bundle_create_cover_image_save_button.click()

    def input_bundle_description_field(self, bundle_description):
        bundle_description_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.BUNDLE_DESCRIPTION_FIELD))
        bundle_description_field.send_keys(bundle_description)

    def input_bundle_component_name_field(self, bundle_component_name):
        bundle_component_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.BUNDLE_COMPONENT_FIELD))
        bundle_component_name_field.send_keys(bundle_component_name)

    def click_bundle_add_items_button(self):
        bundle_add_items_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.BUNDLE_ADD_ITEMS_BUTTON))
        bundle_add_items_button.click()

    def click_add_items_first_product_list(self):
        add_items_first_product_list = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.BUNDLE_ADD_ITEMS_FIRST_PRODUCT_LIST))
        add_items_first_product_list.click()

    def click_add_items_second_product_list(self):
        add_items_second_product_list = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.BUNDLE_ADD_ITEMS_SECOND_PRODUCT_LIST))
        add_items_second_product_list.click()
        time.sleep(1)

    def click_select_all_product_checkbox(self):
        select_all_product_checkbox = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.BUNDLE_ADD_ITEMS_SELECT_ALL_PRODUCTS_CHECKBOX))
        select_all_product_checkbox.click()

    def click_add_items_save_button(self):
        add_items_save_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.BUNDLE_ADD_ITEMS_ADD_BUTTON))
        add_items_save_button.click()
        time.sleep(0.5)

    def click_create_bundle_button(self):
        create_bundle_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.BUNDLE_CREATE_BUTTON))
        create_bundle_button.click()
