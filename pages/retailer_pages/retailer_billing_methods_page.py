import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class RetailerBillingMethodsPage(BasePage):
    def create_new_billing_method(
            self,
            card_number=cs.BILLING_METHOD_CARD_NUMBER,
            expiration_date=cs.BILLING_METHOD_EXPIRATION_DATE
    ):
        self.click_create_new_billing_method_button()
        self.input_billing_method_card_number_field(card_number)
        self.input_billing_method_expiration_date_field(expiration_date)
        self.click_billing_methods_create_confirmation_button()

    def click_create_new_billing_method_button(self):
        create_new_billing_method_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_NEW_BILLING_METHODS_BUTTON))
        create_new_billing_method_button.click()
        time.sleep(1)

    def input_billing_method_card_number_field(self, card_number):
        billing_method_card_number_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.BILLING_METHOD_CARD_NUMBER_INPUT_FIELD))
        billing_method_card_number_field.send_keys(card_number)

    def input_billing_method_expiration_date_field(self, expiration_date):
        billing_method_expiration_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.BILLING_METHOD_EXPIRATION_DATE_INPUT_FIELD))
        billing_method_expiration_date_field.send_keys(expiration_date)

    def click_billing_methods_create_confirmation_button(self):
        billing_methods_create_confirmation_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.BILLING_METHOD_CREATE_CONFIRMATION_BUTTON))
        billing_methods_create_confirmation_button.click()
        time.sleep(2)
