import configparser

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


class RetailerEditProfilePage(BasePage):
    action = ActionChains

    def edit_retailer_profile(
            self,
            retailer_first_name=config.get(cs.ENVIRONMENT, 'RETAILER_FIRST_NAME'),
            retailer_last_name=config.get(cs.ENVIRONMENT, 'RETAILER_LAST_NAME'),
            retailer_email_address=config.get(cs.ENVIRONMENT, 'RETAILER_EMAIL_ADDRESS'),
            retailer_contact_phone_number=cs.RETAILER_CONTACT_PHONE,
            verify_code=cs.VERIFY_NUMBER_CODE,
            retailer_sort_code=config.get(cs.ENVIRONMENT, 'RETAILER_SORT_CODE'),
            retailer_bank_account_number=config.get(cs.ENVIRONMENT, 'RETAILER_BANK_ACCOUNT_NUMBER'),
            retailer_bank_account_name=config.get(cs.ENVIRONMENT, 'RETAILER_BANK_ACCOUNT_NAME'),
            retailer_vat_number=config.get(cs.ENVIRONMENT, 'RETAILER_VAT_NUMBER'),
            retailer_company_name=config.get(cs.ENVIRONMENT, 'RETAILER_COMPANY_NAME'),
            retailer_company_registration_number=config.get(cs.ENVIRONMENT, 'RETAILER_COMPANY_REGISTRATION_NUMBER'),
            retailer_business_description=config.get(cs.ENVIRONMENT, 'RETAILER_BUSINESS_DESCRIPTION'),
            retailer_address_line_1=config.get(cs.ENVIRONMENT, 'RETAILER_ADDRESS_LINE_1'),
            retailer_address_line_2=config.get(cs.ENVIRONMENT, 'RETAILER_ADDRESS_LINE_2'),
            retailer_town=config.get(cs.ENVIRONMENT, 'RETAILER_TOWN'),
            retailer_postcode=config.get(cs.ENVIRONMENT, 'RETAILER_POSTCODE'),
            retailer_store_contact_number=config.get(cs.ENVIRONMENT, 'RETAILER_STORE_CONTACT_NUMBER'),
            retailer_website=config.get(cs.ENVIRONMENT, 'RETAILER_WEBSITE')
    ):
        self.input_retailer_first_name_field(retailer_first_name)
        self.input_retailer_last_name_field(retailer_last_name)
        self.input_retailer_email_address_field(retailer_email_address)
        self.input_retailer_contact_phone_field(retailer_contact_phone_number)
        self.click_retailer_verify_number_button()
        self.input_retailer_verify_code_number_field(verify_code)
        self.click_retailer_verify_code_number_confirm_button()
        self.input_retailer_sort_code_field(retailer_sort_code)
        self.input_retailer_bank_account_number_field(retailer_bank_account_number)
        self.input_retailer_bank_account_name_field(retailer_bank_account_name)
        self.input_retailer_vat_number_field(retailer_vat_number)
        self.input_retailer_company_name_field(retailer_company_name)
        self.input_retailer_company_registration_number_field(retailer_company_registration_number)
        self.input_retailer_business_description_field(retailer_business_description)
        self.input_retailer_address_line_1_field(retailer_address_line_1)
        self.input_retailer_address_line_2_field(retailer_address_line_2)
        self.input_retailer_town_field(retailer_town)
        self.input_retailer_postcode_field(retailer_postcode)
        self.input_retailer_store_contact_number_field(retailer_store_contact_number)
        self.input_retailer_website_field(retailer_website)
        self.click_retailer_save_button()

    def input_retailer_first_name_field(self, retailer_whatsapp_number):
        retailer_first_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_FIRST_NAME_FIELD))
        retailer_first_name_field.send_keys(cs.TEXT_SELECTION)
        retailer_first_name_field.send_keys(retailer_whatsapp_number)

    def input_retailer_last_name_field(self, retailer_last_name):
        retailer_last_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_LAST_NAME_FIELD))
        retailer_last_name_field.send_keys(cs.TEXT_SELECTION)
        retailer_last_name_field.send_keys(retailer_last_name)

    def input_retailer_email_address_field(self, retailer_email_address):
        retailer_email_address_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_EMAIL_ADDRESS_FIELD))
        retailer_email_address_field.send_keys(cs.TEXT_SELECTION)
        retailer_email_address_field.send_keys(retailer_email_address)

    def input_retailer_contact_phone_field(self, retailer_contact_phone_number):
        retailer_contact_phone_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_CONTACT_PHONE_FIELD))
        retailer_contact_phone_field.send_keys(cs.TEXT_SELECTION)
        retailer_contact_phone_field.send_keys(retailer_contact_phone_number)

    def click_retailer_verify_number_button(self):
        retailer_verify_number_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.RETAILER_VERIFY_NUMBER_BUTTON))
        retailer_verify_number_button.click()

    def input_retailer_verify_code_number_field(self, verify_code):
        retailer_verify_code_number_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_VERIFY_CODE_NUMBER_FIELD))
        retailer_verify_code_number_field.send_keys(verify_code)

    def click_retailer_verify_code_number_confirm_button(self):
        retailer_verify_code_number_confirm_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.RETAILER_VERIFY_NUMBER_CONFIRM_BUTTON))
        retailer_verify_code_number_confirm_button.click()

    def input_retailer_whatsapp_number_field(self, retailer_whatsapp_number):
        retailer_whatsapp_number_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_WHATSAPP_NUMBER_FIELD))
        retailer_whatsapp_number_field.send_keys(cs.TEXT_SELECTION)
        retailer_whatsapp_number_field.send_keys(retailer_whatsapp_number)

    def input_retailer_sort_code_field(self, retailer_sort_code):
        retailer_sort_code_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_SORT_CODE_FIELD))
        retailer_sort_code_field.send_keys(cs.TEXT_SELECTION)
        retailer_sort_code_field.send_keys(retailer_sort_code)

    def input_retailer_bank_account_number_field(self, retailer_bank_account_number):
        retailer_bank_account_number_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_BANK_ACCOUNT_NUMBER_FIELD))
        retailer_bank_account_number_field.send_keys(cs.TEXT_SELECTION)
        retailer_bank_account_number_field.send_keys(retailer_bank_account_number)

    def input_retailer_bank_account_name_field(self, retailer_bank_account_name):
        retailer_bank_account_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_BANK_ACCOUNT_NAME_FIELD))
        retailer_bank_account_name_field.send_keys(cs.TEXT_SELECTION)
        retailer_bank_account_name_field.send_keys(retailer_bank_account_name)

    def input_retailer_vat_number_field(self, retailer_vat_number):
        retailer_vat_number_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_VAT_NUMBER_FIELD))
        retailer_vat_number_field.send_keys(cs.TEXT_SELECTION)
        retailer_vat_number_field.send_keys(retailer_vat_number)

    def input_retailer_company_name_field(self, retailer_company_name):
        retailer_company_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_COMPANY_NAME_FIELD))
        retailer_company_name_field.send_keys(cs.TEXT_SELECTION)
        retailer_company_name_field.send_keys(retailer_company_name)

    def input_retailer_company_registration_number_field(self, retailer_company_registration_number):
        retailer_company_registration_number_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_COMPANY_REGISTRATION_NUMBER_FIELD))
        retailer_company_registration_number_field.send_keys(cs.TEXT_SELECTION)
        retailer_company_registration_number_field.send_keys(retailer_company_registration_number)

    def input_retailer_business_description_field(self, retailer_business_description):
        retailer_business_description_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_BUSINESS_DESCRIPTION_FIELD))
        retailer_business_description_field.send_keys(cs.TEXT_SELECTION)
        retailer_business_description_field.send_keys(retailer_business_description)

    def input_retailer_address_line_1_field(self, retailer_address_line_1):
        retailer_address_line_1_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_ADDRESS_LINE_1_FIELD))
        retailer_address_line_1_field.send_keys(cs.TEXT_SELECTION)
        retailer_address_line_1_field.send_keys(retailer_address_line_1)

    def input_retailer_address_line_2_field(self, retailer_address_line_2):
        retailer_address_line_2_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_ADDRESS_LINE_2_FIELD))
        retailer_address_line_2_field.send_keys(cs.TEXT_SELECTION)
        retailer_address_line_2_field.send_keys(retailer_address_line_2)

    def input_retailer_town_field(self, retailer_town):
        retailer_town_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_TOWN_FIELD))
        retailer_town_field.send_keys(cs.TEXT_SELECTION)
        retailer_town_field.send_keys(retailer_town)

    def input_retailer_postcode_field(self, retailer_postcode):
        retailer_postcode_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_POSTCODE_FIELD))
        retailer_postcode_field.send_keys(cs.TEXT_SELECTION)
        retailer_postcode_field.send_keys(retailer_postcode)

    def input_retailer_store_contact_number_field(self, retailer_store_contact_number):
        retailer_store_contact_number_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_STORE_CONTACT_NUMBER_FIELD))
        retailer_store_contact_number_field.send_keys(cs.TEXT_SELECTION)
        retailer_store_contact_number_field.send_keys(retailer_store_contact_number)

    def input_retailer_website_field(self, retailer_website):
        retailer_website_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RETAILER_WEBSITE_FIELD))
        retailer_website_field.send_keys(cs.TEXT_SELECTION)
        retailer_website_field.send_keys(retailer_website)

    def click_retailer_save_button(self):
        retailer_save_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.RETAILER_SAVE_BUTTON))
        retailer_save_button.click()
