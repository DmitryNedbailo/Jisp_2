import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class RetailerBuyJispMaterialsPage(BasePage):
    def add_materials_to_basket(self, quantity_materials=cs.QUANTITY_MATERIALS):
        self.input_quantity_materials_field(quantity_materials)
        self.click_add_to_basket_materials_button()
        self.click_go_to_basket_button()

    def input_quantity_materials_field(self, quantity_materials):
        quantity_materials_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.QUANTITY_MATERIALS_FIELD))
        quantity_materials_field.send_keys(quantity_materials)

    def click_add_to_basket_materials_button(self):
        add_to_basket_materials_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADD_TO_BASKET_MATERIALS_BUTTON))
        add_to_basket_materials_button.click()

    def click_go_to_basket_button(self):
        go_to_basket_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.GO_TO_BASKET_BUTTON))
        go_to_basket_button.click()
        time.sleep(1)
