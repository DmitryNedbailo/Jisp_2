import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class RetailerVoucherListPage(BasePage):
    def open_create_new_voucher_page(self):
        self.click_create_new_voucher_button()

    def open_edit_voucher_page(self):
        self.click_edit_voucher_button()

    def delete_voucher(self):
        self.click_delete_voucher_button()
        self.click_delete_voucher_confirm_button()

    def publish_voucher(self):
        self.click_publish_voucher_button()
        self.click_confirm_publish_voucher_button()

    def click_create_new_voucher_button(self):
        create_new_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHERS_LIST_CREATE_NEW_BUTTON))
        create_new_voucher_button.click()

    def click_publish_voucher_button(self):
        publish_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.PUBLISH_VOUCHER_BUTTON))
        publish_voucher_button.click()

    def click_confirm_publish_voucher_button(self):
        confirm_publish_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONFIRM_PUBLISH_VOUCHER_BUTTON))
        confirm_publish_voucher_button.click()

    def click_edit_voucher_button(self):
        edit_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.EDIT_VOUCHER_BUTTON))
        edit_voucher_button.click()

    def click_delete_voucher_button(self):
        delete_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DELETE_VOUCHER_BUTTON))
        delete_voucher_button.click()

    def click_delete_voucher_confirm_button(self):
        delete_voucher_confirm_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DELETE_VOUCHER_CONFIRM_BUTTON))
        delete_voucher_confirm_button.click()
        time.sleep(1)
