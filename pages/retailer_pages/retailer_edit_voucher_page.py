from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

action = ActionChains


class RetailerEditVoucherPage(BasePage):
    def edit_voucher(
            self,
            min_order_price=cs.MIN_ORDER_PRICE,
            voucher_start_date=cs.VOUCHER_START_DATE,
            voucher_end_date=cs.VOUCHER_END_DATE,
            voucher_code=cs.VOUCHER_CODE
    ):
        self.input_change_min_order_price_field(min_order_price)
        self.input_change_voucher_start_date_field(voucher_start_date)
        self.input_change_voucher_end_date_field(voucher_end_date)
        self.input_change_voucher_code_field(voucher_code)
        self.click_voucher_create_draft_button()

    def input_change_min_order_price_field(self, min_order_price):
        change_min_order_price_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.VOUCHER_MIN_ORDER_PRICE_FIELD))
        change_min_order_price_field.send_keys(cs.TEXT_SELECTION)
        change_min_order_price_field.send_keys(min_order_price)

    def input_change_voucher_start_date_field(self, voucher_start_date):
        change_voucher_start_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.VOUCHER_START_DATE_FIELD))
        change_voucher_start_date_field.send_keys(cs.TEXT_SELECTION)
        change_voucher_start_date_field.send_keys(voucher_start_date)

    def input_change_voucher_end_date_field(self, voucher_end_date):
        change_voucher_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.VOUCHER_END_DATE_FIELD))
        change_voucher_end_date_field.send_keys(cs.TEXT_SELECTION)
        change_voucher_end_date_field.send_keys(voucher_end_date)

    def input_change_voucher_code_field(self, voucher_code):
        change_voucher_code_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.VOUCHER_CODE_FIELD))
        change_voucher_code_field.send_keys(cs.TEXT_SELECTION)
        change_voucher_code_field.send_keys(voucher_code)

    def click_voucher_create_draft_button(self):
        voucher_create_draft_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHER_CREATE_DRAFT_BUTTON))
        voucher_create_draft_button.click()
