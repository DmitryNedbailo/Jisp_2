from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class RetailerMyCampaignPage(BasePage):
    def open_make_a_campaign_page(self):
        self.click_make_a_campaign_button()

    def click_make_a_campaign_button(self):
        make_a_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.MAKE_A_CAMPAIGN_BUTTON))
        make_a_campaign_button.click()
