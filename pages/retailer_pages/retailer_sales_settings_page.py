import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class RetailerSalesSettingPage(BasePage):
    def activate_deactivate_settings_and_change_delivery_method(self):
        self.activate_deactivate_scan_and_go()
        self.activate_deactivate_click_and_collect()
        self.activate_deactivate_delivery()
        self.change_delivery_method()
        self.click_save_sales_settings_button()

    def activate_deactivate_scan_and_go(self):
        self.click_activate_deactivate_scan_and_go_switcher()
        self.click_activate_deactivate_confirm_button()
        self.click_activate_deactivate_scan_and_go_switcher()
        self.click_activate_deactivate_confirm_button()

    def activate_deactivate_click_and_collect(self):
        self.click_activate_deactivate_click_and_collect_switcher()
        self.click_activate_deactivate_confirm_button()
        self.click_activate_deactivate_click_and_collect_switcher()
        self.click_activate_deactivate_confirm_button()

    def activate_deactivate_delivery(self):
        self.click_activate_deactivate_delivery_switcher()
        self.click_activate_deactivate_confirm_button()
        self.click_activate_deactivate_delivery_switcher()
        self.click_activate_deactivate_confirm_button()
        time.sleep(1)

    def change_delivery_method(self):
        self.click_change_delivery_method_button()
        self.click_select_zoom_1hr_delivery_button()
        self.click_change_delivery_method_button()
        self.click_select_gophr_delivery_button()
        self.click_change_delivery_method_button()
        self.click_select_in_house_delivery_button()

    def click_activate_deactivate_scan_and_go_switcher(self):
        activate_deactivate_scan_and_go_switcher = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ACTIVATE_DEACTIVATE_SCAN_AND_GO_SWITCHER))
        activate_deactivate_scan_and_go_switcher.click()
        time.sleep(1)

    def click_activate_deactivate_click_and_collect_switcher(self):
        activate_deactivate_click_and_collect_switcher = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ACTIVATE_DEACTIVATE_CLICK_AND_COLLECT_SWITCHER))
        activate_deactivate_click_and_collect_switcher.click()
        time.sleep(1)

    def click_activate_deactivate_delivery_switcher(self):
        activate_deactivate_delivery_switcher = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ACTIVATE_DEACTIVATE_DELIVERY_SWITCHER))
        activate_deactivate_delivery_switcher.click()
        time.sleep(1)

    def click_activate_deactivate_confirm_button(self):
        activate_deactivate_confirm_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ACTIVATE_DEACTIVATE_CONFIRM_BUTTON))
        activate_deactivate_confirm_button.click()

    def click_change_delivery_method_button(self):
        change_delivery_method_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CHANGE_DELIVERY_METHOD_BUTTON))
        change_delivery_method_button.click()

    def click_select_zoom_1hr_delivery_button(self):
        select_zoom_1hr_delivery_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_ZOOM_1HR_DELIVERY_BUTTON))
        select_zoom_1hr_delivery_button.click()

    def click_select_gophr_delivery_button(self):
        select_gophr_delivery_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_GOPHR_DELIVERY_BUTTON))
        select_gophr_delivery_button.click()

    def click_select_in_house_delivery_button(self):
        select_in_house_delivery_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_IN_HOUSE_DELIVERY_BUTTON))
        select_in_house_delivery_button.click()

    def click_save_sales_settings_button(self):
        save_sales_settings_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SAVE_SALES_SETTINGS_BUTTON))
        save_sales_settings_button.click()
        time.sleep(1)
