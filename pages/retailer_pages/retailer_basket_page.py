import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class RetailerBasketPage(BasePage):
    def click_buy_now_button(self):
        buy_now_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.BASKET_BUY_NOW_BUTTON))
        buy_now_button.click()
        time.sleep(5)
