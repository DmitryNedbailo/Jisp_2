import configparser
import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


class AdminAdvertiserPage(BasePage):

    def log_in_via_advertiser(self, company_name=config.get(cs.ENVIRONMENT, 'COMPANY_NAME')):
        self.input_advertiser_company_name(company_name)
        self.click_advertiser_search_apply_button()
        self.click_log_in_via_advert()

    def log_in_via_system_advertiser(self, company_name=cs.SYSTEM_ADVERTISER_COMPANY_NAME):
        self.input_advertiser_company_name(company_name)
        self.click_advertiser_search_apply_button()
        self.click_advertiser_sort_reg_button()
        self.click_advertiser_sort_reg_button()
        self.click_log_in_via_advert()

    def input_advertiser_company_name(self, company_name):
        advertiser_company_name = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERTISER_TAB_COMPANY_NAME_FIELD))
        advertiser_company_name.send_keys(company_name)

    def click_advertiser_search_apply_button(self):
        advertiser_search_apply_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERTISER_SEARCH_APPLY_BUTTON))
        advertiser_search_apply_button.click()
        time.sleep(2)

    def click_advertiser_sort_reg_button(self):
        advertiser_sort_reg_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERTISER_SORT_REG_BUTTON))
        advertiser_sort_reg_button.click()
        time.sleep(0.5)

    def click_log_in_via_advert(self):
        log_in_via_advert = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERTISER_LOG_IN_VIA_ADVERTISER))
        log_in_via_advert.click()
