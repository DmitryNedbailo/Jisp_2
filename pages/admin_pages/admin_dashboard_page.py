import configparser
import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


class AdminDashboardPage(BasePage):
    def log_in_via_retailer_admin(self, retailer_company_name=config.get(cs.ENVIRONMENT, 'RETAILER_COMPANY_NAME')):
        self.input_retailer_company_name_field(retailer_company_name)
        self.click_retailer_login_type_button()
        self.click_retailer_nfrn_login_type_button()

    def click_advertiser_tab_button(self):
        advertiser_tab_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADMIN_ADVERTISERS_TAB))
        advertiser_tab_button.click()

    def input_retailer_company_name_field(self, retailer_company_name):
        retailer_company_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.SEARCH_BY_COMPANY_NAME_FIELD))
        retailer_company_name_field.send_keys(retailer_company_name)
        time.sleep(3)

    def click_retailer_login_type_button(self):
        retailer_login_type_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.LOGIN_TYPE_BUTTON))
        retailer_login_type_button.click()

    def click_retailer_nfrn_login_type_button(self):
        retailer_nfrn_login_type_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.NFRN_LOGIN_TYPE_BUTTON))
        retailer_nfrn_login_type_button.click()
        time.sleep(4)
