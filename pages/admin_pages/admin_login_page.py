import configparser

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


class AdminLoginPage(BasePage):
    def sign_in_via_admin(
            self,
            admin_username=config.get(cs.ENVIRONMENT, 'ADMIN_USERNAME'),
            admin_password=config.get(cs.ENVIRONMENT, 'ADMIN_PASSWORD')
    ):
        self.input_admin_name(admin_username)
        self.input_admin_password(admin_password)
        self.click_admin_log_in_button()

    def click_admin_log_in_button(self):
        admin_log_in_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADMIN_LOG_IN_BUTTON))
        admin_log_in_button.click()

    def input_admin_name(self, admin_username):
        admin_username_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ADMIN_USERNAME_FIELD))
        admin_username_field.send_keys(admin_username)

    def input_admin_password(self, admin_password):
        admin_password_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ADMIN_PASSWORD_FIELD))
        admin_password_field.send_keys(admin_password)
