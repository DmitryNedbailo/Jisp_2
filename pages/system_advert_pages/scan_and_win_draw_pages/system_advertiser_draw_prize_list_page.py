import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class SystemAdvertiserDrawPrizeListPage(BasePage):
    def open_create_draw_prize_page(self):
        self.click_create_draw_prize_button()

    def open_first_draw_prize(self):
        self.click_first_draw_prize()

    def activate_deactivate_draw_prize(self):
        self.click_activate_deactivate_draw_prize_switcher()
        self.click_confirm_activate_deactivate_draw_prize_button()

    def delete_draw_prize(self):
        self.click_delete_draw_prize_button()
        self.click_confirm_delete_draw_prize_button()

    def click_create_draw_prize_button(self):
        create_draw_prize_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_NEW_DRAW_PRIZE_BUTTON))
        create_draw_prize_button.click()

    def click_first_draw_prize(self):
        first_draw_prize = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.SELECT_FIRST_DRAW_PRIZE))
        first_draw_prize.click()

    def click_activate_deactivate_draw_prize_switcher(self):
        activate_deactivate_draw_prize_switcher = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ACTIVATE_DEACTIVATE_DRAW_PRIZE_SWITCHER))
        activate_deactivate_draw_prize_switcher.click()

    def click_confirm_activate_deactivate_draw_prize_button(self):
        confirm_activate_deactivate_draw_prize_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DRAW_PRIZE_CONFIRM_ACTIVATE_DEACTIVATE_BUTTON))
        confirm_activate_deactivate_draw_prize_button.click()
        time.sleep(1)

    def click_delete_draw_prize_button(self):
        delete_draw_prize_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DRAW_PRIZE_DELETE_BUTTON))
        delete_draw_prize_button.click()

    def click_confirm_delete_draw_prize_button(self):
        confirm_delete_draw_prize_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DRAW_PRIZE_CONFIRM_DELETE_BUTTON))
        confirm_delete_draw_prize_button.click()
        time.sleep(1)
