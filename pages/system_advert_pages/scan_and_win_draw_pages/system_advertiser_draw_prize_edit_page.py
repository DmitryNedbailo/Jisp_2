import time

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

action = ActionChains


class SystemAdvertiserDrawPrizeEditPage(BasePage):
    def edit_draw_prize(
            self,
            draw_name=cs.DRAW_PRIZE_NAME,
            description=cs.DESCRIPTION,
            end_date=cs.DRAW_PRIZE_END_DATE
    ):
        self.input_change_draw_prize_name_field(draw_name)
        self.input_change_draw_prize_description_field(description)
        self.input_change_draw_prize_end_date_field(end_date)
        self.click_draw_prize_save_changes_button()

    def input_change_draw_prize_name_field(self, draw_name):
        change_draw_prize_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.DRAW_PRIZE_NAME_FIELD))
        change_draw_prize_name_field.send_keys(cs.TEXT_SELECTION)
        time.sleep(1)
        change_draw_prize_name_field.send_keys(draw_name)

    def input_change_draw_prize_description_field(self, description):
        change_draw_prize_description_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.DRAW_PRIZE_DESCRIPTION_FIELD))
        change_draw_prize_description_field.send_keys(cs.TEXT_SELECTION)
        change_draw_prize_description_field.send_keys(description)

    def input_change_draw_prize_end_date_field(self, end_date):
        change_draw_prize_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.DRAW_PRIZE_END_DATE_FIELD))
        change_draw_prize_end_date_field.send_keys(cs.TEXT_SELECTION)
        change_draw_prize_end_date_field.send_keys(end_date)

    def click_draw_prize_save_changes_button(self):
        draw_prize_save_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DRAW_PRIZE_SAVE_PRIZE_BUTTON))
        draw_prize_save_button.click()
        time.sleep(1)
