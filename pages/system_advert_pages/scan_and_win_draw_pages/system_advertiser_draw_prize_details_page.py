import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class SystemAdvertiserDrawPrizeDetailsPage(BasePage):
    def activate_deactivate_draw_prize(self):
        self.click_draw_prize_activate_deactivate_button()
        self.click_draw_prize_confirm_activate_deactivate_button()

    def open_edit_draw_prize_page(self):
        self.click_edit_draw_prize_button()

    def click_draw_prize_activate_deactivate_button(self):
        draw_prize_activate_deactivate_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DRAW_PRIZE_ACTIVATE_DEACTIVATE_BUTTON))
        draw_prize_activate_deactivate_button.click()

    def click_draw_prize_confirm_activate_deactivate_button(self):
        draw_prize_confirm_activate_deactivate_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DRAW_PRIZE_CONFIRM_ACTIVATE_DEACTIVATE_BUTTON))
        draw_prize_confirm_activate_deactivate_button.click()
        time.sleep(1)

    def click_edit_draw_prize_button(self):
        edit_draw_prize_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DRAW_PRIZE_EDIT_BUTTON))
        edit_draw_prize_button.click()
