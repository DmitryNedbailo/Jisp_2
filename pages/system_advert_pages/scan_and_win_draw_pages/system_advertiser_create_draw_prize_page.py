import os
import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class SystemAdvertiserCreateDrawPrizePage(BasePage):
    def create_draw_prize(
            self,
            draw_prize_name=cs.DRAW_PRIZE_NAME,
            draw_prize_description=cs.DESCRIPTION,
            draw_prize_end_date=cs.DRAW_PRIZE_END_DATE,
            draw_entry_image=cs.DRAW_ENTRY_IMAGE,
            draw_image=cs.DRAW_PRIZE_IMAGE
    ):
        self.input_draw_prize_name_field(draw_prize_name)
        self.input_draw_prize_description_field(draw_prize_description)
        self.input_draw_prize_end_date_field(draw_prize_end_date)
        self.upload_draw_prize_entry_image(draw_entry_image)
        self.upload_draw_prize_image(draw_image)
        self.click_draw_prize_save_prize_button()

    def input_draw_prize_name_field(self, draw_prize_name):
        draw_prize_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.DRAW_PRIZE_NAME_FIELD))
        draw_prize_name_field.send_keys(draw_prize_name)

    def input_draw_prize_description_field(self, draw_prize_description):
        draw_prize_description_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.DRAW_PRIZE_DESCRIPTION_FIELD))
        draw_prize_description_field.send_keys(draw_prize_description)

    def input_draw_prize_end_date_field(self, draw_prize_end_date):
        draw_prize_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.DRAW_PRIZE_END_DATE_FIELD))
        draw_prize_end_date_field.send_keys(draw_prize_end_date)

    def upload_draw_prize_entry_image(self, draw_entry_image):
        draw_prize_entry_image = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.DRAW_PRIZE_ENTRY_IMAGE_UPLOAD))
        draw_prize_entry_image.send_keys(os.getcwd() + draw_entry_image)

    def upload_draw_prize_image(self, draw_image):
        draw_prize_image = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.DRAW_PRIZE_IMAGE_UPLOAD))
        draw_prize_image.send_keys(os.getcwd() + draw_image)

    def click_draw_prize_save_prize_button(self):
        time.sleep(1)
        draw_prize_save_prize_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DRAW_PRIZE_SAVE_PRIZE_BUTTON))
        draw_prize_save_prize_button.click()
