import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class SystemAdvertiserCreateVoucherReportPage(BasePage):
    def create_credit_vouchers_report(self, voucher_report_name=cs.CREDIT_VOUCHERS_REPORT_NAME):
        self.input_vouchers_report_name_field(voucher_report_name)
        self.click_select_system_advertiser_vouchers_report_button()
        self.click_select_all_credit_vouchers_button()
        self.click_save_credit_vouchers_button()
        self.click_generate_vouchers_report_button()

    def create_loyalty_voucher_report(self, voucher_report_name=cs.LOYALTY_VOUCHER_REPORT_NAME):
        self.input_vouchers_report_name_field(voucher_report_name)
        self.click_select_system_advertiser_vouchers_report_button()
        self.click_select_loyalty_voucher_button()
        self.click_save_loyalty_voucher_button()
        self.click_generate_vouchers_report_button()

    def input_vouchers_report_name_field(self, voucher_report_name):
        credit_vouchers_report_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CREDIT_VOUCHER_REPORT_NAME_INPUT_FIELD))
        credit_vouchers_report_name_field.send_keys(voucher_report_name)

    def click_select_system_advertiser_vouchers_report_button(self):
        select_system_advertiser_vouchers_report_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_SYSTEM_ADVERTISER_VOUCHERS_REPORT_BUTTON))
        select_system_advertiser_vouchers_report_button.click()

    def click_select_all_credit_vouchers_button(self):
        select_all_credit_vouchers_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_ALL_CREDIT_VOUCHERS_BUTTON))
        select_all_credit_vouchers_button.click()

    def click_select_loyalty_voucher_button(self):
        select_loyalty_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_LOYALTY_VOUCHER_BUTTON))
        select_loyalty_voucher_button.click()

    def click_save_credit_vouchers_button(self):
        save_credit_vouchers_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_ALL_CREDIT_VOUCHERS_SAVE_BUTTON))
        save_credit_vouchers_button.click()
        time.sleep(1)

    def click_save_loyalty_voucher_button(self):
        save_loyalty_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_LOYALTY_VOUCHER_SAVE_BUTTON))
        save_loyalty_voucher_button.click()
        time.sleep(1)

    def click_generate_vouchers_report_button(self):
        generate_vouchers_report_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREDIT_VOUCHERS_REPORT_GENERATE_REPORT_BUTTON))
        generate_vouchers_report_button.click()
        time.sleep(3)
