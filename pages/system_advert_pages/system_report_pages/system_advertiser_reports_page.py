from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class SystemAdvertiserReportsPage(BasePage):
    def open_system_advertiser_create_voucher_report_page(self):
        self.click_system_advertiser_create_voucher_report_button()

    def click_system_advertiser_create_voucher_report_button(self):
        system_advertiser_create_voucher_report_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SYSTEM_ADVERTISER_CREATE_NEW_REPORT_BUTTON))
        system_advertiser_create_voucher_report_button.click()
