import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class SystemAdvertiserCreditVouchersPage(BasePage):
    def open_create_credit_voucher_page(self):
        self.click_create_new_credit_voucher_button()

    def open_system_advertiser_reports_tab(self):
        self.click_system_advert_reports_tab()

    def open_draw_prize_list_page(self):
        self.click_scan_and_win_tab()

    def open_credit_voucher(self):
        self.select_credit_voucher()

    def click_create_new_credit_voucher_button(self):
        create_new_credit_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_NEW_CREDIT_VOUCHER_BUTTON))
        create_new_credit_voucher_button.click()

    def click_system_advert_reports_tab(self):
        system_advert_reports_tab = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SYSTEM_ADVERTISER_REPORTS_TAB))
        system_advert_reports_tab.click()

    def click_scan_and_win_tab(self):
        scan_and_win_tab = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SYSTEM_ADVERTISER_SCAN_AND_WIN_TAB))
        scan_and_win_tab.click()

    def select_credit_voucher(self):
        self.driver.execute_script("window.scrollTo(0, 1000);")
        time.sleep(2)
        credit_voucher = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_CREDIT_VOUCHER))
        credit_voucher.click()
