import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class SystemAdvertiserCreditVoucherDetailsPage(BasePage):
    def open_credit_voucher_edit_page(self):
        self.click_edit_credit_voucher_button()

    def delete_credit_voucher(self):
        self.click_delete_credit_voucher_button()
        self.click_confirm_delete_credit_voucher_button()

    def click_edit_credit_voucher_button(self):
        edit_credit_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.EDIT_CREDIT_VOUCHER_BUTTON))
        edit_credit_voucher_button.click()

    def click_delete_credit_voucher_button(self):
        delete_credit_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DELETE_CREDIT_VOUCHER_BUTTON))
        delete_credit_voucher_button.click()

    def click_confirm_delete_credit_voucher_button(self):
        confirm_delete_credit_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONFIRM_DELETE_CREDIT_VOUCHER_BUTTON))
        confirm_delete_credit_voucher_button.click()
        time.sleep(1)
