import os
import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class SystemAdvertiserCreateCreditVoucherPage(BasePage):
    def create_credit_voucher(
            self,
            credit_voucher_name=cs.CREDIT_VOUCHER_NAME,
            credit_voucher_description=cs.DESCRIPTION,
            credit_voucher_end_date=cs.VOUCHER_END_DATE,
            credit_voucher_discount=cs.DISCOUNT,
            credit_voucher_image=cs.VOUCHER_LEEDS_4,
            credit_voucher_background_image=cs.BACKGROUND_LEEDS_4
    ):
        self.input_credit_voucher_name_field(credit_voucher_name)
        self.input_credit_voucher_description_field(credit_voucher_description)
        self.input_credit_voucher_end_date_field(credit_voucher_end_date)
        self.input_credit_voucher_discount_field(credit_voucher_discount)
        self.upload_credit_voucher_image(credit_voucher_image)
        self.upload_credit_voucher_background_image(credit_voucher_background_image)
        self.click_credit_voucher_save_draft_button()
        self.click_credit_voucher_publish_button()
        self.click_credit_voucher_publish_confirm_button()

    def input_credit_voucher_name_field(self, credit_voucher_name):
        credit_voucher_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CREDIT_VOUCHER_NAME_FIELD_INPUT))
        credit_voucher_name_field.send_keys(credit_voucher_name)

    def input_credit_voucher_description_field(self, credit_voucher_description):
        credit_voucher_description_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CREDIT_VOUCHER_DESCRIPTION_INPUT_FIELD))
        credit_voucher_description_field.send_keys(credit_voucher_description)

    def input_credit_voucher_end_date_field(self, credit_voucher_end_date):
        credit_voucher_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CREDIT_VOUCHER_END_DATE_SELECTION))
        credit_voucher_end_date_field.send_keys(credit_voucher_end_date)

    def input_credit_voucher_discount_field(self, credit_voucher_discount):
        credit_voucher_discount_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CREDIT_VOUCHER_DISCOUNT_INPUT_FIELD))
        credit_voucher_discount_field.send_keys(credit_voucher_discount)

    def upload_credit_voucher_image(self, credit_voucher_image):
        credit_voucher_image_upload = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CREDIT_VOUCHER_IMG_UPLOAD_VOUCHER_IMAGE))
        credit_voucher_image_upload.send_keys(os.getcwd() + credit_voucher_image)

    def upload_credit_voucher_background_image(self, credit_voucher_background_image):
        credit_voucher_background_image_upload = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CREDIT_VOUCHER_IMG_UPLOAD_BACKGROUND_IMAGE))
        credit_voucher_background_image_upload.send_keys(os.getcwd() + credit_voucher_background_image)

    def click_credit_voucher_save_draft_button(self):
        time.sleep(1)
        credit_voucher_save_draft_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREDIT_VOUCHER_SAVE_DRAFT_BUTTON))
        credit_voucher_save_draft_button.click()

    def click_credit_voucher_publish_button(self):
        credit_voucher_publish_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREDIT_VOUCHER_PUBLISH_VOUCHER_BUTTON))
        credit_voucher_publish_button.click()

    def click_credit_voucher_publish_confirm_button(self):
        credit_voucher_publish_confirm_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREDIT_VOUCHER_PUBLISH_VOUCHER_CONFIRM_BUTTON))
        credit_voucher_publish_confirm_button.click()
        time.sleep(2)
