from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class SystemAdvertiserCreditVoucherEditPage(BasePage):
    def edit_credit_voucher(
            self,
            credit_voucher_name=cs.CREDIT_VOUCHER_NAME,
            description=cs.DESCRIPTION,
            end_date=cs.VOUCHER_END_DATE,
            discount=cs.DISCOUNT
    ):
        self.input_change_credit_voucher_name_field(credit_voucher_name)
        self.input_change_credit_voucher_description_field(description)
        self.input_change_credit_voucher_end_date_field(end_date)
        self.input_change_credit_voucher_discount_field(discount)
        self.click_credit_voucher_save_changes_button()

    def input_change_credit_voucher_name_field(self, credit_voucher_name):
        change_credit_voucher_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CREDIT_VOUCHER_NAME_FIELD_INPUT))
        change_credit_voucher_name_field.send_keys(cs.TEXT_SELECTION)
        change_credit_voucher_name_field.send_keys(credit_voucher_name)

    def input_change_credit_voucher_description_field(self, description):
        change_credit_voucher_description_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CREDIT_VOUCHER_DESCRIPTION_INPUT_FIELD))
        change_credit_voucher_description_field.send_keys(cs.TEXT_SELECTION)
        change_credit_voucher_description_field.send_keys(description)

    def input_change_credit_voucher_end_date_field(self, end_date):
        change_credit_voucher_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CREDIT_VOUCHER_END_DATE_SELECTION))
        change_credit_voucher_end_date_field.send_keys(cs.TEXT_SELECTION)
        change_credit_voucher_end_date_field.send_keys(end_date)

    def input_change_credit_voucher_discount_field(self, discount):
        change_credit_voucher_discount_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CREDIT_VOUCHER_DISCOUNT_INPUT_FIELD))
        change_credit_voucher_discount_field.send_keys(cs.TEXT_SELECTION)
        change_credit_voucher_discount_field.send_keys(discount)

    def click_credit_voucher_save_changes_button(self):
        credit_voucher_save_changes_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREDIT_VOUCHER_SAVE_EDIT_BUTTON))
        credit_voucher_save_changes_button.click()
