class BasePage:
    def __init__(self, driver):
        """
        Instance Variable
        """
        self.driver = driver

    def open(self, link):
        self.driver.get(link)
        self.driver.maximize_window()
