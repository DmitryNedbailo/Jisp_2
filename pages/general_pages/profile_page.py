import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class ProfilePage(BasePage):
    def open_advert_dashboard(self):
        self.click_advert_dashboard_button()

    def click_advert_dashboard_button(self):
        advert_dashboard_tab = WebDriverWait(self.driver, 10).until(
            (EC.presence_of_element_located(locators.ADVERT_DASHBOARD_TAB)))
        advert_dashboard_tab.click()
        time.sleep(2)
