import configparser

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


class AdvertLoginPage(BasePage):

    def sign_in_via_advert(self, advert_username=config.get(cs.ENVIRONMENT, 'ADVERT_USERNAME'),
                           advert_password=config.get(cs.ENVIRONMENT, 'ADVERT_PASSWORD')):
        self.input_advert_name(advert_username)
        self.input_advert_password(advert_password)
        self.click_advert_log_in_button()

    def open_sign_up_page(self):
        self.click_advert_sign_up_button()

    def click_advert_log_in_button(self):
        log_in_button = self.driver.find_element(*locators.ADVERT_LOG_IN_BUTTON)
        log_in_button.click()

    def input_advert_name(self, advert_username):
        advert_username_field = self.driver.find_element(*locators.ADVERT_USERNAME_FIELD)
        advert_username_field.send_keys(advert_username)

    def input_advert_password(self, advert_password):
        advert_password_field = self.driver.find_element(*locators.ADVERT_PASSWORD_FILED)
        advert_password_field.send_keys(advert_password)

    def click_advert_sign_up_button(self):
        advert_sign_up_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_SIGN_UP_BUTTON))
        advert_sign_up_button.click()
