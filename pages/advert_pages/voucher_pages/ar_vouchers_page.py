import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class ArVouchersPage(BasePage):
    def open_create_voucher_page(self):
        self.click_create_new_button()

    def open_first_ar_voucher_details_page(self):
        self.select_first_ar_voucher()

    def delete_ar_voucher(self):
        self.click_ar_voucher_delete_button()
        self.click_ar_voucher_confirm_delete_button()

    def click_create_new_button(self):
        create_new_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_NEW_BUTTON)
        )
        create_new_button.click()

    def select_first_ar_voucher(self):
        first_ar_voucher = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.SELECT_FIRST_AR_VOUCHER))
        first_ar_voucher.click()

    def click_ar_voucher_delete_button(self):
        ar_voucher_delete_button = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.AR_VOUCHER_DELETE_BUTTON))
        ar_voucher_delete_button.click()

    def click_ar_voucher_confirm_delete_button(self):
        ar_voucher_confirm_delete_button = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.AR_VOUCHER_CONFIRM_DELETE_BUTTON))
        ar_voucher_confirm_delete_button.click()
        time.sleep(1)
