from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class ArVoucherDetailsPage(BasePage):
    def open_ar_voucher_edit_page(self):
        self.click_ar_voucher_edit_button()

    def click_ar_voucher_edit_button(self):
        ar_voucher_edit_button = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.EDIT_AR_VOUCHER_BUTTON))
        ar_voucher_edit_button.click()
