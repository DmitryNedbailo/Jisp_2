import configparser
import os
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


class CreateVoucherPage(BasePage):
    def create_ar_voucher(
            self,
            one_time_use=False,
            one_time_use_60_min=False,
            one_time_use_priority=False,
            one_time_use_priority_60_min=False,
            redeemable_once_per_day=False,
            redeemable_once_per_day_60_min=False,
            redeemable_once_per_day_priority=False,
            redeemable_once_per_day_priority_60_min=False,
            unlimited_use=False,
            unlimited_use_60_min=False,
            unlimited_use_priority=False,
            unlimited_use_priority_60_min=False,
            description=cs.DESCRIPTION,
            voucher_end_date=cs.VOUCHER_END_DATE,
            rrp=cs.RRP,
            discount=cs.DISCOUNT,
            postcode=config.get(cs.ENVIRONMENT, 'POSTCODE'),
            store=config.get(cs.ENVIRONMENT, 'STORE'),
    ):
        unlimited_use_dict = {
            "voucher_name_1": cs.VOUCHER_UNLIMITED_NAME,
            "voucher_name_2": cs.VOUCHER_UNLIMITED_60_MIN_NAME,
            "voucher_name_3": cs.VOUCHER_PRIORITY_UNLIMITED_NAME,
            "voucher_name_4": cs.VOUCHER_PRIORITY_UNLIMITED_60_MIN_NAME,
            "barcode_1": config.get(cs.ENVIRONMENT, 'BARCODE_1'),
            "barcode_2": config.get(cs.ENVIRONMENT, 'BARCODE_2'),
            "barcode_3": config.get(cs.ENVIRONMENT, 'BARCODE_3'),
            "barcode_4": config.get(cs.ENVIRONMENT, 'BARCODE_4'),
            "small_leeds_1": cs.SMALL_LEEDS_1,
            "medium_leeds_1": cs.MEDIUM_LEEDS_1,
            "voucher_leeds_1": cs.VOUCHER_LEEDS_1,
            "background_image_1": cs.BACKGROUND_LEEDS_1
        }
        one_time_use_dict = {
            "voucher_name_5": cs.VOUCHER_ONE_TIME_USE_NAME,
            "voucher_name_6": cs.VOUCHER_ONE_TIME_USE_60_MIN_NAME,
            "voucher_name_7": cs.VOUCHER_PRIORITY_ONE_TIME_USE_NAME,
            "voucher_name_8": cs.VOUCHER_PRIORITY_ONE_TIME_USE_60_MIN_NAME,
            "barcode_5": config.get(cs.ENVIRONMENT, 'BARCODE_5'),
            "barcode_6": config.get(cs.ENVIRONMENT, 'BARCODE_6'),
            "barcode_7": config.get(cs.ENVIRONMENT, 'BARCODE_7'),
            "barcode_8": config.get(cs.ENVIRONMENT, 'BARCODE_8'),
            "small_leeds_2": cs.SMALL_LEEDS_2,
            "medium_leeds_2": cs.MEDIUM_LEEDS_2,
            "voucher_leeds_2": cs.VOUCHER_LEEDS_2,
            "background_image_2": cs.BACKGROUND_LEEDS_2
        }
        redeemable_once_per_day_dict = {
            "voucher_name_9": cs.VOUCHER_REDEEMABLE_ONCE_PER_DAY_NAME,
            "voucher_name_10": cs.VOUCHER_REDEEMABLE_ONCE_PER_DAY_60_MIN_NAME,
            "voucher_name_11": cs.VOUCHER_PRIORITY_REDEEMABLE_ONCE_PER_DAY_NAME,
            "voucher_name_12": cs.VOUCHER_PRIORITY_REDEEMABLE_ONCE_PER_DAY_60_MIN_NAME,
            "barcode_9": config.get(cs.ENVIRONMENT, 'BARCODE_9'),
            "barcode_10": config.get(cs.ENVIRONMENT, 'BARCODE_10'),
            "barcode_11": config.get(cs.ENVIRONMENT, 'BARCODE_11'),
            "barcode_12": config.get(cs.ENVIRONMENT, 'BARCODE_12'),
            "small_leeds_3": cs.SMALL_LEEDS_3,
            "medium_leeds_3": cs.MEDIUM_LEEDS_3,
            "voucher_leeds_3": cs.VOUCHER_LEEDS_3,
            "background_image_3": cs.BACKGROUND_LEEDS_3
        }

        self.click_terms_of_use_conditions_list()
        if unlimited_use:
            self.click_unlimited_use_button()
            voucher_name = unlimited_use_dict['voucher_name_1']
            barcode = unlimited_use_dict['barcode_1']
            small_leeds = unlimited_use_dict['small_leeds_1']
            medium_leeds = unlimited_use_dict['medium_leeds_1']
            voucher_leeds = unlimited_use_dict['voucher_leeds_1']
            background_image = unlimited_use_dict['background_image_1']
        elif unlimited_use_60_min:
            self.click_unlimited_use_button()
            self.click_expires_in_hour_after_scan_checkbox()
            voucher_name = unlimited_use_dict['voucher_name_2']
            barcode = unlimited_use_dict['barcode_2']
            small_leeds = unlimited_use_dict['small_leeds_1']
            medium_leeds = unlimited_use_dict['medium_leeds_1']
            voucher_leeds = unlimited_use_dict['voucher_leeds_1']
            background_image = unlimited_use_dict['background_image_1']
        elif unlimited_use_priority:
            self.click_unlimited_use_button()
            self.click_priority_voucher_checkbox()
            voucher_name = unlimited_use_dict['voucher_name_3']
            barcode = unlimited_use_dict['barcode_3']
            small_leeds = unlimited_use_dict['small_leeds_1']
            medium_leeds = unlimited_use_dict['medium_leeds_1']
            voucher_leeds = unlimited_use_dict['voucher_leeds_1']
            background_image = unlimited_use_dict['background_image_1']
        elif unlimited_use_priority_60_min:
            self.click_unlimited_use_button()
            self.click_priority_voucher_checkbox()
            self.click_expires_in_hour_after_scan_checkbox()
            voucher_name = unlimited_use_dict['voucher_name_4']
            barcode = unlimited_use_dict['barcode_4']
            small_leeds = unlimited_use_dict['small_leeds_1']
            medium_leeds = unlimited_use_dict['medium_leeds_1']
            voucher_leeds = unlimited_use_dict['voucher_leeds_1']
            background_image = unlimited_use_dict['background_image_1']
        elif one_time_use:
            self.click_one_time_use_button()
            voucher_name = one_time_use_dict['voucher_name_5']
            barcode = one_time_use_dict['barcode_5']
            small_leeds = one_time_use_dict['small_leeds_2']
            medium_leeds = one_time_use_dict['medium_leeds_2']
            voucher_leeds = one_time_use_dict['voucher_leeds_2']
            background_image = one_time_use_dict['background_image_2']
        elif one_time_use_60_min:
            self.click_one_time_use_button()
            self.click_expires_in_hour_after_scan_checkbox()
            voucher_name = one_time_use_dict['voucher_name_6']
            barcode = one_time_use_dict['barcode_6']
            small_leeds = one_time_use_dict['small_leeds_2']
            medium_leeds = one_time_use_dict['medium_leeds_2']
            voucher_leeds = one_time_use_dict['voucher_leeds_2']
            background_image = one_time_use_dict['background_image_2']
        elif one_time_use_priority:
            self.click_one_time_use_button()
            self.click_priority_voucher_checkbox()
            voucher_name = one_time_use_dict['voucher_name_7']
            barcode = one_time_use_dict['barcode_7']
            small_leeds = one_time_use_dict['small_leeds_2']
            medium_leeds = one_time_use_dict['medium_leeds_2']
            voucher_leeds = one_time_use_dict['voucher_leeds_2']
            background_image = one_time_use_dict['background_image_2']
        elif one_time_use_priority_60_min:
            self.click_one_time_use_button()
            self.click_priority_voucher_checkbox()
            self.click_expires_in_hour_after_scan_checkbox()
            voucher_name = one_time_use_dict['voucher_name_8'],
            barcode = one_time_use_dict['barcode_8']
            small_leeds = one_time_use_dict['small_leeds_2']
            medium_leeds = one_time_use_dict['medium_leeds_2']
            voucher_leeds = one_time_use_dict['voucher_leeds_2']
            background_image = one_time_use_dict['background_image_2']
        elif redeemable_once_per_day:
            self.click_redeemable_once_per_day_button()
            voucher_name = redeemable_once_per_day_dict['voucher_name_9']
            barcode = redeemable_once_per_day_dict['barcode_9']
            small_leeds = redeemable_once_per_day_dict['small_leeds_3']
            medium_leeds = redeemable_once_per_day_dict['medium_leeds_3']
            voucher_leeds = redeemable_once_per_day_dict['voucher_leeds_3']
            background_image = redeemable_once_per_day_dict['background_image_3']
        elif redeemable_once_per_day_60_min:
            self.click_redeemable_once_per_day_button()
            self.click_expires_in_hour_after_scan_checkbox()
            voucher_name = redeemable_once_per_day_dict['voucher_name_10']
            barcode = redeemable_once_per_day_dict['barcode_10']
            small_leeds = redeemable_once_per_day_dict['small_leeds_3']
            medium_leeds = redeemable_once_per_day_dict['medium_leeds_3']
            voucher_leeds = redeemable_once_per_day_dict['voucher_leeds_3']
            background_image = redeemable_once_per_day_dict['background_image_3']
        elif redeemable_once_per_day_priority:
            self.click_redeemable_once_per_day_button()
            self.click_priority_voucher_checkbox()
            voucher_name = redeemable_once_per_day_dict['voucher_name_11']
            barcode = redeemable_once_per_day_dict['barcode_11']
            small_leeds = redeemable_once_per_day_dict['small_leeds_3']
            medium_leeds = redeemable_once_per_day_dict['medium_leeds_3']
            voucher_leeds = redeemable_once_per_day_dict['voucher_leeds_3']
            background_image = redeemable_once_per_day_dict['background_image_3']
        elif redeemable_once_per_day_priority_60_min:
            self.click_redeemable_once_per_day_button()
            self.click_priority_voucher_checkbox()
            self.click_expires_in_hour_after_scan_checkbox()
            voucher_name = redeemable_once_per_day_dict['voucher_name_12']
            barcode = redeemable_once_per_day_dict['barcode_12']
            small_leeds = redeemable_once_per_day_dict['small_leeds_3']
            medium_leeds = redeemable_once_per_day_dict['medium_leeds_3']
            voucher_leeds = redeemable_once_per_day_dict['voucher_leeds_3']
            background_image = redeemable_once_per_day_dict['background_image_3']
        else:
            raise Exception("Wrong parameter")

        self.input_voucher_name_field(voucher_name)
        self.input_voucher_description_field(description)
        self.input_end_date_field(voucher_end_date)
        self.input_rrp_voucher_field(rrp)
        self.input_discount_field(discount)
        self.input_barcode_field(barcode)
        self.click_select_product_button()
        self.input_product_barcode_field(barcode)
        self.click_on_product()
        self.click_voucher_targeting_button()
        self.input_voucher_store_postcode_field(postcode)
        self.click_voucher_store_search_next_button()
        self.input_voucher_store_name_field(store)
        self.click_on_voucher_store()
        self.click_save_voucher_store_button()
        self.upload_small_image(small_leeds)
        self.upload_medium_image(medium_leeds)
        self.upload_voucher_image(voucher_leeds)
        self.upload_background_image(background_image)
        self.click_save_voucher_button()
        self.click_voucher_publish_button()
        self.click_voucher_confirm_publish_button()

    def click_voucher_confirm_publish_button(self):
        voucher_confirm_publish_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.NON_PUBLISH_AR_VOUCHER_CONFIRM_PUBLISH_BUTTON))
        voucher_confirm_publish_button.click()
        time.sleep(1)

    def click_voucher_publish_button(self):
        voucher_publish_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.NON_PUBLISH_AR_VOUCHER_PUBLISH_BUTTON))
        voucher_publish_button.click()

    def click_voucher_store_search_next_button(self):
        voucher_store_search_next_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHER_STORE_SEARCH_NEXT_BUTTON))
        voucher_store_search_next_button.click()

    def click_on_product(self):
        select_product = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located(locators.SELECT_PRODUCT))
        select_product.click()

    def click_on_voucher_store(self):
        select_voucher_store = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHER_SELECT_STORE_BUTTON))
        select_voucher_store.click()

    def click_one_time_use_button(self):
        one_time_use_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ONE_TIME_USE_BUTTON))
        one_time_use_button.click()

    def click_redeemable_once_per_day_button(self):
        redeemable_once_per_day_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.REDEEMABLE_ONCE_PER_DAY))
        redeemable_once_per_day_button.click()

    def click_save_voucher_store_button(self):
        save_voucher_store_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(
            locators.VOUCHER_SAVE_STORE_BUTTON))
        save_voucher_store_button.click()

    def click_save_voucher_button(self):
        time.sleep(1)
        tap_save_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHER_SAVE_VOUCHER_BUTTON))
        tap_save_voucher_button.click()

    def click_select_product_button(self):
        select_product_button = self.driver.find_element(*locators.SELECT_PRODUCT_BUTTON)
        select_product_button.click()

    def click_voucher_targeting_button(self):
        targeting_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHER_TARGETING_BUTTON))
        targeting_button.click()

    def click_terms_of_use_conditions_list(self):
        terms_of_use_list = WebDriverWait(self.driver, 10).until(
            (EC.element_to_be_clickable(locators.TERMS_OF_USE_LIST)))
        terms_of_use_list.click()

    def click_unlimited_use_button(self):
        unlimited_use = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(locators.UNLIMITED_USE_BUTTON))
        unlimited_use.click()

    def input_barcode_field(self, barcode):
        voucher_barcode_input_field = self.driver.find_element(*locators.VOUCHER_BARCODE_INPUT_FIELD)
        voucher_barcode_input_field.send_keys(barcode)

    def input_discount_field(self, discount):
        discount_input_field = self.driver.find_element(*locators.DISCOUNT_INPUT_FILED)
        discount_input_field.send_keys(discount)

    def input_end_date_field(self, voucher_end_date):
        end_date_selection_field = self.driver.find_element(*locators.END_DATE_SELECTION)
        end_date_selection_field.send_keys(voucher_end_date)

    def input_voucher_store_postcode_field(self, postcode):
        input_voucher_postcode_field = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHER_POSTCODE_FIELD))
        input_voucher_postcode_field.send_keys(postcode)
        input_voucher_postcode_field.send_keys(Keys.RETURN)
        time.sleep(1)

    def input_product_barcode_field(self, barcode):
        select_product_input_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.SELECT_PRODUCT_INPUT_FIELD)
        )
        select_product_input_field.send_keys(barcode)

    def input_voucher_store_name_field(self, store):
        voucher_store_search_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.VOUCHER_STORE_SEARCH_FIELD))
        voucher_store_search_field.send_keys(store)
        time.sleep(1)

    def input_voucher_description_field(self, description):
        description_input_field = self.driver.find_element(*locators.VOUCHER_DESCRIPTION_INPUT_FIELD)
        description_input_field.send_keys(description)

    def input_voucher_name_field(self, voucher_name):
        name_field_input_field = self.driver.find_element(*locators.VOUCHER_NAME_FIELD_INPUT)
        name_field_input_field.send_keys(voucher_name)

    def upload_background_image(self, background_leeds):
        img_input_background = self.driver.find_element(*locators.IMG_UPLOAD_BACKGROUND)
        img_input_background.send_keys(os.getcwd() + background_leeds)

    def upload_medium_image(self, medium_leeds):
        img_input_medium = self.driver.find_element(*locators.IMG_UPLOAD_MEDIUM)
        img_input_medium.send_keys(os.getcwd() + medium_leeds)

    def upload_small_image(self, small_leeds):
        img_input_small = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IMG_UPLOAD_SMALL))
        img_input_small.send_keys(os.getcwd() + small_leeds)

    def upload_voucher_image(self, voucher_image):
        img_input_voucher = self.driver.find_element(*locators.IMG_UPLOAD_VOUCHER)
        img_input_voucher.send_keys(os.getcwd() + voucher_image)

    def click_priority_voucher_checkbox(self):
        priority_voucher_checkbox = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.PRIORITY_CHECKBOX))
        priority_voucher_checkbox.click()

    def click_expires_in_hour_after_scan_checkbox(self):
        expires_in_hour_after_scan_checkbox = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.EXPIRES_IN_HOUR_AFTER_SCAN_CHECKBOX))
        expires_in_hour_after_scan_checkbox.click()

    def input_rrp_voucher_field(self, rrp):
        rrp_voucher_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RRP_INPUT_FIELD))
        rrp_voucher_field.send_keys(rrp)
