import configparser
import time

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

action = ActionChains

config = configparser.ConfigParser()
config.read('config.ini')


class ArVoucherEditPage(BasePage):
    def edit_voucher(
            self,
            voucher_name=cs.VOUCHER_ONE_TIME_USE_NAME,
            description=cs.DESCRIPTION,
            end_date=cs.VOUCHER_END_DATE,
            rrp=cs.RRP,
            discount=cs.DISCOUNT,
            barcode=config.get(cs.ENVIRONMENT, 'BARCODE_9')
    ):
        self.input_change_voucher_name_field(voucher_name)
        self.input_change_voucher_description_field(description)
        self.input_change_voucher_end_date_field(end_date)
        self.input_change_voucher_rrp_field(rrp)
        self.input_change_voucher_discount_field(discount)
        self.input_change_voucher_barcode_field(barcode)
        self.click_voucher_save_changes_button()

    def input_change_voucher_name_field(self, voucher_name):
        change_voucher_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.VOUCHER_NAME_FIELD_INPUT))
        change_voucher_name_field.send_keys(cs.TEXT_SELECTION)
        change_voucher_name_field.send_keys(voucher_name)

    def input_change_voucher_description_field(self, description):
        change_voucher_description_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.VOUCHER_DESCRIPTION_INPUT_FIELD))
        change_voucher_description_field.send_keys(cs.TEXT_SELECTION)
        change_voucher_description_field.send_keys(description)

    def input_change_voucher_end_date_field(self, end_date):
        change_voucher_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.END_DATE_SELECTION))
        change_voucher_end_date_field.send_keys(cs.TEXT_SELECTION)
        change_voucher_end_date_field.send_keys(end_date)

    def input_change_voucher_rrp_field(self, rrp):
        change_voucher_rrp_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.RRP_INPUT_FIELD))
        change_voucher_rrp_field.send_keys(cs.TEXT_SELECTION)
        change_voucher_rrp_field.send_keys(rrp)

    def input_change_voucher_discount_field(self, discount):
        change_voucher_discount_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.DISCOUNT_INPUT_FILED))
        change_voucher_discount_field.send_keys(cs.TEXT_SELECTION)
        change_voucher_discount_field.send_keys(discount)

    def input_change_voucher_barcode_field(self, barcode):
        change_voucher_barcode_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.VOUCHER_BARCODE_INPUT_FIELD))
        change_voucher_barcode_field.send_keys(cs.TEXT_SELECTION)
        change_voucher_barcode_field.send_keys(barcode)

    def click_voucher_save_changes_button(self):
        voucher_save_changes_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHER_SAVE_EDIT_BUTTON))
        voucher_save_changes_button.click()
        time.sleep(1)
