import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class CreateInStoreSignageReportPage(BasePage):
    def create_in_store_signage_report_page(self, in_store_signage_report_name=cs.IN_STORE_SIGNAGE_REPORT_NAME):
        self.input_in_store_signage_report_name(in_store_signage_report_name)
        self.click_select_in_store_signage_campaign_button()
        self.click_select_all_in_store_signage_campaign_button()
        self.click_save_all_in_store_signage_campaign_button()
        self.click_generate_in_store_signage_report_button()

    def input_in_store_signage_report_name(self, in_store_signage_report_name):
        in_store_signage_report_name_input = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IN_STORE_SIGNAGE_REPORT_NAME_INPUT_FIELD))
        in_store_signage_report_name_input.send_keys(in_store_signage_report_name)

    def click_select_in_store_signage_campaign_button(self):
        select_in_store_signage_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_IN_STORE_SIGNAGE_CAMPAIGN_BUTTON))
        select_in_store_signage_campaign_button.click()

    def click_select_all_in_store_signage_campaign_button(self):
        select_all_in_store_signage_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_ALL_IN_STORE_SIGNAGE_CAMPAIGN_BUTTON))
        select_all_in_store_signage_campaign_button.click()

    def click_save_all_in_store_signage_campaign_button(self):
        save_all_in_store_signage_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_IN_STORE_SIGNAGE_CAMPAIGN_SAVE_BUTTON))
        save_all_in_store_signage_campaign_button.click()
        time.sleep(1)

    def click_generate_in_store_signage_report_button(self):
        generate_in_store_signage_report_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_REPORT_GENERATE_REPORT_BUTTON))
        generate_in_store_signage_report_button.click()
        time.sleep(2)
