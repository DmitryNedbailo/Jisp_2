import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class CreateConsumerAppReportPage(BasePage):
    def create_consumer_app_report(self, consumer_app_report_name=cs.CONSUMER_APP_REPORT_NAME):
        self.input_consumer_app_report_name(consumer_app_report_name)
        self.click_select_campaign_button()
        self.click_select_all_campaign_button()
        self.click_save_all_campaign_button()
        self.click_generate_consumer_app_report_button()

    def input_consumer_app_report_name(self, consumer_app_report_name):
        consumer_app_report_name_input = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CONSUMER_APP_REPORT_NAME_INPUT_FIELD))
        consumer_app_report_name_input.send_keys(consumer_app_report_name)

    def click_select_campaign_button(self):
        select_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_CONSUMER_APP_CAMPAIGN_BUTTON))
        select_campaign_button.click()

    def click_select_all_campaign_button(self):
        select_all_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_ALL_CAMPAIGN_BUTTON))
        select_all_campaign_button.click()

    def click_save_all_campaign_button(self):
        save_all_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_CAMPAIGN_SAVE_BUTTON))
        save_all_campaign_button.click()
        time.sleep(1)

    def click_generate_consumer_app_report_button(self):
        generate_consumer_app_report_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_REPORT_GENERATE_REPORT_BUTTON))
        generate_consumer_app_report_button.click()
        time.sleep(3)
