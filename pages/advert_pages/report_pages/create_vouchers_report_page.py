import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class CreateVouchersReportPage(BasePage):
    def create_vouchers_report(self, vouchers_report_name=cs.VOUCHERS_REPORT_NAME):
        self.input_vouchers_report_name(vouchers_report_name)
        self.click_select_voucher_report_button()
        self.click_select_all_vouchers_button()
        self.click_save_vouchers_button()
        self.click_generate_vouchers_report_button()

    def input_vouchers_report_name(self, vouchers_report_name):
        vouchers_report_name_input = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHERS_REPORT_NAME_FIELD_INPUT))
        vouchers_report_name_input.send_keys(vouchers_report_name)

    def click_select_voucher_report_button(self):
        select_voucher_report_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_VOUCHERS_REPORT_BUTTON))
        select_voucher_report_button.click()

    def click_select_all_vouchers_button(self):
        select_all_vouchers_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHERS_REPORT_SELECT_ALL_VOUCHERS_BUTTON))
        select_all_vouchers_button.click()

    def click_save_vouchers_button(self):
        save_vouchers_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHERS_REPORT_SAVE_VOUCHERS_BUTTON))
        save_vouchers_button.click()
        time.sleep(1)

    def click_generate_vouchers_report_button(self):
        generate_vouchers_report_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.VOUCHERS_REPORT_GENERATE_REPORT_BUTTON))
        generate_vouchers_report_button.click()
        time.sleep(2)
