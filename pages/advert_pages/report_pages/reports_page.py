from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class ReportsPage(BasePage):
    def open_ar_voucher_reports_page(self):
        self.click_ar_voucher_button()

    def open_in_store_signage_page(self):
        self.click_in_store_signage_button()

    def click_ar_voucher_button(self):
        ar_voucher_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.AR_VOUCHERS_REPORTS))
        ar_voucher_button.click()

    def click_in_store_signage_button(self):
        in_store_signage_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_REPORTS))
        in_store_signage_button.click()
