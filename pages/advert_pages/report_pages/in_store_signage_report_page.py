import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class InStoreSignageReportPage(BasePage):
    def open_create_in_store_signage_report_page(self):
        self.click_create_new_in_store_signage_report_button()

    def click_create_new_in_store_signage_report_button(self):
        time.sleep(0.5)
        create_new_in_store_report_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_NEW_IN_STORE_SIGNAGE_REPORT_BUTTON))
        create_new_in_store_report_button.click()
