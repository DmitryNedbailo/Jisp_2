from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class ConsumerAppReportPage(BasePage):
    def open_create_consumer_app_report_page(self):
        self.click_create_new_consumer_app_report_button()

    def click_create_new_consumer_app_report_button(self):
        create_new_consumer_app_report_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_NEW_CONSUMER_APP_REPORT_BUTTON))
        create_new_consumer_app_report_button.click()
