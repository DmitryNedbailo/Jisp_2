import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class ArAdvertPage(BasePage):
    def open_ar_voucher_page(self):
        self.click_ar_voucher_button()

    def open_create_ar_campaign_page(self):
        self.click_create_new_campaign_button()

    def open_first_ar_campaign_details_page(self):
        self.select_first_ar_campaign()

    def click_ar_voucher_button(self):
        ar_vouchers_button = WebDriverWait(self.driver, 10).until(
            (EC.element_to_be_clickable(locators.AR_VOUCHERS_TAB)))
        ar_vouchers_button.click()
        time.sleep(1)

    def click_create_new_campaign_button(self):
        create_new_campaign_button = WebDriverWait(self.driver, 10).until(
            (EC.element_to_be_clickable(locators.CREATE_AR_CAMPAIGNS)))
        create_new_campaign_button.click()

    def select_first_ar_campaign(self):
        first_ar_campaign = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.SELECT_FIRST_AR_CAMPAIGN))
        first_ar_campaign.click()
