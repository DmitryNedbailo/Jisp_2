import time
from random import randint

from faker import Faker
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators

fake = Faker()
password = randint(100000000000, 999999999999)
confirm_password = password

phone = randint(10000000, 99999999)


class AdvertSignUpPage(BasePage):
    def advert_sign_up(self, email=fake.email()):
        self.input_advert_first_name_field()
        self.input_advert_last_name_field()
        self.input_advert_email_address_field(email)
        self.input_advert_mobile_number_field()
        self.input_advert_password_field()
        self.input_advert_confirm_password_field()
        self.click_advert_privacy_policy_checkbox()
        self.click_advert_terms_conditions_checkbox()
        self.click_advert_confirm_sign_up_button()

    def input_advert_first_name_field(self):
        advert_first_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ADVERT_FIRST_NAME_INPUT_FIELD))
        advert_first_name_field.send_keys(fake.name())

    def input_advert_last_name_field(self):
        advert_last_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ADVERT_LAST_NAME_INPUT_FIELD))
        advert_last_name_field.send_keys(fake.name())

    def input_advert_email_address_field(self, email):
        advert_email_address_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ADVERT_EMAIL_ADDRESS_INPUT_FIELD))
        advert_email_address_field.send_keys(email)

    def input_advert_mobile_number_field(self):
        advert_mobile_number_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ADVERT_MOBILE_NUMBER_INPUT_FIELD))
        advert_mobile_number_field.send_keys(phone)

    def input_advert_password_field(self):
        advert_password_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ADVERT_PASSWORD_INPUT_FIELD))
        advert_password_field.send_keys(password)

    def input_advert_confirm_password_field(self):
        advert_confirm_password_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ADVERT_CONFIRM_PASSWORD_INPUT_FIELD))
        advert_confirm_password_field.send_keys(confirm_password)

    def click_advert_privacy_policy_checkbox(self):
        advert_privacy_policy_checkbox = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_PRIVACY_POLICY_CHECKBOX))
        advert_privacy_policy_checkbox.click()

    def click_advert_terms_conditions_checkbox(self):
        advert_terms_conditions_checkbox = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_TERMS_CONDITIONS_CHECKBOX))
        advert_terms_conditions_checkbox.click()

    def click_advert_confirm_sign_up_button(self):
        advert_confirm_sign_up_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_CONFIRM_SIGN_UP_BUTTON))
        advert_confirm_sign_up_button.click()
        time.sleep(3)
