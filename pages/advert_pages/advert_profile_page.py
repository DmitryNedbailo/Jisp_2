from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class AdvertProfilePage(BasePage):
    def open_advert_details_page_after_reg(self):
        self.click_advertiser_dashboard_button()

    def click_advertiser_dashboard_button(self):
        advertiser_dashboard_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERTISER_DASHBOARD_TAB))
        advertiser_dashboard_button.click()
