import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class AdvertPage(BasePage):
    def open_ar_advert_page(self):
        self.click_ar_advert_button()

    def open_reports_page(self):
        self.click_reports_button()

    def open_advert_guide_page(self):
        self.click_advert_guide_button()

    def open_competition_prize_page(self):
        self.click_scan_and_win_button()

    def open_product_card_effects(self):
        self.click_product_card_effects_subtab()

    def open_in_store_formats(self):
        self.click_in_store_formats_subtab()

    def click_ar_advert_button(self):
        time.sleep(2)
        ar_advert_tab = WebDriverWait(self.driver, 10).until((EC.element_to_be_clickable(locators.AR_ADVERT_TAB)))
        ar_advert_tab.click()

    def click_reports_button(self):
        reports_button = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable(locators.REPORTS_TAB))
        reports_button.click()

    def click_advert_guide_button(self):
        time.sleep(2)
        advert_guide_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_GUIDE_TAB))
        advert_guide_button.click()
        time.sleep(3)

    def click_scan_and_win_button(self):
        scan_and_win_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SCAN_AND_WIN_TAB))
        scan_and_win_button.click()

    def click_product_card_effects_subtab(self):
        product_card_effects_subtab = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ADVERT_GUIDE_PRODUCT_CARD_EFFECTS_SUBTAB))
        product_card_effects_subtab.click()

    def click_in_store_formats_subtab(self):
        in_store_formats_subtab = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ADVERT_GUIDE_IN_STORE_FORMATS_SUBTAB))
        in_store_formats_subtab.click()
