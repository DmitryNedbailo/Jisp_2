import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs


class AdvertFillingDataCompanyPage(BasePage):
    def create_new_advert_company(self, company_name=cs.NEW_ADVERT_COMPANY_NAME):
        self.input_advert_company_name_field(company_name)
        self.click_save_advert_company_button()

    def input_advert_company_name_field(self, company_name):
        advert_company_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ADVERT_COMPANY_DETAILS_FIELD))
        advert_company_name_field.send_keys(company_name)

    def click_save_advert_company_button(self):
        save_advert_company_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_COMPANY_SAVE_BUTTON))
        save_advert_company_button.click()
        time.sleep(3)
