import configparser
import os
import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


class CreateInStoreSignageAdvertPage(BasePage):
    def create_in_store_signage_advert(
            self,
            in_store_signage_advert_name=cs.IN_STORE_SIGNAGE_ADVERT_NAME,
            in_store_signage_advert_end_date=cs.IN_STORE_SIGNAGE_ADVERT_END_DATE,
            in_store_signage_advert_barcode=config.get(cs.ENVIRONMENT, 'BARCODE_1'),
            in_store_signage_advert_full_screen_video=cs.IN_STORE_SIGNAGE_ADVERT_FULL_SCREEN_VIDEO,
            in_store_signage_advert_postcode=config.get(cs.ENVIRONMENT, 'POSTCODE'),
            in_store_signage_advert_store=config.get(cs.ENVIRONMENT, 'STORE')

    ):
        self.input_in_store_signage_advert_name_field(in_store_signage_advert_name)
        self.input_in_store_signage_advert_end_date_field(in_store_signage_advert_end_date)
        self.click_in_store_signage_advert_select_product_button()
        self.input_in_store_signage_advert_search_product_field(in_store_signage_advert_barcode)
        self.click_in_store_signage_advert_select_product()
        self.upload_in_store_signage_advert_full_screen_video_button(in_store_signage_advert_full_screen_video)
        self.click_in_store_signage_advert_targeting_list()
        self.click_in_store_signage_advert_distance_targeting_button()
        self.click_in_store_signage_advert_add_store_button()
        self.input_in_store_signage_advert_postcode_field(in_store_signage_advert_postcode)
        self.click_in_store_signage_advert_search_store_button()
        self.click_in_store_signage_advert_next_button()
        self.input_in_store_signage_advert_select_stores_field(in_store_signage_advert_store)
        self.click_in_store_signage_advert_select_store_button()
        self.click_in_store_signage_advert_save_store_button()
        self.click_in_store_signage_advert_create_advert_button()

    def input_in_store_signage_advert_name_field(self, in_store_signage_advert_name):
        in_store_signage_advert_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IN_STORE_SIGNAGE_ADVERT_NAME_INPUT_FIELD))
        in_store_signage_advert_name_field.send_keys(in_store_signage_advert_name)

    def input_in_store_signage_advert_end_date_field(self, in_store_signage_advert_end_date):
        in_store_signage_advert_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IN_STORE_SIGNAGE_ADVERT_END_DATE))
        in_store_signage_advert_end_date_field.send_keys(in_store_signage_advert_end_date)

    def click_in_store_signage_advert_select_product_button(self):
        in_store_signage_advert_select_product_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_ADVERT_SELECT_PRODUCT_BUTTON))
        in_store_signage_advert_select_product_button.click()

    def input_in_store_signage_advert_search_product_field(self, in_store_signage_advert_barcode):
        in_store_signage_advert_search_product_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IN_STORE_SIGNAGE_ADVERT_SEARCH_PRODUCT_FIELD))
        in_store_signage_advert_search_product_field.send_keys(in_store_signage_advert_barcode)

    def click_in_store_signage_advert_select_product(self):
        in_store_signage_advert_select_product = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IN_STORE_SIGNAGE_ADVERT_SELECT_PRODUCT))
        in_store_signage_advert_select_product.click()

    def upload_in_store_signage_advert_full_screen_video_button(self, in_store_signage_advert_full_screen_video):
        in_store_signage_advert_full_screen_video_button = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IN_STORE_SIGNAGE_ADVERT_UPLOAD_VIDEO))
        in_store_signage_advert_full_screen_video_button.send_keys(
            os.getcwd() + in_store_signage_advert_full_screen_video)
        time.sleep(1)

    def click_in_store_signage_advert_targeting_list(self):
        in_store_signage_advert_targeting_list = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_ADVERT_TARGETING_LIST))
        in_store_signage_advert_targeting_list.click()

    def click_in_store_signage_advert_distance_targeting_button(self):
        in_store_signage_advert_distance_targeting_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_BUTTON))
        in_store_signage_advert_distance_targeting_button.click()

    def click_in_store_signage_advert_add_store_button(self):
        in_store_signage_advert_add_store_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_ADD_STORE_BUTTON))
        in_store_signage_advert_add_store_button.click()

    def input_in_store_signage_advert_postcode_field(self, in_store_signage_advert_postcode):
        in_store_signage_advert_postcode_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_SEARCH_STORE_FIELD))
        in_store_signage_advert_postcode_field.send_keys(in_store_signage_advert_postcode)

    def click_in_store_signage_advert_search_store_button(self):
        in_store_signage_advert_search_store_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_SEARCH_STORE_BUTTON))
        in_store_signage_advert_search_store_button.click()
        time.sleep(1)

    def click_in_store_signage_advert_next_button(self):
        in_store_signage_advert_next_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_NEXT_BUTTON))
        in_store_signage_advert_next_button.click()

    def input_in_store_signage_advert_select_stores_field(self, in_store_signage_advert_store):
        in_store_signage_advert_select_stores_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_SELECT_STORES_FIELD))
        in_store_signage_advert_select_stores_field.send_keys(in_store_signage_advert_store)

    def click_in_store_signage_advert_select_store_button(self):
        in_store_signage_advert_select_store_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_SELECT_STORE_BUTTON))
        in_store_signage_advert_select_store_button.click()

    def click_in_store_signage_advert_save_store_button(self):
        in_store_signage_advert_save_store_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_SAVE_STORE_BUTTON))
        in_store_signage_advert_save_store_button.click()

    def click_in_store_signage_advert_create_advert_button(self):
        in_store_signage_advert_create_advert_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_ADVERT_CREATE_ADVERT_BUTTON))
        in_store_signage_advert_create_advert_button.click()
        time.sleep(5)
