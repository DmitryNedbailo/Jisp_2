import time

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

action = ActionChains


class InStoreSignagePage(BasePage):
    def create_in_store_signage_campaign(self, in_store_signage_campaign_name=cs.IN_STORE_SIGNAGE_CAMPAIGN_NAME):
        self.click_create_in_store_signage_campaign_button()
        self.input_in_store_signage_campaign_name_field(in_store_signage_campaign_name)
        self.click_in_store_signage_campaign_confirm_create_button()

    def edit_in_store_signage_campaign(self, in_store_signage_campaign_name=cs.IN_STORE_SIGNAGE_CAMPAIGN_NAME):
        self.click_edit_in_store_signage_campaign_button()
        self.input_in_store_signage_campaign_name_field(in_store_signage_campaign_name)
        self.click_in_store_signage_campaign_confirm_create_button()

    def delete_in_store_signage_campaign(self):
        self.click_delete_in_store_signage_campaign_button()
        self.click_confirm_delete_in_store_signage_campaign_button()

    def open_in_store_signage_campaign_details_page(self):
        self.select_first_in_store_signage_campaign()

    def activate_deactivate_in_store_signage_campaign(self):
        self.click_activate_deactivate_in_store_signage_campaign_switcher()
        self.click_activate_deactivate_in_store_signage_campaign_switcher()

    def click_create_in_store_signage_campaign_button(self):
        create_in_store_signage_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_IN_STORE_SIGNAGE_CAMPAIGN_BUTTON))
        create_in_store_signage_campaign_button.click()

    def input_in_store_signage_campaign_name_field(self, in_store_signage_campaign_name):
        in_store_signage_campaign_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IN_STORE_SIGNAGE_CAMPAIGN_NAME_FIELD))
        in_store_signage_campaign_name_field.send_keys(cs.TEXT_SELECTION)
        in_store_signage_campaign_name_field.send_keys(in_store_signage_campaign_name)

    def click_in_store_signage_campaign_confirm_create_button(self):
        in_store_signage_campaign_confirm_create_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_CAMPAIGN_CREATE_CONFIRM_BUTTON))
        in_store_signage_campaign_confirm_create_button.click()
        time.sleep(1)

    def click_edit_in_store_signage_campaign_button(self):
        edit_in_store_signage_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.EDIT_IN_STORE_SIGNAGE_CAMPAIGN_BUTTON))
        edit_in_store_signage_campaign_button.click()

    def click_delete_in_store_signage_campaign_button(self):
        delete_in_store_signage_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DELETE_IN_STORE_SIGNAGE_CAMPAIGN_BUTTON))
        delete_in_store_signage_campaign_button.click()

    def click_confirm_delete_in_store_signage_campaign_button(self):
        confirm_delete_in_store_signage_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONFIRM_DELETE_IS_STORE_SIGNAGE_CAMPAIGN_BUTTON))
        confirm_delete_in_store_signage_campaign_button.click()
        time.sleep(1)

    def select_first_in_store_signage_campaign(self):
        first_in_store_signage_campaign = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_FIRST_IN_STORE_SIGNAGE_CAMPAIGN))
        first_in_store_signage_campaign.click()

    def click_activate_deactivate_in_store_signage_campaign_switcher(self):
        activate_deactivate_in_store_signage_campaign_switcher = WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(locators.ACTIVATE_DEACTIVATE_IN_STORE_SIGNAGE_CAMPAIGN_SWITCHER))
        activate_deactivate_in_store_signage_campaign_switcher.click()
        time.sleep(2)
