import time

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

action = ActionChains


class ConsumerAppFlightCampaignEditPage(BasePage):
    def edit_consumer_app_flight_campaign(
            self,
            flight_name=cs.CONSUMER_APP_FLIGHT_NAME,
            end_date=cs.CONSUMER_APP_FLIGHT_END_DATE
    ):
        self.input_change_consumer_app_flight_name_field(flight_name)
        self.input_change_consumer_app_flight_end_date_field(end_date)
        self.click_consumer_app_flight_save_changes_button()

    def input_change_consumer_app_flight_name_field(self, flight_name):
        change_consumer_app_flight_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CONSUMER_APP_FLIGHT_NAME_INPUT_FIELD))
        change_consumer_app_flight_name_field.send_keys(cs.TEXT_SELECTION)
        change_consumer_app_flight_name_field.send_keys(flight_name)

    def input_change_consumer_app_flight_end_date_field(self, end_date):
        change_consumer_app_flight_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CONSUMER_APP_FLIGHT_END_DATE_SELECTION))
        change_consumer_app_flight_end_date_field.send_keys(cs.TEXT_SELECTION)
        change_consumer_app_flight_end_date_field.send_keys(end_date)

    def click_consumer_app_flight_save_changes_button(self):
        consumer_app_flight_save_changes_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_CONFIRM_EDIT_FLIGHT_BUTTON))
        consumer_app_flight_save_changes_button.click()
        time.sleep(1)
