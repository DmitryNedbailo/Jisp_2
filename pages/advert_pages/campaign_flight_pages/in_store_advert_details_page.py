from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class InStoreAdvertDetailsPage(BasePage):
    def open_in_store_advert_edit_page(self):
        self.click_in_store_advert_edit_button()

    def delete_in_store_advert(self):
        self.click_delete_in_store_advert_button()
        self.click_confirm_delete_in_store_advert_button()

    def click_in_store_advert_edit_button(self):
        in_store_advert_edit_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.EDIT_IN_STORE_ADVERT_BUTTON))
        in_store_advert_edit_button.click()

    def click_delete_in_store_advert_button(self):
        delete_in_store_advert_button = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.DELETE_IN_STORE_ADVERT_BUTTON))
        delete_in_store_advert_button.click()

    def click_confirm_delete_in_store_advert_button(self):
        confirm_delete_in_store_advert_button = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CONFIRM_DELETE_IN_STORE_ADVERT_BUTTON))
        confirm_delete_in_store_advert_button.click()
