from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

action = ActionChains


class InStoreAdvertEditPage(BasePage):
    def edit_in_store_advert(
            self,
            in_store_advert_name=cs.IN_STORE_SIGNAGE_ADVERT_NAME,
            end_date=cs.IN_STORE_SIGNAGE_ADVERT_END_DATE
    ):
        self.input_change_in_store_advert_name_field(in_store_advert_name)
        self.input_change_in_store_advert_end_date_field(end_date)
        self.click_in_store_advert_save_changes_button()

    def input_change_in_store_advert_name_field(self, in_store_advert_name):
        change_in_store_advert_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IN_STORE_SIGNAGE_ADVERT_NAME_INPUT_FIELD))
        change_in_store_advert_name_field.send_keys(cs.TEXT_SELECTION)
        change_in_store_advert_name_field.send_keys(in_store_advert_name)

    def input_change_in_store_advert_end_date_field(self, end_date):
        change_in_store_advert_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IN_STORE_SIGNAGE_ADVERT_END_DATE))
        change_in_store_advert_end_date_field.send_keys(cs.TEXT_SELECTION)
        change_in_store_advert_end_date_field.send_keys(end_date)

    def click_in_store_advert_save_changes_button(self):
        in_store_advert_save_changes_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_ADVERT_SAVE_EDIT_ADVERT_BUTTON))
        in_store_advert_save_changes_button.click()
