import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class ConsumerAppFlightCampaignDetailsPage(BasePage):
    def open_consumer_app_flight_campaign_edit_page(self):
        self.click_consumer_app_flight_campaign_edit_button()

    def delete_consumer_app_flight_campaign(self):
        self.click_consumer_app_flight_campaign_delete_button()
        self.click_consumer_app_flight_campaign_confirm_delete_button()

    def click_consumer_app_flight_campaign_edit_button(self):
        consumer_app_flight_campaign_edit_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.EDIT_CONSUMER_APP_FLIGHT_CAMPAIGN_BUTTON))
        consumer_app_flight_campaign_edit_button.click()

    def click_consumer_app_flight_campaign_delete_button(self):
        consumer_app_flight_campaign_delete_button = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.DELETE_CONSUMER_APP_FLIGHT_CAMPAIGN_BUTTON))
        consumer_app_flight_campaign_delete_button.click()

    def click_consumer_app_flight_campaign_confirm_delete_button(self):
        consumer_app_flight_campaign_confirm_delete_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONFIRM_DELETE_CONSUMER_APP_FLIGHT_CAMPAIGN_BUTTON))
        consumer_app_flight_campaign_confirm_delete_button.click()
        time.sleep(3)
