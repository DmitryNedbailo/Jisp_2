import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class CreateNewConsumerAppFlightPage(BasePage):
    def open_create_consumer_app_flight_page(self):
        self.click_create_consumer_app_flight_button()

    def open_consumer_app_flight_campaign_details_page(self):
        self.select_first_consumer_app_flight_campaign()

    def activate_deactivate_consumer_app_flight_campaign(self):
        self.click_activate_deactivate_consumer_app_flight_campaign_button()
        self.click_activate_deactivate_consumer_app_flight_campaign_button()

    def open_consumer_app_campaign_list(self):
        self.click_consumer_app_button()

    def click_create_consumer_app_flight_button(self):
        create_consumer_app_flight_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_CONSUMER_APP_FLIGHT_BUTTON))
        create_consumer_app_flight_button.click()

    def select_first_consumer_app_flight_campaign(self):
        first_consumer_app_flight_campaign = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.SELECT_FIRST_CONSUMER_APP_FLIGHT_CAMPAIGN))
        first_consumer_app_flight_campaign.click()

    def click_activate_deactivate_consumer_app_flight_campaign_button(self):
        activate_deactivate_consumer_app_flight_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ACTIVATE_DEACTIVATE_CONSUMER_APP_FLIGHT_CAMPAIGN_SWITCHER))
        activate_deactivate_consumer_app_flight_campaign_button.click()
        time.sleep(1)

    def click_consumer_app_button(self):
        consumer_app_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_BUTTON))
        consumer_app_button.click()
