import configparser
import os
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


class CreateConsumerAppFlightPage(BasePage):
    def create_consumer_app_flight(
            self,
            consumer_app_flight_name=cs.CONSUMER_APP_FLIGHT_NAME,
            consumer_app_flight_end_date=cs.CONSUMER_APP_FLIGHT_END_DATE,
            flight_product_barcode=config.get(cs.ENVIRONMENT, 'BARCODE_1'),
            distance_targeting_postcode=config.get(cs.ENVIRONMENT, 'POSTCODE'),
            flight_store_name=config.get(cs.ENVIRONMENT, 'STORE'),
            flight_premium_image=cs.FLIGHT_PREMIUM_IMAGE,
            flight_secondary_image=cs.FLIGHT_SECONDARY_IMAGE
    ):
        self.input_consumer_app_flight_name(consumer_app_flight_name)
        self.input_consumer_app_flight_end_date(consumer_app_flight_end_date)
        self.click_select_product_category_button()
        self.input_consumer_app_flight_product_barcode(flight_product_barcode)
        self.click_on_flight_product()
        self.click_select_flight_advert()
        self.upload_homescreen_premium_image(flight_premium_image)
        self.upload_homescreen_secondary_image(flight_secondary_image)
        self.click_add_video_flight()
        self.click_geo_distance_targeting_field()
        self.click_consumer_app_flight_distance_targeting()
        self.click_select_flight_store_targeting()
        self.input_flight_distance_targeting_postcode(distance_targeting_postcode)
        self.click_flight_store_next_button()
        self.input_flight_store_name_field(flight_store_name)
        self.click_flight_store_select_button()
        self.click_flight_store_save_button()
        self.click_confirm_create_flight_button()

    def click_add_video_flight(self):
        add_video_flight_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_ADD_VIDEO_BUTTON))
        add_video_flight_button.click()

    def click_confirm_create_flight_button(self):
        confirm_create_flight_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_CONFIRM_CREATE_FLIGHT_BUTTON))
        confirm_create_flight_button.click()
        time.sleep(3)

    def click_consumer_app_flight_distance_targeting(self):
        select_distance_targeting = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_SELECT_DISTANCE_TARGETING))
        select_distance_targeting.click()

    def click_flight_store_next_button(self):
        flight_store_next_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_NEXT_STORE_TARGETING_BUTTON))
        flight_store_next_button.click()

    def click_flight_store_save_button(self):
        flight_store_save_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_SAVE_STORE_BUTTON))
        flight_store_save_button.click()

    def click_flight_store_select_button(self):
        flight_store_select_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_SELECT_STORE_BUTTON))
        flight_store_select_button.click()

    def click_geo_distance_targeting_field(self):
        geo_distance_targeting_field = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_SELECT_TARGETING))
        geo_distance_targeting_field.click()

    def click_on_flight_product(self):
        select_flight_product = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_SELECT_PRODUCT))
        select_flight_product.click()

    def click_select_flight_advert(self):
        time.sleep(0.5)
        select_flight_advert = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_SELECT_ADVERT))
        select_flight_advert.click()

    def click_select_flight_store_targeting(self):
        select_flight_store_targeting = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_SELECT_STORE_TARGETING))
        select_flight_store_targeting.click()

    def click_select_product_category_button(self):
        select_product_category_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_SELECT_PRODUCT_BUTTON))
        select_product_category_button.click()

    def input_consumer_app_flight_end_date(self, consumer_app_flight_end_date):
        input_flight_end_date = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_END_DATE_SELECTION))
        input_flight_end_date.send_keys(consumer_app_flight_end_date)

    def input_consumer_app_flight_name(self, consumer_app_flight_name):
        flight_name_field_input = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_NAME_INPUT_FIELD))
        flight_name_field_input.send_keys(consumer_app_flight_name)

    def input_consumer_app_flight_product_barcode(self, flight_product_barcode):
        input_flight_barcode = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_SELECT_PRODUCT_INPUT_FIELD))
        input_flight_barcode.send_keys(flight_product_barcode)

    def input_flight_distance_targeting_postcode(self, distance_targeting_postcode):
        flight_distance_targeting_postcode = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONSUMER_APP_FLIGHT_INPUT_STORE_NAME))
        flight_distance_targeting_postcode.send_keys(distance_targeting_postcode)
        flight_distance_targeting_postcode.send_keys(Keys.RETURN)
        time.sleep(0.5)

    def input_flight_store_name_field(self, flight_store_name):
        flight_store_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CONSUMER_APP_FLIGHT_STORE_SEARCH_FIELD))
        flight_store_name_field.send_keys(flight_store_name)

    def upload_homescreen_premium_image(self, flight_premium_image):
        homescreen_premium_image = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CONSUMER_APP_FLIGHT_UPLOAD_PREMIUM_IMAGE))
        homescreen_premium_image.send_keys(os.getcwd() + flight_premium_image)

    def upload_homescreen_secondary_image(self, flight_secondary_image):
        homescreen_secondary_image = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CONSUMER_APP_FLIGHT_UPLOAD_SECONDARY_IMAGE))
        homescreen_secondary_image.send_keys(os.getcwd() + flight_secondary_image)
