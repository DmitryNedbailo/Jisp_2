import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class InStoreSignageCampaignPage(BasePage):
    def open_create_in_store_signage_advert_page(self):
        self.click_create_in_store_signage_advert_button()

    def delete_in_store_advert(self):
        self.click_delete_in_store_advert_button()
        self.click_confirm_delete_in_store_advert_button()

    def open_in_store_advert_details_page(self):
        self.select_in_store_advert()

    def activate_deactivate_in_store_advert(self):
        self.click_activate_deactivate_in_store_advert_switcher()
        self.click_activate_deactivate_in_store_advert_switcher()

    def open_in_store_signage_campaign_list(self):
        self.click_in_store_campaign_button()

    def click_create_in_store_signage_advert_button(self):
        create_in_store_signage_advert_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_IN_STORE_SIGNAGE_ADVERT_BUTTON))
        create_in_store_signage_advert_button.click()

    def select_in_store_advert(self):
        in_store_advert = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_FIRST_IN_STORE_ADVERT))
        in_store_advert.click()

    def click_delete_in_store_advert_button(self):
        delete_in_store_advert_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DELETE_IN_STORE_ADVERT_BUTTON))
        delete_in_store_advert_button.click()

    def click_confirm_delete_in_store_advert_button(self):
        confirm_delete_in_store_advert_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONFIRM_DELETE_IN_STORE_ADVERT_BUTTON))
        confirm_delete_in_store_advert_button.click()

    def click_activate_deactivate_in_store_advert_switcher(self):
        activate_deactivate_in_store_advert_switcher = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ACTIVATE_DEACTIVATE_IN_STORE_ADVERT_SWITCHER))
        activate_deactivate_in_store_advert_switcher.click()
        time.sleep(1)

    def click_in_store_campaign_button(self):
        in_store_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.IN_STORE_SIGNAGE_CAMPAIGN_BUTTON))
        in_store_campaign_button.click()
        time.sleep(1)
