import time

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

action = ActionChains


class CampaignsPage(BasePage):
    def open_create_flight_page(self, consumer_app_name=cs.CONSUMER_APP_CAMPAIGN_NAME):
        self.click_create_new_consumer_app_campaign_button()
        self.input_consumer_app_campaign_name(consumer_app_name)
        self.click_consumer_app_campaign_create_confirm_button()

    def open_in_store_signage_page(self):
        self.click_in_store_signage_button()

    def edit_consumer_app_campaign(self, consumer_app_campaign_name=cs.CONSUMER_APP_CAMPAIGN_NAME):
        self.click_edit_consumer_app_campaign_button()
        self.input_change_consumer_app_campaign_name(consumer_app_campaign_name)
        self.click_consumer_app_campaign_create_confirm_button()
        time.sleep(2)

    def activate_deactivate_consumer_app_campaign(self):
        self.click_activate_deactivate_consumer_app_campaign_switcher()
        self.click_activate_deactivate_consumer_app_campaign_switcher()

    def open_consumer_app_campaign_details_page(self):
        self.select_first_consumer_app_campaign()

    def delete_consumer_app_campaign(self):
        self.click_delete_consumer_app_campaign_button()
        self.click_confirm_delete_consumer_app_campaign_button()

    def click_create_new_consumer_app_campaign_button(self):
        create_new_consumer_app_campaign = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_NEW_CONSUMER_APP_CAMPAIGN_BUTTON))
        create_new_consumer_app_campaign.click()

    def input_consumer_app_campaign_name(self, consumer_app_name):
        consumer_app_campaign_name = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_NEW_CONSUMER_APP_CAMPAIGN_NAME_FIELD_INPUT))
        consumer_app_campaign_name.send_keys(consumer_app_name)

    def click_consumer_app_campaign_create_confirm_button(self):
        consumer_app_campaign_create_confirm = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_CONSUMER_APP_FLIGHT_CONFIRM_BUTTON))
        consumer_app_campaign_create_confirm.click()

    def click_in_store_signage_button(self):
        in_store_signage_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.IN_STORE_SIGNAGE_BUTTON))
        in_store_signage_button.click()

    def click_edit_consumer_app_campaign_button(self):
        edit_consumer_app_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.EDIT_CONSUMER_APP_CAMPAIGN_BUTTON))
        edit_consumer_app_campaign_button.click()

    def input_change_consumer_app_campaign_name(self, consumer_app_campaign_name):
        change_consumer_app_campaign_name = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CREATE_NEW_CONSUMER_APP_CAMPAIGN_NAME_FIELD_INPUT))
        change_consumer_app_campaign_name.send_keys(cs.TEXT_SELECTION)
        change_consumer_app_campaign_name.send_keys(consumer_app_campaign_name)

    def select_first_consumer_app_campaign(self):
        first_consumer_app_campaign = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.SELECT_FIRST_CONSUMER_APP_CAMPAIGN))
        first_consumer_app_campaign.click()

    def click_activate_deactivate_consumer_app_campaign_switcher(self):
        activate_deactivate_consumer_app_campaign_switcher = WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(locators.ACTIVATE_DEACTIVATE_CONSUMER_APP_CAMPAIGN_SWITCHER))
        activate_deactivate_consumer_app_campaign_switcher.click()
        time.sleep(1)

    def click_delete_consumer_app_campaign_button(self):
        delete_consumer_app_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DELETE_CONSUMER_APP_CAMPAIGN_BUTTON))
        delete_consumer_app_campaign_button.click()

    def click_confirm_delete_consumer_app_campaign_button(self):
        confirm_delete_consumer_app_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONFIRM_DELETE_CONSUMER_APP_CAMPAIGN_BUTTON))
        confirm_delete_consumer_app_campaign_button.click()
        time.sleep(1)
