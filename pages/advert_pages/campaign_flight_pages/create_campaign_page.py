import configparser
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


class CreateArCampaignPage(BasePage):
    def create_campaign(
            self,
            campaign_name=cs.CAMPAIGN_NAME,
            campaign_start_date=cs.CAMPAIGN_START_DATE,
            campaign_end_date=cs.CAMPAIGN_END_DATE,
            campaign_store_postcode=config.get(cs.ENVIRONMENT, 'POSTCODE'),
            campaign_store_name=config.get(cs.ENVIRONMENT, 'STORE')
    ):
        self.input_campaign_name(campaign_name)
        self.input_campaign_start_date(campaign_start_date)
        self.input_campaign_end_date(campaign_end_date)
        self.click_campaign_targeting_button()
        self.input_campaign_store_postcode(campaign_store_postcode)
        self.click_campaign_store_search_next_button()
        self.input_campaign_store_name(campaign_store_name)
        self.click_on_campaign_store()
        self.click_save_campaign_store_button()
        self.click_save_campaign_button()

    def click_campaign_store_search_next_button(self):
        campaign_store_search_next_button = WebDriverWait(self.driver, 10).until(
            (EC.element_to_be_clickable(locators.CAMPAIGN_STORE_SEARCH_NEXT_BUTTON)))
        campaign_store_search_next_button.click()

    def click_campaign_targeting_button(self):
        campaign_targeting_button = WebDriverWait(self.driver, 15).until(
            (EC.element_to_be_clickable(locators.CAMPAIGN_TARGETING_BUTTON)))
        campaign_targeting_button.click()

    def click_on_campaign_store(self):
        select_campaign_store = WebDriverWait(self.driver, 10).until(
            (EC.element_to_be_clickable(locators.CAMPAIGN_SELECT_STORE_BUTTON)))
        select_campaign_store.click()

    def click_save_campaign_button(self):
        tap_save_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CAMPAIGN_SAVE_BUTTON))
        tap_save_campaign_button.click()
        time.sleep(5)

    def click_save_campaign_store_button(self):
        save_campaign_store_button = WebDriverWait(self.driver, 10).until(
            (EC.element_to_be_clickable(locators.CAMPAIGN_SAVE_STORE_BUTTON)))
        save_campaign_store_button.click()

    def input_campaign_end_date(self, campaign_end_date):
        campaign_start_date_input = self.driver.find_element(*locators.CAMPAIGN_END_DATE_SELECTION)
        campaign_start_date_input.send_keys(campaign_end_date)

    def input_campaign_name(self, campaign_name):
        name_field_input = WebDriverWait(self.driver, 10).until(
            (EC.element_to_be_clickable(locators.CAMPAIGN_NAME_FIELD_INPUT)))
        name_field_input.send_keys(campaign_name)

    def input_campaign_start_date(self, campaign_start_date):
        campaign_start_date_input = self.driver.find_element(*locators.CAMPAIGN_START_DATE_SELECTION)
        campaign_start_date_input.send_keys(campaign_start_date)

    def input_campaign_store_name(self, campaign_store_name):
        campaign_store_search = WebDriverWait(self.driver, 10).until(
            (EC.presence_of_element_located(locators.CAMPAIGN_STORE_SEARCH_FIELD)))
        campaign_store_search.send_keys(campaign_store_name)

    def input_campaign_store_postcode(self, campaign_store_postcode):
        input_campaign_postcode = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CAMPAIGN_STORE_POSTCODE_FIELD))
        input_campaign_postcode.send_keys(campaign_store_postcode)
        input_campaign_postcode.send_keys(Keys.RETURN)
        time.sleep(1)
