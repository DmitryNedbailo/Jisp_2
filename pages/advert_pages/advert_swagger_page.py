import configparser

import pytest
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


class AdvertSwaggerPage(BasePage):
    def open_and_log_in_swagger_page(self, username=config.get(cs.ENVIRONMENT, 'ADVERT_USERNAME'),
                                     password=config.get(cs.ENVIRONMENT, 'ADVERT_PASSWORD')):
        self.click_advert_authorize_button()
        self.click_advertiser_gateway_api_checkbox()
        self.click_advert_authorize_confirm_button()
        self.switch_to_second_window()
        self.input_advert_username_field(username)
        self.input_advert_password_field(password)
        self.click_log_in_advert_button()
        self.switch_to_first_window()
        self.click_advert_close_button()

    def execute_get_v1_campaigns_method(self):
        self.click_get_campaigns_method_button()
        self.click_get_campaigns_method_try_it_out_button()
        self.click_get_campaigns_method_execute_button()
        self.get_access_token()

    def click_advert_authorize_button(self):
        advert_authorize_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_AUTHORIZE_BUTTON))
        advert_authorize_button.click()

    def click_advertiser_gateway_api_checkbox(self):
        advertiser_gateway_api_checkbox = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERTISER_GATEWAY_API_CHECKBOX))
        advertiser_gateway_api_checkbox.click()

    def click_advert_authorize_confirm_button(self):
        advert_authorize_confirm_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_AUTHORIZE_CONFIRM_BUTTON))
        advert_authorize_confirm_button.click()

    def switch_to_second_window(self):
        window_2 = self.driver.window_handles[1]
        self.driver.switch_to.window(window_2)

    def input_advert_username_field(self, username):
        advert_username_field = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_USERNAME_FIELD))
        advert_username_field.send_keys(username)

    def input_advert_password_field(self, password):
        advert_password_field = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_PASSWORD_FILED))
        advert_password_field.send_keys(password)

    def click_log_in_advert_button(self):
        log_in_advert_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_LOG_IN_BUTTON))
        log_in_advert_button.click()

    def switch_to_first_window(self):
        window_1 = self.driver.window_handles[0]
        self.driver.switch_to.window(window_1)

    def click_advert_close_button(self):
        advert_close_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_AUTHORIZE_CLOSE_BUTTON))
        advert_close_button.click()

    def click_get_campaigns_method_button(self):
        get_campaigns_method_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_GET_CAMPAIGNS_METHOD_BUTTON))
        get_campaigns_method_button.click()

    def click_get_campaigns_method_try_it_out_button(self):
        get_campaigns_method_try_it_out_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_GET_CAMPAIGNS_METHOD_TRY_IT_OUT_BUTTON))
        get_campaigns_method_try_it_out_button.click()

    def click_get_campaigns_method_execute_button(self):
        get_campaigns_method_execute_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ADVERT_GET_CAMPAIGNS_METHOD_EXECUTE_BUTTON))
        get_campaigns_method_execute_button.click()

    def get_access_token(self):
        access_token = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.ADVERT_GET_TOKEN_FROM_CAMPAIGNS_METHOD))
        access_token = access_token.text.replace("Authorization: Bearer ", "")
        pytest.token = access_token
