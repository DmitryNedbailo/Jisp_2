import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class CompetitionPrizeDetailsPage(BasePage):
    def activate_deactivate_competition_prize(self):
        self.click_activate_deactivate_button()
        self.click_confirmation_activate_deactivate_button()

    def open_edit_competition_prize_page(self):
        self.click_edit_competition_prize_button()

    def click_activate_deactivate_button(self):
        activate_deactivate_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.COMPETITION_PRIZE_ACTIVATE_DEACTIVATE_BUTTON))
        activate_deactivate_button.click()

    def click_confirmation_activate_deactivate_button(self):
        confirmation_activate_deactivate_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.COMPETITION_PRIZE_CONFIRMATION_ACTIVATE_DEACTIVATE_BUTTON))
        confirmation_activate_deactivate_button.click()
        time.sleep(1)

    def click_edit_competition_prize_button(self):
        edit_competition_prize_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.COMPETITION_PRIZE_EDIT_BUTTON))
        edit_competition_prize_button.click()
