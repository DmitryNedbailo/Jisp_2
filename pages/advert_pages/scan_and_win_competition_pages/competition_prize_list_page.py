import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class CompetitionPrizeListPage(BasePage):
    def open_create_competition_prize_page(self):
        self.click_create_competition_prize_button()

    def open_first_competition_prize(self):
        self.click_first_competition_prize()

    def activate_deactivate_competition_prize(self):
        self.click_activate_deactivate_competition_prize_switcher()
        self.click_confirm_activate_deactivate_competition_prize_button()

    def delete_competition_prize(self):
        self.click_delete_competition_prize_button()
        self.click_confirm_delete_competition_prize_button()

    def click_create_competition_prize_button(self):
        create_competition_prize_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CREATE_NEW_COMPETITION_PRIZE_BUTTON))
        create_competition_prize_button.click()

    def click_first_competition_prize(self):
        first_competition_prize = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.SELECT_FIRST_COMPETITION_PRIZE))
        first_competition_prize.click()

    def click_activate_deactivate_competition_prize_switcher(self):
        deactivate_competition_prize_switcher = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.ACTIVATE_DEACTIVATE_COMPETITION_PRIZE_SWITCHER))
        deactivate_competition_prize_switcher.click()

    def click_confirm_activate_deactivate_competition_prize_button(self):
        confirm_deactivate_competition_prize_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.COMPETITION_PRIZE_CONFIRMATION_ACTIVATE_DEACTIVATE_BUTTON))
        confirm_deactivate_competition_prize_button.click()
        time.sleep(1)

    def click_delete_competition_prize_button(self):
        delete_competition_prize_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DELETE_COMPETITION_PRIZE_BUTTON))
        delete_competition_prize_button.click()

    def click_confirm_delete_competition_prize_button(self):
        confirm_delete_competition_prize_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONFIRM_DELETE_COMPETITION_PRIZE_BUTTON))
        confirm_delete_competition_prize_button.click()
        time.sleep(1)
