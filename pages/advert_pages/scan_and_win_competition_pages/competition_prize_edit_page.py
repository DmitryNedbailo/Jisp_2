import configparser
import time

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

action = ActionChains

config = configparser.ConfigParser()
config.read('config.ini')


class CompetitionPrizeEditPage(BasePage):
    def edit_competition_prize(
            self,
            competition_name=cs.COMPETITION_PRIZE_NAME,
            description=cs.DESCRIPTION,
            end_date=cs.COMPETITION_PRIZE_END_DATE,
            number_of_winners=cs.COMPETITION_PRIZE_NUMBER_OF_WINNERS,
            win_rate=cs.COMPETITION_PRIZE_WIN_RATE,
            loss_title=cs.COMPETITION_PRIZE_LOSS_TITLE,
            loss_message=cs.COMPETITION_PRIZE_LOSS_MESSAGE,
            barcode=config.get(cs.ENVIRONMENT, 'BARCODE_1')
    ):
        self.input_change_competition_prize_name_field(competition_name)
        self.input_change_competition_prize_description_field(description)
        self.input_change_competition_prize_end_date_field(end_date)
        self.input_change_competition_prize_number_of_winners_field(number_of_winners)
        self.input_change_competition_prize_win_rate_field(win_rate)
        self.input_change_competition_prize_loss_title_field(loss_title)
        self.input_change_competition_prize_loss_message_field(loss_message)
        self.input_change_competition_prize_barcode(barcode)
        self.click_competition_prize_save_changes_button()

    def input_change_competition_prize_name_field(self, competition_name):
        change_competition_prize_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_NAME_FIELD))
        change_competition_prize_name_field.send_keys(cs.TEXT_SELECTION)
        time.sleep(1)
        change_competition_prize_name_field.send_keys(competition_name)

    def input_change_competition_prize_description_field(self, description):
        change_competition_prize_description_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_DESCRIPTION_FIELD))
        change_competition_prize_description_field.send_keys(cs.TEXT_SELECTION)
        change_competition_prize_description_field.send_keys(description)

    def input_change_competition_prize_end_date_field(self, end_date):
        change_competition_prize_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_END_DATE_FIELD))
        change_competition_prize_end_date_field.send_keys(cs.TEXT_SELECTION)
        change_competition_prize_end_date_field.send_keys(end_date)

    def input_change_competition_prize_number_of_winners_field(self, number_of_winners):
        change_competition_prize_number_of_winners_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_NUMBER_OF_WINNERS_FIELD))
        change_competition_prize_number_of_winners_field.send_keys(cs.TEXT_SELECTION)
        change_competition_prize_number_of_winners_field.send_keys(number_of_winners)

    def input_change_competition_prize_win_rate_field(self, win_rate):
        change_competition_prize_win_rate_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_WIN_RATE_FIELD))
        change_competition_prize_win_rate_field.send_keys(cs.TEXT_SELECTION)
        change_competition_prize_win_rate_field.send_keys(win_rate)

    def input_change_competition_prize_loss_title_field(self, loss_title):
        change_competition_prize_loss_title_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_LOSS_TITLE_FIELD))
        change_competition_prize_loss_title_field.send_keys(cs.TEXT_SELECTION)
        change_competition_prize_loss_title_field.send_keys(loss_title)

    def input_change_competition_prize_loss_message_field(self, loss_message):
        change_competition_prize_loss_message_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_LOSS_MESSAGE_FIELD))
        change_competition_prize_loss_message_field.send_keys(cs.TEXT_SELECTION)
        change_competition_prize_loss_message_field.send_keys(loss_message)

    def input_change_competition_prize_barcode(self, barcode):
        change_competition_prize_barcode = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_SCAN_AND_WIN_BARCODE_FIELD))
        change_competition_prize_barcode.send_keys(cs.TEXT_SELECTION)
        change_competition_prize_barcode.send_keys(barcode)

    def click_competition_prize_save_changes_button(self):
        competition_prize_save_changes_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.COMPETITION_PRIZE_SAVE_BUTTON))
        competition_prize_save_changes_button.click()
        time.sleep(1)
