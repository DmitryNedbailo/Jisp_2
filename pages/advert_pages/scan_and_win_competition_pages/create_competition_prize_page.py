import configparser
import os
import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


class CreateCompetitionPrizePage(BasePage):
    def create_competition_prize(
            self,
            competition_name=cs.COMPETITION_PRIZE_NAME,
            description=cs.DESCRIPTION,
            competition_end_date=cs.COMPETITION_PRIZE_END_DATE,
            competition_number_of_winners=cs.COMPETITION_PRIZE_NUMBER_OF_WINNERS,
            competition_win_rate=cs.COMPETITION_PRIZE_WIN_RATE,
            competition_loss_title=cs.COMPETITION_PRIZE_LOSS_TITLE,
            competition_loss_message=cs.COMPETITION_PRIZE_LOSS_MESSAGE,
            competition_barcode=config.get(cs.ENVIRONMENT, 'BARCODE_1'),
            win_congratulation_image=cs.WIN_CONGRATULATION_IMAGE,
            competition_win_image=cs.WIN_COMPETITION_IMAGE,
            competition_loss_image=cs.LOSS_IMAGE,
            competition_loss_background_image=cs.LOSS_BACKGROUND_IMAGE
    ):
        self.input_competition_prize_name_field(competition_name)
        self.input_competition_prize_description_field(description)
        self.input_competition_prize_end_date_field(competition_end_date)
        self.input_competition_prize_number_of_winners(competition_number_of_winners)
        self.input_competition_prize_win_rate_field(competition_win_rate)
        self.input_competition_prize_loss_title_field(competition_loss_title)
        self.input_competition_prize_loss_message_field(competition_loss_message)
        self.input_competition_prize_scan_and_win_barcode_field(competition_barcode)
        self.upload_competition_prize_win_congratulation_image(win_congratulation_image)
        self.upload_win_competition_image(competition_win_image)
        self.upload_competition_prize_loss_image(competition_loss_image)
        self.upload_competition_prize_loss_background_image(competition_loss_background_image)
        self.click_competition_prize_save_button()

    def input_competition_prize_name_field(self, competition_name):
        competition_prize_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_NAME_FIELD))
        competition_prize_name_field.send_keys(competition_name)

    def input_competition_prize_description_field(self, description):
        competition_prize_description_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_DESCRIPTION_FIELD))
        competition_prize_description_field.send_keys(description)

    def input_competition_prize_end_date_field(self, competition_end_date):
        competition_prize_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_END_DATE_FIELD))
        competition_prize_end_date_field.send_keys(competition_end_date)

    def input_competition_prize_number_of_winners(self, competition_number_of_winners):
        competition_prize_number_of_winners = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_NUMBER_OF_WINNERS_FIELD))
        competition_prize_number_of_winners.send_keys(competition_number_of_winners)

    def input_competition_prize_win_rate_field(self, competition_win_rate):
        competition_prize_win_rate_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_WIN_RATE_FIELD))
        competition_prize_win_rate_field.send_keys(competition_win_rate)

    def input_competition_prize_loss_title_field(self, competition_loss_title):
        competition_prize_loss_title_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_LOSS_TITLE_FIELD))
        competition_prize_loss_title_field.send_keys(competition_loss_title)

    def input_competition_prize_loss_message_field(self, competition_loss_message):
        competition_prize_loss_message_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_LOSS_MESSAGE_FIELD))
        competition_prize_loss_message_field.send_keys(competition_loss_message)

    def input_competition_prize_scan_and_win_barcode_field(self, competition_barcode):
        competition_prize_scan_and_win_barcode_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_SCAN_AND_WIN_BARCODE_FIELD))
        competition_prize_scan_and_win_barcode_field.send_keys(competition_barcode)

    def upload_competition_prize_win_congratulation_image(self, win_congratulation_image):
        competition_win_congratulation_image = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_WIN_CONGRATULATION_IMAGE_UPLOAD))
        competition_win_congratulation_image.send_keys(os.getcwd() + win_congratulation_image)

    def upload_win_competition_image(self, competition_win_image):
        win_competition_image = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_WIN_COMPETITION_IMAGE_UPLOAD))
        win_competition_image.send_keys(os.getcwd() + competition_win_image)

    def upload_competition_prize_loss_image(self, competition_loss_image):
        competition_prize_loss_image = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_LOSS_IMAGE_UPLOAD))
        competition_prize_loss_image.send_keys(os.getcwd() + competition_loss_image)
        time.sleep(1)

    def upload_competition_prize_loss_background_image(self, competition_loss_background_image):
        competition_prize_loss_background_image = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.COMPETITION_PRIZE_LOSS_BACKGROUND_IMAGE_UPLOAD))
        competition_prize_loss_background_image.send_keys(os.getcwd() + competition_loss_background_image)

    def click_competition_prize_save_button(self):
        time.sleep(1)
        competition_prize_save_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.COMPETITION_PRIZE_SAVE_BUTTON))
        competition_prize_save_button.click()
