import time

from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators, constants as cs

action = ActionChains


class ArCampaignEditPage(BasePage):
    def edit_ar_campaign(
            self,
            ar_campaign_name=cs.CAMPAIGN_NAME,
            end_date=cs.CAMPAIGN_END_DATE
    ):
        self.input_change_ar_campaign_name_field(ar_campaign_name)
        self.input_change_ar_campaign_end_date_field(end_date)
        self.click_ar_campaign_save_changes_button()

    def input_change_ar_campaign_name_field(self, ar_campaign_name):
        change_ar_campaign_name_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CAMPAIGN_NAME_FIELD_INPUT))
        change_ar_campaign_name_field.send_keys(cs.TEXT_SELECTION)
        time.sleep(1)
        change_ar_campaign_name_field.send_keys(ar_campaign_name)

    def input_change_ar_campaign_end_date_field(self, end_date):
        change_ar_campaign_end_date_field = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CAMPAIGN_END_DATE_SELECTION))
        change_ar_campaign_end_date_field.send_keys(cs.TEXT_SELECTION)
        change_ar_campaign_end_date_field.send_keys(end_date)

    def click_ar_campaign_save_changes_button(self):
        ar_campaign_save_changes_button = WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located(locators.CAMPAIGN_SAVE_BUTTON))
        ar_campaign_save_changes_button.click()
