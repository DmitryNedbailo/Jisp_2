import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.general_pages.base_page import BasePage
from tests import locators


class ArCampaignDetailsPage(BasePage):
    def open_edit_ar_campaign_page(self):
        self.click_edit_ar_campaign_button()

    def delete_ar_campaign(self):
        self.click_delete_ar_campaign_button()
        self.click_confirm_delete_ar_campaign_button()

    def click_edit_ar_campaign_button(self):
        edit_ar_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.EDIT_AR_CAMPAIGN_BUTTON))
        edit_ar_campaign_button.click()

    def click_delete_ar_campaign_button(self):
        delete_ar_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.DELETE_AR_CAMPAIGN_BUTTON))
        delete_ar_campaign_button.click()

    def click_confirm_delete_ar_campaign_button(self):
        confirm_delete_ar_campaign_button = WebDriverWait(self.driver, 10).until(
            EC.element_to_be_clickable(locators.CONFIRM_DELETE_AR_CAMPAIGN_BUTTON))
        confirm_delete_ar_campaign_button.click()
        time.sleep(1)
