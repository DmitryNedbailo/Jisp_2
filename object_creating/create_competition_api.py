import configparser
import json

import requests

import example_body
from tests import constants as cs
from tests.get_token import get_token
from tests.utils import generate_uuids

config = configparser.ConfigParser()
config.read('config.ini')

random_uuids = generate_uuids(5)


def test_create_and_activate_competition_low_winrate():
    return change_competition_status(winrate_type=create_competition_win_low_winrate())


def test_create_and_activate_competition_high_winrate():
    return change_competition_status(winrate_type=create_competition_win_high_winrate())


def create_competition_win_low_winrate():
    url = config.get(cs.ENVIRONMENT, "SERVICES_URL") + "scanandwincompetitions"
    headers = {
        "accept": "text/plain",
        "Content-Type": "application/json",
        "Authorization": get_token().replace("'", "")
    }
    body = example_body.competition_low_winrate

    response = requests.post(url, json.dumps(body), headers=headers)
    return response.json()


def create_competition_win_high_winrate():
    url = config.get(cs.ENVIRONMENT, "SERVICES_URL") + "scanandwincompetitions"
    headers = {
        "accept": "text/plain",
        "Content-Type": "application/json",
        "Authorization": get_token().replace("'", "")
    }
    body = example_body.competition_high_winrate

    response = requests.post(url, json.dumps(body), headers=headers)
    return response.json()


def change_competition_status(winrate_type):
    url = config.get(cs.ENVIRONMENT, "SERVICES_URL") + "scanandwincompetitions/" + str(winrate_type) + "/changeactivity"
    headers = {
        "accept": "text/plain",
        "Content-Type": "application/json",
        "Authorization": get_token().replace("'", "")
    }
    body = {
        "isActive": True
    }

    response = requests.put(url, json.dumps(body), headers=headers)
    return response
