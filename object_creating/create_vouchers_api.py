import configparser
import json
import unittest

import requests

import example_body
from tests import constants as cs
from tests.get_token import get_token

config = configparser.ConfigParser()
config.read('config.ini')


class TestCreateVoucherApi(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        cls.headers = {
            "accept": "text/plain",
            "Content-Type": "application/json",
            "Authorization": get_token().replace("'", "")
        }

    def test_unlimited(self):
        self.publish_voucher(voucher_type=self.unlimited())

    def test_unlimited_60(self):
        self.publish_voucher(voucher_type=self.unlimited_60())

    def test_unlimited_priority(self):
        self.publish_voucher(voucher_type=self.unlimited_priority())

    def test_unlimited_priority_60(self):
        self.publish_voucher(voucher_type=self.unlimited_priority_60())

    def test_ropd(self):
        self.publish_voucher(voucher_type=self.ropd())

    def test_ropd_60(self):
        self.publish_voucher(voucher_type=self.ropd_60())

    def test_ropd_priority(self):
        self.publish_voucher(voucher_type=self.ropd_priority())

    def test_ropd_priority_60(self):
        self.publish_voucher(voucher_type=self.ropd_priority_60())

    def test_only_once(self):
        self.publish_voucher(voucher_type=self.only_once())

    def test_only_once_60(self):
        self.publish_voucher(voucher_type=self.only_once_60())

    def test_only_once_priority(self):
        self.publish_voucher(voucher_type=self.only_once_priority())

    def test_only_once_priority_60(self):
        self.publish_voucher(voucher_type=self.only_once_priority_60())

    def unlimited(self):
        body = example_body.unlimited_use_body
        response = requests.post(self.url + f"vouchers?{config.get(cs.ENVIRONMENT, 'ADVERTISER_ID')}", json.dumps(body),
                                 headers=self.headers)
        assert response.status_code == 200, "SWW"
        return response.json()["id"]

    def unlimited_60(self):
        body = example_body.unlimited_use_60_body
        response = requests.post(self.url + f"vouchers?{config.get(cs.ENVIRONMENT, 'ADVERTISER_ID')}", json.dumps(body),
                                 headers=self.headers)
        assert response.status_code == 200, "SWW"
        return response.json()["id"]

    def unlimited_priority(self):
        body = example_body.unlimited_use_priority_body
        response = requests.post(self.url + f"vouchers?{config.get(cs.ENVIRONMENT, 'ADVERTISER_ID')}", json.dumps(body),
                                 headers=self.headers)
        assert response.status_code == 200, "SWW"
        return response.json()["id"]

    def unlimited_priority_60(self):
        body = example_body.unlimited_use_priority_60_body
        response = requests.post(self.url + f"vouchers?{config.get(cs.ENVIRONMENT, 'ADVERTISER_ID')}", json.dumps(body),
                                 headers=self.headers)
        assert response.status_code == 200, "SWW"
        return response.json()["id"]

    def ropd(self):
        body = example_body.ropd_body
        response = requests.post(self.url + f"vouchers?{config.get(cs.ENVIRONMENT, 'ADVERTISER_ID')}", json.dumps(body),
                                 headers=self.headers)
        assert response.status_code == 200, "SWW"
        return response.json()["id"]

    def ropd_60(self):
        body = example_body.ropd_60_body
        response = requests.post(self.url + f"vouchers?{config.get(cs.ENVIRONMENT, 'ADVERTISER_ID')}", json.dumps(body),
                                 headers=self.headers)
        assert response.status_code == 200, "SWW"
        return response.json()["id"]

    def ropd_priority(self):
        body = example_body.ropd_priority_body
        response = requests.post(self.url + f"vouchers?{config.get(cs.ENVIRONMENT, 'ADVERTISER_ID')}", json.dumps(body),
                                 headers=self.headers)
        assert response.status_code == 200, "SWW"
        return response.json()["id"]

    def ropd_priority_60(self):
        body = example_body.ropd_priority_60_body
        response = requests.post(self.url + f"vouchers?{config.get(cs.ENVIRONMENT, 'ADVERTISER_ID')}", json.dumps(body),
                                 headers=self.headers)
        assert response.status_code == 200, "SWW"
        return response.json()["id"]

    def only_once(self):
        body = example_body.only_once_body
        response = requests.post(self.url + f"vouchers?{config.get(cs.ENVIRONMENT, 'ADVERTISER_ID')}", json.dumps(body),
                                 headers=self.headers)
        assert response.status_code == 200, "SWW"
        return response.json()["id"]

    def only_once_60(self):
        body = example_body.only_once_60_body
        response = requests.post(self.url + f"vouchers?{config.get(cs.ENVIRONMENT, 'ADVERTISER_ID')}", json.dumps(body),
                                 headers=self.headers)
        assert response.status_code == 200, "SWW"
        return response.json()["id"]

    def only_once_priority(self):
        body = example_body.only_once_priority_body
        response = requests.post(self.url + f"vouchers?{config.get(cs.ENVIRONMENT, 'ADVERTISER_ID')}", json.dumps(body),
                                 headers=self.headers)
        assert response.status_code == 200, "SWW"
        return response.json()["id"]

    def only_once_priority_60(self):
        body = example_body.only_once_priority_60_body
        response = requests.post(self.url + f"vouchers?{config.get(cs.ENVIRONMENT, 'ADVERTISER_ID')}", json.dumps(body),
                                 headers=self.headers)
        assert response.status_code == 200, "SWW"
        return response.json()["id"]

    def publish_voucher(self, voucher_type):
        response = requests.post(self.url + f"vouchers/{voucher_type}", headers=self.headers)
        assert response.status_code == 204, "SWW"
        return response
