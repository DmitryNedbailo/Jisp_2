import pytest
from selenium import webdriver

from tests.utils import environment_search, get_os_and_os_version


@pytest.fixture(scope="function")
def driver():
    environment_search()
    get_os_and_os_version()
    options = webdriver.ChromeOptions()
    options.add_argument("--window-size=1920,1080")
    options.add_argument('--headless')
    driver = webdriver.Chrome(options=options)
    driver.implicitly_wait(10)
    yield driver
    driver.quit()
