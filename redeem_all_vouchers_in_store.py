import configparser
import json

import requests

from tests import constants as cs
from tests.redemption_voucher import customer_orders_auth
from tests.utils import shopper_auth

config = configparser.ConfigParser()
config.read('config.ini')


def redeem_all_vouchers_for_shopper():
    url = config.get(cs.ENVIRONMENT, 'VOUCHER_HISTORY_URL')
    url_redemption_voucher = config.get(cs.ENVIRONMENT, 'REDEMPTION_VOUCHER_URL')
    headers = {
        "Authorization": "Bearer " + shopper_auth()
    }

    headers_redeem = {
        "Authorization": "Bearer " + customer_orders_auth()
    }

    response = requests.get(url, headers=headers)
    all_places = json.loads(response.text)['Places'][0]['Vouchers']
    for voucher in all_places:
        body = {
            "Code": voucher["RedemptionCode"]
        }

        response = requests.post(url_redemption_voucher, body, headers=headers_redeem)
        assert response.status_code == 200, "redemption_voucher: SWW"


redeem_all_vouchers_for_shopper()
