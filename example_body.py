import configparser

from tests import constants as cs
from tests.utils import generate_uuids

config = configparser.ConfigParser()
config.read('config.ini')

random_uuids = generate_uuids(5)

unlimited_use_body = {
    "title": cs.VOUCHER_UNLIMITED_NAME,
    "description": cs.DESCRIPTION,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "barcode": config.get(cs.ENVIRONMENT, 'BARCODE_3'),
    "voucherFreqCapType": 2,
    "expiresInAnHour": False,
    "isRemoteAllowed": False,
    "productId": "ff17d63a-61ab-48f6-86b3-ef5014f253b8",
    "productSKU": config.get(cs.ENVIRONMENT, 'BARCODE_3'),
    "smallIconId": "f89d665a-1aa0-460a-8847-afb16062efce",
    "iconId": "0899d5c8-06da-4e57-a36a-e3508212e3ed",
    "headerImageId": "12c6469c-b1e9-40ab-b4e3-9c8c6610a04a",
    "backgroundImageId": "a8d8df26-3da4-4033-ba6f-5a15c61a541a",
    "geoPoints": [
        {
            "id": config.get(cs.ENVIRONMENT, 'GEO_POINTS_ID'),
            "name": "2fx, London SE17 2TL, Великобритания",
            "lon": -0.09237150000000001,
            "lat": 51.4873809,
            "placeIds": [config.get(cs.ENVIRONMENT, 'PLACE_ID')],
            "distance": 2
        }
    ],
    "additionalSKUs": [],
    "discount": cs.DISCOUNT,
    "additionalConfiguration": {
        "redemptionValue": 0,
        "tapValue": 0.25,
        "redemptionShare": 0.05,
        "tapShare": 0.05,
        "jispFunded": 0
    },
    "rrp": cs.RRP
}

unlimited_use_60_body = {
    "title": cs.VOUCHER_UNLIMITED_60_MIN_NAME,
    "description": cs.DESCRIPTION,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "barcode": config.get(cs.ENVIRONMENT, 'BARCODE_10'),
    "voucherFreqCapType": 2,
    "expiresInAnHour": True,
    "isRemoteAllowed": False,
    "productId": "9737e1e8-a539-4ba8-a53a-dcc4f7116e5c",
    "productSKU": config.get(cs.ENVIRONMENT, 'BARCODE_10'),
    "smallIconId": "8ddc6f5c-e3c0-44de-ae12-bb064aa27a67",
    "iconId": "6120d02f-0c79-4862-b908-db58a63e9ddb",
    "headerImageId": "f2c40b5b-20ac-42b9-9c99-535356d9bf8e",
    "backgroundImageId": "a7f8a140-30be-4068-986a-88d9413c9b09",
    "geoPoints": [
        {
            "id": config.get(cs.ENVIRONMENT, 'GEO_POINTS_ID'),
            "name": "2fx, London SE17 2TL, Великобритания",
            "lon": -0.09237150000000001,
            "lat": 51.4873809,
            "placeIds": [config.get(cs.ENVIRONMENT, 'PLACE_ID')],
            "distance": 2
        }
    ],
    "additionalSKUs": [],
    "discount": cs.DISCOUNT,
    "additionalConfiguration": {
        "redemptionValue": 0,
        "tapValue": 0.25,
        "redemptionShare": 0.05,
        "tapShare": 0.05,
        "jispFunded": 0
    },
    "rrp": cs.RRP
}

unlimited_use_priority_body = {
    "title": cs.VOUCHER_PRIORITY_UNLIMITED_NAME,
    "description": cs.DESCRIPTION,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "barcode": config.get(cs.ENVIRONMENT, 'BARCODE_6'),
    "voucherFreqCapType": 2,
    "expiresInAnHour": False,
    "isRemoteAllowed": True,
    "productId": "7cbd08d7-b61e-4b51-9c0e-efb4d9c4e7ae",
    "productSKU": config.get(cs.ENVIRONMENT, 'BARCODE_6'),
    "smallIconId": "69f297d8-c5af-414a-9f4f-9c0369369555",
    "iconId": "4bed9eff-9622-48bc-be3d-415167d3f419",
    "headerImageId": "d95e89f8-4325-4a2b-975c-e883f07ef57e",
    "backgroundImageId": "89f36d07-99d2-44f6-8088-88666d6e75c5",
    "geoPoints": [
        {
            "id": config.get(cs.ENVIRONMENT, 'GEO_POINTS_ID'),
            "name": "2fx, London SE17 2TL, Великобритания",
            "lon": -0.09237150000000001,
            "lat": 51.4873809,
            "placeIds": [config.get(cs.ENVIRONMENT, 'PLACE_ID')],
            "distance": 2
        }
    ],
    "additionalSKUs": [],
    "discount": cs.DISCOUNT,
    "additionalConfiguration": {
        "redemptionValue": 0,
        "tapValue": 0.25,
        "redemptionShare": 0.05,
        "tapShare": 0.05,
        "jispFunded": 0
    },
    "rrp": cs.RRP
}

unlimited_use_priority_60_body = {
    "title": cs.VOUCHER_PRIORITY_UNLIMITED_60_MIN_NAME,
    "description": cs.DESCRIPTION,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "barcode": config.get(cs.ENVIRONMENT, 'BARCODE_12'),
    "voucherFreqCapType": 2,
    "expiresInAnHour": True,
    "isRemoteAllowed": True,
    "productId": "a527aec4-018f-4946-a75c-21642b6f6acd",
    "productSKU": config.get(cs.ENVIRONMENT, 'BARCODE_12'),
    "smallIconId": "f07cb1dc-c019-4c5d-8bd0-0ba86a042b1e",
    "iconId": "91d4a860-0324-4acf-ab6f-9475566757d3",
    "headerImageId": "8782f962-a03e-4fb2-88d2-6ec16f88a5c4",
    "backgroundImageId": "36ce9025-e06e-4876-b467-532b69899086",
    "geoPoints": [
        {
            "id": config.get(cs.ENVIRONMENT, 'GEO_POINTS_ID'),
            "name": "2fx, London SE17 2TL, Великобритания",
            "lon": -0.09237150000000001,
            "lat": 51.4873809,
            "placeIds": [config.get(cs.ENVIRONMENT, 'PLACE_ID')],
            "distance": 2
        }
    ],
    "additionalSKUs": [],
    "discount": cs.DISCOUNT,
    "additionalConfiguration": {
        "redemptionValue": 0,
        "tapValue": 0.25,
        "redemptionShare": 0.05,
        "tapShare": 0.05,
        "jispFunded": 0
    },
    "rrp": cs.RRP
}

ropd_body = {
    "title": cs.VOUCHER_REDEEMABLE_ONCE_PER_DAY_NAME,
    "description": cs.DESCRIPTION,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "barcode": config.get(cs.ENVIRONMENT, 'BARCODE_2'),
    "voucherFreqCapType": 1,
    "expiresInAnHour": False,
    "isRemoteAllowed": False,
    "productId": "97103e93-11c9-4ba3-bd3a-73757d833fdf",
    "productSKU": config.get(cs.ENVIRONMENT, 'BARCODE_2'),
    "smallIconId": "c171931c-bb3d-45db-93bb-4b15449d129e",
    "iconId": "d45ae634-2bbd-4a24-a8e6-110f5298ba58",
    "headerImageId": "c1e93419-b55e-4bb4-985a-04db164d95e9",
    "backgroundImageId": "1d62ed12-9ff9-4b05-9650-abe3be66926d",
    "geoPoints": [
        {
            "id": config.get(cs.ENVIRONMENT, 'GEO_POINTS_ID'),
            "name": "2fx, London SE17 2TL, Великобритания",
            "lon": -0.09237150000000001,
            "lat": 51.4873809,
            "placeIds": [config.get(cs.ENVIRONMENT, 'PLACE_ID')],
            "distance": 2
        }
    ],
    "additionalSKUs": [],
    "discount": cs.DISCOUNT,
    "additionalConfiguration": {
        "redemptionValue": 0,
        "tapValue": 0.25,
        "redemptionShare": 0.05,
        "tapShare": 0.05,
        "jispFunded": 0
    },
    "rrp": cs.RRP
}

ropd_60_body = {
    "title": cs.VOUCHER_REDEEMABLE_ONCE_PER_DAY_60_MIN_NAME,
    "description": cs.DESCRIPTION,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "barcode": config.get(cs.ENVIRONMENT, 'BARCODE_5'),
    "voucherFreqCapType": 1,
    "expiresInAnHour": True,
    "isRemoteAllowed": False,
    "productId": "bd142c1f-e0bf-497c-8fc1-a6bb89d0c6ca",
    "productSKU": config.get(cs.ENVIRONMENT, 'BARCODE_5'),
    "smallIconId": "f52665e7-4e0d-4009-9ccf-aa35dae6c7bb",
    "iconId": "4a45fd3c-4784-4e09-9c62-27b3e1df9782",
    "headerImageId": "ad79b33a-88ac-4431-b69b-dc7ea4d43938",
    "backgroundImageId": "352c1a91-9b70-4a91-8d07-f49de251657c",
    "geoPoints": [
        {
            "id": config.get(cs.ENVIRONMENT, 'GEO_POINTS_ID'),
            "name": "2fx, London SE17 2TL, Великобритания",
            "lon": -0.09237150000000001,
            "lat": 51.4873809,
            "placeIds": [config.get(cs.ENVIRONMENT, 'PLACE_ID')],
            "distance": 2
        }
    ],
    "additionalSKUs": [],
    "discount": cs.DISCOUNT,
    "additionalConfiguration": {
        "redemptionValue": 0,
        "tapValue": 0.25,
        "redemptionShare": 0.05,
        "tapShare": 0.05,
        "jispFunded": 0
    },
    "rrp": cs.RRP
}

ropd_priority_body = {
    "title": cs.VOUCHER_PRIORITY_REDEEMABLE_ONCE_PER_DAY_NAME,
    "description": cs.DESCRIPTION,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "barcode": config.get(cs.ENVIRONMENT, 'BARCODE_9'),
    "voucherFreqCapType": 1,
    "expiresInAnHour": False,
    "isRemoteAllowed": True,
    "productId": "7739ded7-b658-4278-a734-4e41a1e31eba",
    "productSKU": config.get(cs.ENVIRONMENT, 'BARCODE_9'),
    "smallIconId": "12e0ebdf-3585-41ba-84c0-85d4122bdf12",
    "iconId": "091ccea2-61da-4796-b775-47f15e6e280e",
    "headerImageId": "2f760ccc-40b9-4599-b1e5-ce213f8dc0c4",
    "backgroundImageId": "1a94853c-6efe-4391-942b-4e714cb42686",
    "geoPoints": [
        {"id": config.get(cs.ENVIRONMENT, 'GEO_POINTS_ID'),
         "name": "2fx, London SE17 2TL, Великобритания",
         "lon": -0.09237150000000001,
         "lat": 51.4873809,
         "placeIds": [config.get(cs.ENVIRONMENT, 'PLACE_ID')],
         "distance": 2}
    ],
    "additionalSKUs": [],
    "discount": cs.DISCOUNT,
    "additionalConfiguration": {
        "redemptionValue": 0,
        "tapValue": 0.25,
        "redemptionShare": 0.05,
        "tapShare": 0.05,
        "jispFunded": 0
    },
    "rrp": cs.RRP
}

ropd_priority_60_body = {
    "title": cs.VOUCHER_PRIORITY_REDEEMABLE_ONCE_PER_DAY_60_MIN_NAME,
    "description": cs.DESCRIPTION,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "barcode": config.get(cs.ENVIRONMENT, 'BARCODE_13'),
    "voucherFreqCapType": 1,
    "expiresInAnHour": True,
    "isRemoteAllowed": True,
    "productId": "d1eb9c7f-827b-4bf9-bfb6-47da66fddb33",
    "productSKU": config.get(cs.ENVIRONMENT, 'BARCODE_13'),
    "smallIconId": "29c2c30f-a1df-488d-ad87-e08cdbf9d74e",
    "iconId": "01d26e1d-5084-44b5-b02f-70dd4a2587f9",
    "headerImageId": "03fc5810-a7d6-4f25-a2f6-dda51c5622fd",
    "backgroundImageId": "135bac91-b40f-48c9-a4c2-9c5924fd94b0",
    "geoPoints": [
        {
            "id": config.get(cs.ENVIRONMENT, 'GEO_POINTS_ID'),
            "name": "2fx, London SE17 2TL, Великобритания",
            "lon": -0.09237150000000001,
            "lat": 51.4873809,
            "placeIds": [config.get(cs.ENVIRONMENT, 'PLACE_ID')],
            "distance": 2
        }
    ],
    "additionalSKUs": [],
    "discount": cs.DISCOUNT,
    "additionalConfiguration": {
        "redemptionValue": 0,
        "tapValue": 0.25,
        "redemptionShare": 0.05,
        "tapShare": 0.05,
        "jispFunded": 0
    },
    "rrp": cs.RRP
}

only_once_body = {
    "title": cs.VOUCHER_ONE_TIME_USE_NAME,
    "description": cs.DESCRIPTION,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "barcode": config.get(cs.ENVIRONMENT, 'BARCODE_11'),
    "voucherFreqCapType": 0,
    "expiresInAnHour": False,
    "isRemoteAllowed": False,
    "productId": "a5068123-a0e6-49a7-95d2-feed7f1f1fe7",
    "productSKU": config.get(cs.ENVIRONMENT, 'BARCODE_11'),
    "smallIconId": "48034958-cb3d-430c-944e-470bebb2161f",
    "iconId": "5198f7fd-1996-49f8-99bf-bc580b6e4cf6",
    "headerImageId": "d5bc6387-e5d7-4698-805a-ee102a7b2b3f",
    "backgroundImageId": "a8104a7d-7dd6-4bf5-a9bd-017ea04c9b64",
    "geoPoints": [
        {
            "id": config.get(cs.ENVIRONMENT, 'GEO_POINTS_ID'),
            "name": "2fx, London SE17 2TL, Великобритания",
            "lon": -0.09237150000000001,
            "lat": 51.4873809,
            "placeIds": [config.get(cs.ENVIRONMENT, 'PLACE_ID')],
            "distance": 2
        }
    ],
    "additionalSKUs": [],
    "discount": cs.DISCOUNT,
    "additionalConfiguration": {
        "redemptionValue": 0,
        "tapValue": 0.25,
        "redemptionShare": 0.05,
        "tapShare": 0.05,
        "jispFunded": 0
    },
    "rrp": cs.RRP
}

only_once_60_body = {
    "title": cs.VOUCHER_ONE_TIME_USE_60_MIN_NAME,
    "description": cs.DESCRIPTION,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "barcode": config.get(cs.ENVIRONMENT, 'BARCODE_8'),
    "voucherFreqCapType": 0,
    "expiresInAnHour": True,
    "isRemoteAllowed": False,
    "productId": "fa741867-80cc-456d-ab5c-7f2e823a14d5",
    "productSKU": config.get(cs.ENVIRONMENT, 'BARCODE_8'),
    "smallIconId": "3de43080-4908-461b-88fc-b4a62bb2f1ec",
    "iconId": "96803460-2acf-467d-9808-6d3b2e81ff3f",
    "headerImageId": "c8061f8d-cad3-4aae-b1e5-882ff35a3b80",
    "backgroundImageId": "d836f603-636f-426f-b3e4-4756cf8e8bcc",
    "geoPoints": [
        {
            "id": config.get(cs.ENVIRONMENT, 'GEO_POINTS_ID'),
            "name": "2fx, London SE17 2TL, Великобритания",
            "lon": -0.09237150000000001,
            "lat": 51.4873809,
            "placeIds": [config.get(cs.ENVIRONMENT, 'PLACE_ID')],
            "distance": 2
        }
    ],
    "additionalSKUs": [],
    "discount": cs.DISCOUNT,
    "additionalConfiguration": {
        "redemptionValue": 0,
        "tapValue": 0.25,
        "redemptionShare": 0.05,
        "tapShare": 0.05,
        "jispFunded": 0
    },
    "rrp": cs.RRP
}

only_once_priority_body = {
    "title": cs.VOUCHER_PRIORITY_ONE_TIME_USE_NAME,
    "description": cs.DESCRIPTION,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "barcode": config.get(cs.ENVIRONMENT, 'BARCODE_4'),
    "voucherFreqCapType": 0,
    "expiresInAnHour": False,
    "isRemoteAllowed": True,
    "productId": "4e864f0c-ab7d-43ef-a0d0-8f3b6da4b964",
    "productSKU": config.get(cs.ENVIRONMENT, 'BARCODE_4'),
    "smallIconId": "8df86130-acc5-4c5b-835b-803c5e551e04",
    "iconId": "a6e1480f-9eff-4f3b-a3ce-17a3be665c7b",
    "headerImageId": "02730b30-5837-454e-8b07-d3b6e7b6074a",
    "backgroundImageId": "b5e3cb35-75e2-4236-92db-f390ba4f9676",
    "geoPoints": [
        {
            "id": config.get(cs.ENVIRONMENT, 'GEO_POINTS_ID'),
            "name": "2fx, London SE17 2TL, Великобритания",
            "lon": -0.09237150000000001,
            "lat": 51.4873809,
            "placeIds": [
                config.get(cs.ENVIRONMENT, 'PLACE_ID')
            ],
            "distance": 2
        }
    ],
    "additionalSKUs": [],
    "discount": cs.DISCOUNT,
    "additionalConfiguration": {
        "redemptionValue": 0,
        "tapValue": 0.25,
        "redemptionShare": 0.05,
        "tapShare": 0.05,
        "jispFunded": 0
    },
    "rrp": cs.RRP
}

only_once_priority_60_body = {
    "title": cs.VOUCHER_PRIORITY_ONE_TIME_USE_60_MIN_NAME,
    "description": cs.DESCRIPTION,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "barcode": config.get(cs.ENVIRONMENT, 'BARCODE_1'),
    "voucherFreqCapType": 0,
    "expiresInAnHour": True,
    "isRemoteAllowed": True,
    "productId": "7c4f4626-4747-47eb-b10e-b4fcb3e6f493",
    "productSKU": config.get(cs.ENVIRONMENT, 'BARCODE_1'),
    "smallIconId": "aecf242e-db2d-48b9-b4e3-12adcabe83a9",
    "iconId": "5e57b12a-59ad-49d5-ab01-20f70cf4c190",
    "headerImageId": "d166c430-2662-40fb-86cc-6b634d85a3f2",
    "backgroundImageId": "a43e4736-3259-4316-87e4-811f06cae91d",
    "geoPoints": [
        {
            "id": config.get(cs.ENVIRONMENT, 'GEO_POINTS_ID'),
            "name": "2fx, London SE17 2TL, Великобритания",
            "lon": -0.09237150000000001,
            "lat": 51.4873809,
            "placeIds": [config.get(cs.ENVIRONMENT, 'PLACE_ID')],
            "distance": 2
        }
    ],
    "additionalSKUs": [],
    "discount": cs.DISCOUNT,
    "additionalConfiguration": {
        "redemptionValue": 0,
        "tapValue": 0.25,
        "redemptionShare": 0.05,
        "tapShare": 0.05,
        "jispFunded": 0
    },
    "rrp": cs.RRP
}

competition_low_winrate = {
    "title": cs.COMPETITION_PRIZE_NAME + "low",
    "description": cs.COMPETITION_PRIZE_WIN_MESSAGE,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "winAsset": {
        "imageId": random_uuids[0],
        "backgroundImageId": random_uuids[1]
    },
    "loseAsset": {
        "imageId": random_uuids[2],
        "backgroundImageId": random_uuids[3],
        "message": cs.COMPETITION_PRIZE_LOSS_MESSAGE
    },
    "maxWinners": "10",
    "winRate": "1",
    "barcodes": [random_uuids[4]],
    "lossTitle": cs.COMPETITION_PRIZE_LOSS_TITLE
}

competition_high_winrate = {
    "title": cs.COMPETITION_PRIZE_NAME + "low",
    "description": cs.COMPETITION_PRIZE_WIN_MESSAGE,
    "startDate": cs.START_DATE,
    "endDate": cs.END_DATE,
    "winAsset": {
        "imageId": random_uuids[0],
        "backgroundImageId": random_uuids[1]
    },
    "loseAsset": {
        "imageId": random_uuids[2],
        "backgroundImageId": random_uuids[3],
        "message": cs.COMPETITION_PRIZE_LOSS_MESSAGE
    },
    "maxWinners": "10",
    "winRate": "100",
    "barcodes": [random_uuids[4]],
    "lossTitle": cs.COMPETITION_PRIZE_LOSS_TITLE
}
