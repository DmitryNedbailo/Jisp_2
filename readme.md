# Getting started

***

## Technologies used in the project

- Python 3
- Selenium WebDriver
- pytest

## Preconditions for running tests

- Install plugins from the **requirements.txt**
- Mark directory as Source Root

## Running tests

To run all tests, you need to run the command in the terminal:

##### `pytest`

Tests are divided into groups:

- tests for the advertiser
- tests for the system advertiser
- tests for the retailer
- tests for the shopper

All tests have their own markers, which allows you to run them separately in groups \
Below are the markers that are available at the time of writing this file:

- smoke
- draft
- ar_vouchers
- credit_vouchers
- shopper
- retailer

To run tests that are marked with a specific marker, you need to run the command in the terminal:

##### `pytest -m "selected marker"`

For example the command `pytest -m shopper` runs all tests that are marked with a **_`shopper`_** marker

## Running tests with Allure

To run tests with allure, you need to run the command in the terminal:

#### `pytest --alluredir="new_folder_name" tests/"test_name"`

Also, you can use command with markers:

#### `pytest -m "selected marker" --alluredir="new_folder_name"`

if folder already exist, run the command below:

#### `pytest -m "selected marker" --alluredir="exist_folder"`

## Rerun tests if failed

To rerun tests which failed, use the command below:

#### `pytest --lf`

If you need to rewrite results to allure report, run the command below:

#### `pytest --lf --alluredir="new_or_exist_folder"`
