import configparser
import time

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.admin_pages.admin_advertisers_page import AdminAdvertiserPage
from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from tests import locators, constants as cs

"""
Get advertiser token
"""

config = configparser.ConfigParser()
config.read('config.ini')


def get_token():
    options = webdriver.ChromeOptions()
    options.add_argument("--window-size=1920,1080")
    options.add_argument('--headless')
    driver = webdriver.Chrome(options=options)

    advert_swagger_page = AdminLoginPage(driver)
    advert_swagger_page.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
    advert_swagger_page.sign_in_via_admin()

    admin = AdminDashboardPage(driver)
    admin.click_advertiser_tab_button()

    admin_advert_page = AdminAdvertiserPage(driver)
    admin_advert_page.log_in_via_advertiser()

    driver.get(config.get(cs.ENVIRONMENT, 'ADVERT_SWAGGER_URL'))

    advert_authorize_button = WebDriverWait(driver, 5).until(
        EC.element_to_be_clickable(locators.ADVERT_AUTHORIZE_BUTTON))
    advert_authorize_button.click()

    advertiser_gateway_api_checkbox = WebDriverWait(driver, 5).until(
        EC.element_to_be_clickable(locators.ADVERTISER_GATEWAY_API_CHECKBOX))
    advertiser_gateway_api_checkbox.click()

    advert_authorize_confirm_button = WebDriverWait(driver, 5).until(
        EC.element_to_be_clickable(locators.ADVERT_AUTHORIZE_CONFIRM_BUTTON))
    advert_authorize_confirm_button.click()

    window_2 = driver.window_handles[0]
    driver.switch_to.window(window_2)

    advert_close_button = WebDriverWait(driver, 5).until(
        EC.element_to_be_clickable(locators.ADVERT_AUTHORIZE_CLOSE_BUTTON))
    advert_close_button.click()

    get_campaigns_method_button = WebDriverWait(driver, 5).until(
        EC.element_to_be_clickable(locators.ADVERT_GET_CAMPAIGNS_METHOD_BUTTON))
    get_campaigns_method_button.click()

    get_campaigns_method_try_it_out_button = WebDriverWait(driver, 5).until(
        EC.element_to_be_clickable(locators.ADVERT_GET_CAMPAIGNS_METHOD_TRY_IT_OUT_BUTTON))
    get_campaigns_method_try_it_out_button.click()
    time.sleep(1)

    get_campaigns_method_execute_button = WebDriverWait(driver, 5).until(
        EC.element_to_be_clickable(locators.ADVERT_GET_CAMPAIGNS_METHOD_EXECUTE_BUTTON))
    get_campaigns_method_execute_button.click()

    access_token = WebDriverWait(driver, 5).until(
        EC.presence_of_element_located(locators.ADVERT_GET_TOKEN_FROM_CAMPAIGNS_METHOD))
    access_token = access_token.text.replace("Authorization: ", "")

    return access_token
