"""
Test for create Reports
"""
import configparser
import json
import time

import allure
import pytest
import requests

from pages.admin_pages.admin_advertisers_page import AdminAdvertiserPage
from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.advert_pages.advert_login_page import AdvertLoginPage
from pages.advert_pages.advert_page import AdvertPage
from pages.advert_pages.report_pages.ar_vouchers_report_page import ArVouchersReportPage
from pages.advert_pages.report_pages.consumer_app_report_page import ConsumerAppReportPage
from pages.advert_pages.report_pages.create_consumer_app_report_page import CreateConsumerAppReportPage
from pages.advert_pages.report_pages.create_in_store_signage_report_page import CreateInStoreSignageReportPage
from pages.advert_pages.report_pages.create_vouchers_report_page import CreateVouchersReportPage
from pages.advert_pages.report_pages.in_store_signage_report_page import InStoreSignageReportPage
from pages.advert_pages.report_pages.reports_page import ReportsPage
from pages.general_pages.profile_page import ProfilePage
from pages.system_advert_pages.credit_voucher_pages.system_advertiser_credit_vouchers_page import \
    SystemAdvertiserCreditVouchersPage
from pages.system_advert_pages.system_report_pages.system_advertiser_create_voucher_report_page import \
    SystemAdvertiserCreateVoucherReportPage
from pages.system_advert_pages.system_report_pages.system_advertiser_reports_page import SystemAdvertiserReportsPage
from tests import constants as cs
from tests.get_system_advert_token import get_system_advert_token
from tests.get_token import get_token

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Tests Create Reports for all Consumer App Campaigns, In-Store Signage Campaigns and AR Vouchers")
@pytest.mark.smoke
class TestCreateReports:
    @allure.title("Create Consumer App Report")
    @allure.severity("BLOCKER")
    def test_create_report_for_all_consumer_app_campaign(self, driver):
        login_page = AdvertLoginPage(driver)
        profile_page = ProfilePage(driver)
        advert_page = AdvertPage(driver)
        campaign_reports_page = ConsumerAppReportPage(driver)
        create_consumer_app_report_page = CreateConsumerAppReportPage(driver)

        consumer_app_report_name = cs.CONSUMER_APP_REPORT_NAME

        login_page.open(config.get(cs.ENVIRONMENT, 'ADVERT_LOGIN_URL'))

        login_page.sign_in_via_advert()

        profile_page.open_advert_dashboard()
        time.sleep(5)

        advert_page.open_reports_page()

        campaign_reports_page.open_create_consumer_app_report_page()

        create_consumer_app_report_page.create_consumer_app_report(
            consumer_app_report_name=consumer_app_report_name)

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "reporting", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]

        assert consumer_app_report_name in all_title, "No report"

    @allure.title("Create In-Store Signage Report")
    @allure.severity("BLOCKER")
    def test_create_report_for_all_in_store_signage(self, driver):
        login_page = AdvertLoginPage(driver)
        profile_page = ProfilePage(driver)
        advert_page = AdvertPage(driver)
        reports_page = ReportsPage(driver)
        in_store_signage_page = InStoreSignageReportPage(driver)
        create_in_store_signage_report_page = CreateInStoreSignageReportPage(driver)

        in_store_signage_report_name = cs.IN_STORE_SIGNAGE_REPORT_NAME

        login_page.open(config.get(cs.ENVIRONMENT, 'ADVERT_LOGIN_URL'))

        login_page.sign_in_via_advert()

        profile_page.open_advert_dashboard()
        time.sleep(5)

        advert_page.open_reports_page()

        reports_page.open_in_store_signage_page()

        in_store_signage_page.open_create_in_store_signage_report_page()

        create_in_store_signage_report_page.create_in_store_signage_report_page(
            in_store_signage_report_name=in_store_signage_report_name)

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "reporting?AdvertClientType=2", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]

        assert in_store_signage_report_name in all_title, "No report"

    @allure.title("Create AR Voucher Report")
    @allure.severity("BLOCKER")
    def test_create_report_for_all_vouchers(self, driver):
        login_page = AdvertLoginPage(driver)
        profile_page = ProfilePage(driver)
        advert_page = AdvertPage(driver)
        reports_page = ReportsPage(driver)
        ar_vouchers_report_page = ArVouchersReportPage(driver)
        create_vouchers_report_page = CreateVouchersReportPage(driver)

        vouchers_report_name = cs.VOUCHERS_REPORT_NAME

        login_page.open(config.get(cs.ENVIRONMENT, 'ADVERT_LOGIN_URL'))

        login_page.sign_in_via_advert()

        profile_page.open_advert_dashboard()

        time.sleep(4)
        advert_page.open_reports_page()

        reports_page.open_ar_voucher_reports_page()

        ar_vouchers_report_page.open_create_vouchers_report_page()

        create_vouchers_report_page.create_vouchers_report(vouchers_report_name=vouchers_report_name)

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "reporting/ar", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]

        assert vouchers_report_name in all_title, "No report"


@allure.epic("Tests Create Reports for Loyalty and Credit Vouchers")
@pytest.mark.smoke
class TestCreateSystemAdvertReports:
    @allure.title("Create Loyalty Voucher Report")
    @allure.severity("BLOCKER")
    def test_create_report_for_loyalty_voucher(self, driver):
        login = AdminLoginPage(driver)
        admin = AdminDashboardPage(driver)
        admin_advert_page = AdminAdvertiserPage(driver)
        system_advertiser_credit_vouchers_page = SystemAdvertiserCreditVouchersPage(driver)
        system_advertiser_reports_page = SystemAdvertiserReportsPage(driver)
        system_advertiser_create_voucher_report_page = SystemAdvertiserCreateVoucherReportPage(driver)

        voucher_report_name = cs.LOYALTY_VOUCHER_REPORT_NAME

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin.click_advertiser_tab_button()

        admin_advert_page.log_in_via_system_advertiser()
        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)
        time.sleep(5)

        system_advertiser_credit_vouchers_page.open_system_advertiser_reports_tab()

        system_advertiser_reports_page.open_system_advertiser_create_voucher_report_page()

        system_advertiser_create_voucher_report_page.create_loyalty_voucher_report(
            voucher_report_name=voucher_report_name)

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_system_advert_token().replace("'", "")
        }

        response = requests.get(url + "reporting/ar", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]

        assert voucher_report_name in all_title, "No report"

    @allure.title("Create Credit Voucher Report")
    @allure.severity("BLOCKER")
    def test_create_report_for_all_credit_vouchers(self, driver):
        login = AdminLoginPage(driver)
        admin = AdminDashboardPage(driver)
        admin_advert_page = AdminAdvertiserPage(driver)
        system_advertiser_credit_vouchers_page = SystemAdvertiserCreditVouchersPage(driver)
        system_advertiser_reports_page = SystemAdvertiserReportsPage(driver)
        system_advertiser_create_voucher_report_page = SystemAdvertiserCreateVoucherReportPage(driver)

        voucher_report_name = cs.CREDIT_VOUCHERS_REPORT_NAME

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin.click_advertiser_tab_button()

        admin_advert_page.log_in_via_system_advertiser()
        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)
        time.sleep(5)

        system_advertiser_credit_vouchers_page.open_system_advertiser_reports_tab()

        system_advertiser_reports_page.open_system_advertiser_create_voucher_report_page()

        system_advertiser_create_voucher_report_page.create_credit_vouchers_report(
            voucher_report_name=voucher_report_name)

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_system_advert_token().replace("'", "")
        }

        response = requests.get(url + "reporting/ar", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]

        assert voucher_report_name in all_title, "No report"
