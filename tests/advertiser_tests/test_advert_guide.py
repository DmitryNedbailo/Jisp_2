import configparser

import allure
import pytest
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from pages.advert_pages.advert_login_page import AdvertLoginPage
from pages.advert_pages.advert_page import AdvertPage
from pages.general_pages.profile_page import ProfilePage
from tests import constants as cs
from tests import locators

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Advert Guide")
@pytest.mark.smoke
class TestAdvertGuide:
    @allure.title("Test Advert Guide")
    @allure.severity("TRIVIAL")
    def test_advert_guide(self, driver):
        login_page = AdvertLoginPage(driver)
        profile_page = ProfilePage(driver)

        login_page.open(config.get(cs.ENVIRONMENT, 'ADVERT_LOGIN_URL'))
        login_page.sign_in_via_advert()

        profile_page.open_advert_dashboard()

        advert_page = AdvertPage(driver)

        advert_page.open_advert_guide_page()

        mobile_space_locators = [
            locators.ADVERT_GUIDE_IMAGE_PREMIUM,
            locators.ADVERT_GUIDE_IMAGE_SECONDARY,
            locators.ADVERT_GUIDE_IMAGE_STORES,
            locators.ADVERT_GUIDE_IMAGE_NATIVE,
            locators.ADVERT_GUIDE_IMAGE_OFFERS,
            locators.ADVERT_GUIDE_IMAGE_PRODUCTS
        ]

        mobile_space_pictures = [
            WebDriverWait(driver, 5).until(EC.presence_of_element_located(locator)).get_attribute('src')
            for locator in mobile_space_locators
        ]

        mobile_space_expected_urls = [
            config.get(cs.ENVIRONMENT, 'ADVERT_GUIDE_IMAGE_PREMIUM'),
            config.get(cs.ENVIRONMENT, 'ADVERT_GUIDE_IMAGE_SECONDARY'),
            config.get(cs.ENVIRONMENT, 'ADVERT_GUIDE_IMAGE_STORES'),
            config.get(cs.ENVIRONMENT, 'ADVERT_GUIDE_IMAGE_NATIVE'),
            config.get(cs.ENVIRONMENT, 'ADVERT_GUIDE_IMAGE_OFFERS'),
            config.get(cs.ENVIRONMENT, 'ADVERT_GUIDE_IMAGE_PRODUCTS')
        ]

        assert mobile_space_pictures == mobile_space_expected_urls, "Wrong picture is displayed"

        advert_page.open_product_card_effects()

        product_card_effects_locators = [
            locators.ADVERT_GUIDE_MAGPIE_SHINE,
            locators.ADVERT_GUIDE_IMAGE_ANIMATED_STROKE
        ]

        product_card_effects_pictures = [
            WebDriverWait(driver, 5).until(EC.presence_of_element_located(locator)).get_attribute('src')
            for locator in product_card_effects_locators
        ]

        product_card_effects_expected_urls = [
            config.get(cs.ENVIRONMENT, 'ADVERT_GUIDE_MAGPIE_SHINE'),
            config.get(cs.ENVIRONMENT, 'ADVERT_GUIDE_IMAGE_ANIMATED_STROKE')
        ]

        assert product_card_effects_pictures == product_card_effects_expected_urls, "Wrong picture is displayed"

        advert_page.open_in_store_formats()

        in_store_formats_locators = [
            locators.ADVERT_GUIDE_IMAGE_FULL_SCREEN,
            locators.ADVERT_GUIDE_IMAGE_FULL_HALF_SCREEN
        ]

        in_store_formats_picture = [
            WebDriverWait(driver, 5).until(EC.presence_of_element_located(locator)).get_attribute('src')
            for locator in in_store_formats_locators
        ]

        in_store_formats_expected_urls = [
            config.get(cs.ENVIRONMENT, 'ADVERT_GUIDE_IMAGE_FULL_SCREEN'),
            config.get(cs.ENVIRONMENT, 'ADVERT_GUIDE_IMAGE_FULL_HALF_SCREEN')
        ]

        assert in_store_formats_picture == in_store_formats_expected_urls, "Wrong picture is displayed"
