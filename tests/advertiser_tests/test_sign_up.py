import configparser
import json

import allure

from pages.advert_pages.advert_input_data_company_page import AdvertFillingDataCompanyPage
from pages.advert_pages.advert_login_page import AdvertLoginPage
from pages.advert_pages.advert_profile_page import AdvertProfilePage
from pages.advert_pages.advert_sign_up_page import AdvertSignUpPage
from tests.utils import change_advert_status, check_advert_status

"""
Test for advertiser sign up
"""
from tests import constants as cs

import pytest

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Advertiser Sign Up")
@pytest.mark.smoke
class TestSignUp:
    @allure.title("Advertiser Sign Up")
    @allure.severity("BLOCKER")
    def test_sign_up(self, driver):
        advert_login_page = AdvertLoginPage(driver)
        advert_sign_up_page = AdvertSignUpPage(driver)
        advert_profile_page = AdvertProfilePage(driver)
        advert_input_data_company_page = AdvertFillingDataCompanyPage(driver)

        advert_login_page.open(config.get(cs.ENVIRONMENT, 'ADVERT_LOGIN_URL'))
        advert_login_page.open_sign_up_page()

        advert_sign_up_page.advert_sign_up()

        advert_profile_page.open_advert_details_page_after_reg()

        advert_input_data_company_page.create_new_advert_company()

        change_status = change_advert_status()

        assert change_status.status_code == 200, "change_advert_status: SWW"

        check_status = check_advert_status()

        assert check_status.status_code == 200, "check_advert_status: SWW"
        assert json.loads(check_status.text)["Items"][-1]["AdvertiserStatus"] == 1, "Advertiser isn't verified"
