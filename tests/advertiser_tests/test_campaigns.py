"""
Tests for Campaigns
"""
import configparser
import json

import allure
import pytest
import requests

from pages.advert_pages.advert_login_page import AdvertLoginPage
from pages.advert_pages.advert_page import AdvertPage
from pages.advert_pages.ar_advert_page import ArAdvertPage
from pages.advert_pages.ar_campaign_details_page import ArCampaignDetailsPage
from pages.advert_pages.ar_campaign_edit_page import ArCampaignEditPage
from pages.advert_pages.campaign_flight_pages.campaigns_page import CampaignsPage
from pages.advert_pages.campaign_flight_pages.consumer_app_flight_campaign_details_page import \
    ConsumerAppFlightCampaignDetailsPage
from pages.advert_pages.campaign_flight_pages.consumer_app_flight_campaign_edit_page import \
    ConsumerAppFlightCampaignEditPage
from pages.advert_pages.campaign_flight_pages.create_campaign_page import CreateArCampaignPage
from pages.advert_pages.campaign_flight_pages.create_consumer_app_flight_page import CreateConsumerAppFlightPage
from pages.advert_pages.campaign_flight_pages.create_in_store_signage_advert_page import CreateInStoreSignageAdvertPage
from pages.advert_pages.campaign_flight_pages.create_new_consumer_app_flight_page import CreateNewConsumerAppFlightPage
from pages.advert_pages.campaign_flight_pages.in_store_advert_details_page import InStoreAdvertDetailsPage
from pages.advert_pages.campaign_flight_pages.in_store_advert_edit_page import InStoreAdvertEditPage
from pages.advert_pages.campaign_flight_pages.in_store_signage_campaign_page import InStoreSignageCampaignPage
from pages.advert_pages.campaign_flight_pages.in_store_signage_page import InStoreSignagePage
from pages.general_pages.profile_page import ProfilePage
from tests import constants as cs
from tests.get_token import get_token

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Tests for Create/Edit/Delete AR Campaign")
@pytest.mark.smoke
class TestArCampaign:
    @pytest.fixture(autouse=True)
    def _init_pages(self, driver):
        login_page = AdvertLoginPage(driver)
        profile_page = ProfilePage(driver)
        advert_page = AdvertPage(driver)

        login_page.open(config.get(cs.ENVIRONMENT, 'ADVERT_LOGIN_URL'))

        login_page.sign_in_via_advert()

        profile_page.open_advert_dashboard()

        advert_page.open_ar_advert_page()

    @allure.title("Create AR Campaign")
    @allure.severity("BLOCKER")
    def test_create_ar_campaign(self, driver):
        ar_advert_page = ArAdvertPage(driver)
        create_campaign_page = CreateArCampaignPage(driver)

        campaign_name = cs.CAMPAIGN_NAME

        ar_advert_page.click_create_new_campaign_button()

        create_campaign_page.create_campaign(campaign_name=campaign_name)

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "vouchercampaigns?IsCompleted=&Limit=20&Offset=0", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]

        assert campaign_name in all_title, "No campaign"

    @allure.title("Edit AR Campaign")
    @allure.severity("CRITICAL")
    def test_edit_ar_campaign(self, driver):
        ar_advert_page = ArAdvertPage(driver)
        ar_campaign_details_page = ArCampaignDetailsPage(driver)
        ar_campaign_edit_page = ArCampaignEditPage(driver)

        campaign_name = cs.CAMPAIGN_NAME

        ar_advert_page.open_first_ar_campaign_details_page()

        ar_campaign_details_page.open_edit_ar_campaign_page()

        ar_campaign_edit_page.edit_ar_campaign(ar_campaign_name=campaign_name)

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "vouchercampaigns?IsCompleted=&Limit=20&Offset=0", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]

        assert campaign_name in all_title, "No campaign"

    @allure.title("Delete AR Campaign")
    @allure.severity("CRITICAL")
    def test_delete_ar_campaign(self, driver):
        ar_advert_page = ArAdvertPage(driver)
        ar_campaign_details_page = ArCampaignDetailsPage(driver)

        ar_advert_page.open_first_ar_campaign_details_page()

        ar_campaign_details_page.delete_ar_campaign()

    @allure.title("Create AR Campaign 2")
    @allure.severity("BLOCKER")
    def test_create_ar_campaign_2(self, driver):
        self.test_create_ar_campaign(driver)


@allure.epic("Tests for Create/Edit/Activate-Deactivate/Delete Consumer App Campaign")
@pytest.mark.smoke
class TestConsumerAppCampaign:
    @pytest.fixture(autouse=True)
    def _init_pages(self, driver):
        login_page = AdvertLoginPage(driver)
        profile_page = ProfilePage(driver)

        login_page.open(config.get(cs.ENVIRONMENT, 'ADVERT_LOGIN_URL'))

        login_page.sign_in_via_advert()

        profile_page.open_advert_dashboard()

    @allure.title("Create Consumer App Campaign")
    @allure.severity("BLOCKER")
    def test_create_consumer_app_campaign(self, driver):
        campaigns_page = CampaignsPage(driver)
        create_new_consumer_app_flight_page = CreateNewConsumerAppFlightPage(driver)
        create_consumer_app_flight_page = CreateConsumerAppFlightPage(driver)

        consumer_app_name = cs.CONSUMER_APP_CAMPAIGN_NAME

        campaigns_page.open_create_flight_page(consumer_app_name=consumer_app_name)

        create_new_consumer_app_flight_page.open_create_consumer_app_flight_page()

        create_consumer_app_flight_page.create_consumer_app_flight()

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "campaigns", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]

        assert consumer_app_name in all_title, "No campaign"

    @allure.title("Edit Consumer App Campaign")
    @allure.severity("CRITICAL")
    def test_edit_consumer_app_campaign(self, driver):
        campaigns_page = CampaignsPage(driver)
        create_new_consumer_app_flight_page = CreateNewConsumerAppFlightPage(driver)
        consumer_app_flight_campaign_details_page = ConsumerAppFlightCampaignDetailsPage(driver)
        consumer_app_flight_campaign_edit_page = ConsumerAppFlightCampaignEditPage(driver)

        consumer_app_name = cs.CONSUMER_APP_CAMPAIGN_NAME

        campaigns_page.edit_consumer_app_campaign(consumer_app_campaign_name=consumer_app_name)
        campaigns_page.open_consumer_app_campaign_details_page()

        create_new_consumer_app_flight_page.open_consumer_app_flight_campaign_details_page()

        consumer_app_flight_campaign_details_page.open_consumer_app_flight_campaign_edit_page()

        consumer_app_flight_campaign_edit_page.edit_consumer_app_flight_campaign()

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "campaigns", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]

        assert consumer_app_name in all_title, "No campaign"

    @allure.title("Activate-Deactivate Consumer App Campaign")
    @allure.severity("CRITICAL")
    def test_activate_deactivate_consumer_app_campaign(self, driver):
        campaigns_page = CampaignsPage(driver)
        create_new_consumer_app_flight_page = CreateNewConsumerAppFlightPage(driver)

        campaigns_page.activate_deactivate_consumer_app_campaign()
        campaigns_page.open_consumer_app_campaign_details_page()

        create_new_consumer_app_flight_page.activate_deactivate_consumer_app_flight_campaign()

    @allure.title("Edit Consumer App Campaign")
    @allure.severity("CRITICAL")
    def test_delete_consumer_app_campaign(self, driver):
        campaigns_page = CampaignsPage(driver)
        create_new_consumer_app_flight_page = CreateNewConsumerAppFlightPage(driver)
        consumer_app_flight_campaign_details_page = ConsumerAppFlightCampaignDetailsPage(driver)

        campaigns_page.open_consumer_app_campaign_details_page()

        create_new_consumer_app_flight_page.open_consumer_app_flight_campaign_details_page()

        consumer_app_flight_campaign_details_page.delete_consumer_app_flight_campaign()

        create_new_consumer_app_flight_page.open_consumer_app_campaign_list()

        campaigns_page.delete_consumer_app_campaign()

    @allure.title("Create Consumer App Campaign 2")
    @allure.severity("BLOCKER")
    def test_create_consumer_app_campaign_2(self, driver):
        self.test_create_consumer_app_campaign(driver)


@allure.epic("Tests for Create/Edit/Activate-Deactivate/Delete In-Store Signage Campaign")
@pytest.mark.smoke
class TestInStoreSignageCampaign:
    @pytest.fixture(autouse=True)
    def _init_pages(self, driver):
        login_page = AdvertLoginPage(driver)
        profile_page = ProfilePage(driver)
        campaigns_page = CampaignsPage(driver)

        login_page.open(config.get(cs.ENVIRONMENT, 'ADVERT_LOGIN_URL'))

        login_page.sign_in_via_advert()

        profile_page.open_advert_dashboard()

        campaigns_page.open_in_store_signage_page()

    @allure.title("Create In-Store Signage Campaign")
    @allure.severity("BLOCKER")
    def test_create_new_in_store_signage_campaign(self, driver):
        in_store_signage_page = InStoreSignagePage(driver)
        in_store_signage_campaign_page = InStoreSignageCampaignPage(driver)
        in_store_signage_create_advert_page = CreateInStoreSignageAdvertPage(driver)

        in_store_signage_campaign_name = cs.IN_STORE_SIGNAGE_CAMPAIGN_NAME

        in_store_signage_page.create_in_store_signage_campaign(
            in_store_signage_campaign_name=in_store_signage_campaign_name)

        in_store_signage_campaign_page.open_create_in_store_signage_advert_page()

        in_store_signage_create_advert_page.create_in_store_signage_advert()

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "campaigns?AdvertClientType=2", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]

        assert in_store_signage_campaign_name in all_title, "No in store campaign"

    @allure.title("Edit In-Store Signage Campaign")
    @allure.severity("CRITICAL")
    def test_edit_in_store_signage_campaign(self, driver):
        in_store_signage_page = InStoreSignagePage(driver)
        in_store_signage_campaign_page = InStoreSignageCampaignPage(driver)
        in_store_advert_details_page = InStoreAdvertDetailsPage(driver)
        in_store_advert_edit_page = InStoreAdvertEditPage(driver)

        in_store_signage_campaign_name = cs.IN_STORE_SIGNAGE_CAMPAIGN_NAME

        in_store_signage_page.edit_in_store_signage_campaign(
            in_store_signage_campaign_name=in_store_signage_campaign_name)
        in_store_signage_page.open_in_store_signage_campaign_details_page()

        in_store_signage_campaign_page.open_in_store_advert_details_page()

        in_store_advert_details_page.open_in_store_advert_edit_page()

        in_store_advert_edit_page.edit_in_store_advert()

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "campaigns?AdvertClientType=2", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]

        assert in_store_signage_campaign_name in all_title, "No in store campaign"

    @allure.title("Activate-Deactivate In-Store Signage Campaign")
    @allure.severity("CRITICAL")
    def test_activate_deactivate_in_store_signage_campaign(self, driver):
        in_store_signage_page = InStoreSignagePage(driver)
        in_store_signage_campaign_page = InStoreSignageCampaignPage(driver)

        in_store_signage_page.activate_deactivate_in_store_signage_campaign()
        in_store_signage_page.open_in_store_signage_campaign_details_page()

        in_store_signage_campaign_page.activate_deactivate_in_store_advert()

    @allure.title("Delete In-Store Signage Campaign")
    @allure.severity("CRITICAL")
    def test_delete_in_store_signage_campaign(self, driver):
        in_store_signage_page = InStoreSignagePage(driver)
        in_store_signage_campaign_page = InStoreSignageCampaignPage(driver)
        in_store_advert_details_page = InStoreAdvertDetailsPage(driver)
        campaigns_page = CampaignsPage(driver)

        in_store_signage_page.open_in_store_signage_campaign_details_page()

        in_store_signage_campaign_page.open_in_store_advert_details_page()

        in_store_advert_details_page.delete_in_store_advert()

        in_store_signage_campaign_page.open_in_store_signage_campaign_list()

        campaigns_page.open_in_store_signage_page()

        in_store_signage_page.delete_in_store_signage_campaign()

    @allure.title("Create In-Store Signage Campaign")
    @allure.severity("BLOCKER")
    def test_create_new_in_store_signage_campaign_2(self, driver):
        self.test_create_new_in_store_signage_campaign(driver)
