"""
Tests for create AR Vouchers and Credit Vouchers
- scan + add to a wallet as a shopper
- redemption voucher as a retailer
"""
import configparser
import json
import time

import allure
import pytest
import requests

from pages.admin_pages.admin_advertisers_page import AdminAdvertiserPage
from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.advert_pages.advert_login_page import AdvertLoginPage
from pages.advert_pages.advert_page import AdvertPage
from pages.advert_pages.ar_advert_page import ArAdvertPage
from pages.advert_pages.voucher_pages.ar_voucher_details_page import ArVoucherDetailsPage
from pages.advert_pages.voucher_pages.ar_voucher_edit_page import ArVoucherEditPage
from pages.advert_pages.voucher_pages.ar_vouchers_page import ArVouchersPage
from pages.advert_pages.voucher_pages.create_voucher_page import CreateVoucherPage
from pages.general_pages.profile_page import ProfilePage
from pages.system_advert_pages.credit_voucher_pages.system_advertiser_create_credit_voucher_page import \
    SystemAdvertiserCreateCreditVoucherPage
from pages.system_advert_pages.credit_voucher_pages.system_advertiser_credit_voucher_details_page import \
    SystemAdvertiserCreditVoucherDetailsPage
from pages.system_advert_pages.credit_voucher_pages.system_advertiser_credit_voucher_edit_page import \
    SystemAdvertiserCreditVoucherEditPage
from pages.system_advert_pages.credit_voucher_pages.system_advertiser_credit_vouchers_page import \
    SystemAdvertiserCreditVouchersPage
from tests import constants as cs
from tests.get_system_advert_token import get_system_advert_token
from tests.redemption_voucher import redemption_voucher

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Tests for Create AR Vouchers")
@pytest.mark.smoke
class TestCreateARVouchers:
    @pytest.fixture(autouse=True)
    def _init_pages(self, driver):
        login_page = AdvertLoginPage(driver)
        profile_page = ProfilePage(driver)
        advert_page = AdvertPage(driver)
        ar_advert_page = ArAdvertPage(driver)
        ar_vouchers_page = ArVouchersPage(driver)

        login_page.open(config.get(cs.ENVIRONMENT, 'ADVERT_LOGIN_URL'))
        login_page.sign_in_via_advert()

        profile_page.open_advert_dashboard()

        advert_page.open_ar_advert_page()

        ar_advert_page.open_ar_voucher_page()

        ar_vouchers_page.open_create_voucher_page()

    @allure.title("Create Unlimited Use AR Voucher")
    @allure.severity("BLOCKER")
    def test_create_unlimited_use_ar_voucher(self, driver):
        barcode = config.get(cs.ENVIRONMENT, 'BARCODE_1')

        create_voucher_page = CreateVoucherPage(driver)

        create_voucher_page.create_ar_voucher(unlimited_use=True)

        redemption_voucher(barcode)

    @allure.title("Create Unlimited Use 60 min AR Voucher")
    @allure.severity("BLOCKER")
    def test_create_unlimited_use_60_min_ar_voucher(self, driver):
        barcode = config.get(cs.ENVIRONMENT, 'BARCODE_2')

        create_voucher_page = CreateVoucherPage(driver)

        create_voucher_page.create_ar_voucher(unlimited_use_60_min=True)

        redemption_voucher(barcode)

    @allure.title("Create Priority Unlimited Use AR Voucher")
    @allure.severity("BLOCKER")
    def test_create_priority_unlimited_use_ar_voucher(self, driver):
        barcode = config.get(cs.ENVIRONMENT, 'BARCODE_3')

        create_voucher_page = CreateVoucherPage(driver)

        create_voucher_page.create_ar_voucher(unlimited_use_priority=True)

        redemption_voucher(barcode)

    @allure.title("Create Priority Unlimited Use 60 min AR Voucher")
    @allure.severity("BLOCKER")
    def test_create_priority_unlimited_use_60_min_ar_voucher(self, driver):
        barcode = config.get(cs.ENVIRONMENT, 'BARCODE_4')

        create_voucher_page = CreateVoucherPage(driver)

        create_voucher_page.create_ar_voucher(unlimited_use_priority_60_min=True)

        redemption_voucher(barcode)

    @allure.title("Create One Time Use AR Voucher")
    @allure.severity("BLOCKER")
    def test_create_one_time_use_ar_voucher(self, driver):
        barcode = config.get(cs.ENVIRONMENT, 'BARCODE_5')

        create_voucher_page = CreateVoucherPage(driver)

        create_voucher_page.create_ar_voucher(one_time_use=True)

        redemption_voucher(barcode)

    @allure.title("Create One Time Use 60 min AR Voucher")
    @allure.severity("BLOCKER")
    def test_create_one_time_use_60_min_ar_voucher(self, driver):
        barcode = config.get(cs.ENVIRONMENT, 'BARCODE_6')

        create_voucher_page = CreateVoucherPage(driver)

        create_voucher_page.create_ar_voucher(one_time_use_60_min=True)

        redemption_voucher(barcode)

    @allure.title("Create Priority One Time Use AR Voucher")
    @allure.severity("BLOCKER")
    def test_create_priority_one_time_use_ar_voucher(self, driver):
        barcode = config.get(cs.ENVIRONMENT, 'BARCODE_7')

        create_voucher_page = CreateVoucherPage(driver)

        create_voucher_page.create_ar_voucher(one_time_use_priority=True)

        redemption_voucher(barcode)

    @allure.title("Create Priority One Time Use 60 min AR Voucher")
    @allure.severity("BLOCKER")
    def test_create_priority_one_time_use_60_min_ar_voucher(self, driver):
        barcode = config.get(cs.ENVIRONMENT, 'BARCODE_8')

        create_voucher_page = CreateVoucherPage(driver)

        create_voucher_page.create_ar_voucher(one_time_use_priority_60_min=True)

        redemption_voucher(barcode)

    @allure.title("Create Redeemable Once Per Day AR Voucher")
    @allure.severity("BLOCKER")
    def test_create_redeemable_once_per_day_ar_voucher(self, driver):
        barcode = config.get(cs.ENVIRONMENT, 'BARCODE_9')

        create_voucher_page = CreateVoucherPage(driver)

        create_voucher_page.create_ar_voucher(redeemable_once_per_day=True)

        redemption_voucher(barcode)

    @allure.title("Create Redeemable Once Per Day 60 min AR Voucher")
    @allure.severity("BLOCKER")
    def test_create_redeemable_once_per_day_60_min_ar_voucher(self, driver):
        barcode = config.get(cs.ENVIRONMENT, 'BARCODE_10')

        create_voucher_page = CreateVoucherPage(driver)

        create_voucher_page.create_ar_voucher(redeemable_once_per_day_60_min=True)

        redemption_voucher(barcode)

    @allure.title("Create Priority Redeemable Once Per Day AR Voucher")
    @allure.severity("BLOCKER")
    def test_create_priority_redeemable_once_per_day_ar_voucher(self, driver):
        barcode = config.get(cs.ENVIRONMENT, 'BARCODE_11')

        create_voucher_page = CreateVoucherPage(driver)

        create_voucher_page.create_ar_voucher(redeemable_once_per_day_priority=True)

        redemption_voucher(barcode)

    @allure.title("Create Priority Redeemable Once Per Day 60 min AR Voucher")
    @allure.severity("BLOCKER")
    def test_create_priority_redeemable_once_per_day_60_min_ar_voucher(self, driver):
        barcode = config.get(cs.ENVIRONMENT, 'BARCODE_12')

        create_voucher_page = CreateVoucherPage(driver)

        create_voucher_page.create_ar_voucher(redeemable_once_per_day_priority_60_min=True)

        redemption_voucher(barcode)


@allure.epic("Tests for Edit/Delete AR Vouchers")
@pytest.mark.smoke
class TestArVouchers:
    @pytest.fixture(autouse=True)
    def _init_pages(self, driver):
        login = AdminLoginPage(driver)
        admin = AdminDashboardPage(driver)
        admin_advert_page = AdminAdvertiserPage(driver)
        advert_page = AdvertPage(driver)
        ar_advert_page = ArAdvertPage(driver)

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin.click_advertiser_tab_button()

        admin_advert_page.log_in_via_advertiser()
        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)
        time.sleep(5)

        advert_page.open_ar_advert_page()

        ar_advert_page.open_ar_voucher_page()

    @allure.title("Edit AR Voucher")
    @allure.severity("CRITICAL")
    def test_edit_ar_voucher(self, driver):
        ar_vouchers_page = ArVouchersPage(driver)
        ar_voucher_details_page = ArVoucherDetailsPage(driver)
        ar_voucher_edit_page = ArVoucherEditPage(driver)

        ar_vouchers_page.open_first_ar_voucher_details_page()

        ar_voucher_details_page.open_ar_voucher_edit_page()

        ar_voucher_edit_page.edit_voucher()

    @allure.title("Delete AR Voucher")
    @allure.severity("CRITICAL")
    def test_delete_ar_voucher(self, driver):
        ar_vouchers_page = ArVouchersPage(driver)

        ar_vouchers_page.delete_ar_voucher()


@allure.epic("Tests for Credit Voucher")
@pytest.mark.smoke
class TestCreditVoucher:
    @pytest.fixture(autouse=True)
    def _init_pages(self, driver):
        login = AdminLoginPage(driver)
        admin = AdminDashboardPage(driver)
        admin_advert_page = AdminAdvertiserPage(driver)

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin.click_advertiser_tab_button()

        admin_advert_page.log_in_via_system_advertiser()
        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)
        time.sleep(5)

    @allure.title("Create Credit Voucher")
    @allure.severity("BLOCKER")
    def test_create_credit_voucher(self, driver):
        system_advertiser_credit_vouchers_page = SystemAdvertiserCreditVouchersPage(driver)
        system_advertiser_create_credit_vouchers_page = SystemAdvertiserCreateCreditVoucherPage(driver)

        voucher_name = cs.CREDIT_VOUCHER_NAME

        system_advertiser_credit_vouchers_page.open_create_credit_voucher_page()

        system_advertiser_create_credit_vouchers_page.create_credit_voucher(credit_voucher_name=voucher_name)

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_system_advert_token().replace("'", "")
        }

        response = requests.get(url + "vouchers?Limit=200", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]
        assert voucher_name in all_title, "No voucher"

    @allure.title("Edit Credit Voucher")
    @allure.severity("CRITICAL")
    def test_edit_credit_voucher(self, driver):
        system_advertiser_credit_vouchers_page = SystemAdvertiserCreditVouchersPage(driver)
        system_advertiser_credit_voucher_details_page = SystemAdvertiserCreditVoucherDetailsPage(driver)
        system_advertiser_credit_voucher_edit_page = SystemAdvertiserCreditVoucherEditPage(driver)

        voucher_name = cs.CREDIT_VOUCHER_NAME

        system_advertiser_credit_vouchers_page.open_credit_voucher()

        system_advertiser_credit_voucher_details_page.open_credit_voucher_edit_page()

        system_advertiser_credit_voucher_edit_page.edit_credit_voucher(credit_voucher_name=voucher_name)

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_system_advert_token().replace("'", "")
        }

        response = requests.get(url + "vouchers?Limit=200", headers=headers)
        all_title = [item['title'] for item in json.loads(response.text)['items']]
        assert voucher_name in all_title, "No voucher"

    @allure.title("Delete Credit Voucher")
    @allure.severity("CRITICAL")
    def test_delete_credit_voucher(self, driver):
        system_advertiser_credit_vouchers_page = SystemAdvertiserCreditVouchersPage(driver)
        system_advertiser_credit_voucher_details_page = SystemAdvertiserCreditVoucherDetailsPage(driver)

        system_advertiser_credit_vouchers_page.open_credit_voucher()

        system_advertiser_credit_voucher_details_page.delete_credit_voucher()
