import configparser
import json
import time

import allure
import pytest
import requests

from pages.admin_pages.admin_advertisers_page import AdminAdvertiserPage
from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.advert_pages.advert_login_page import AdvertLoginPage
from pages.advert_pages.advert_page import AdvertPage
from pages.advert_pages.scan_and_win_competition_pages.competition_prize_details_page import CompetitionPrizeDetailsPage
from pages.advert_pages.scan_and_win_competition_pages.competition_prize_edit_page import CompetitionPrizeEditPage
from pages.advert_pages.scan_and_win_competition_pages.competition_prize_list_page import CompetitionPrizeListPage
from pages.advert_pages.scan_and_win_competition_pages.create_competition_prize_page import CreateCompetitionPrizePage
from pages.general_pages.profile_page import ProfilePage
from pages.system_advert_pages.credit_voucher_pages.system_advertiser_credit_vouchers_page import \
    SystemAdvertiserCreditVouchersPage
from pages.system_advert_pages.scan_and_win_draw_pages.system_advertiser_create_draw_prize_page import \
    SystemAdvertiserCreateDrawPrizePage
from pages.system_advert_pages.scan_and_win_draw_pages.system_advertiser_draw_prize_details_page import \
    SystemAdvertiserDrawPrizeDetailsPage
from pages.system_advert_pages.scan_and_win_draw_pages.system_advertiser_draw_prize_edit_page import \
    SystemAdvertiserDrawPrizeEditPage
from pages.system_advert_pages.scan_and_win_draw_pages.system_advertiser_draw_prize_list_page import \
    SystemAdvertiserDrawPrizeListPage
from tests import constants as cs
from tests.get_system_advert_token import get_system_advert_token
from tests.get_token import get_token

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test S&W Competition")
@pytest.mark.smoke
class TestScanAndWinCompetitionPrize:
    @pytest.fixture(autouse=True)
    def _init_pages(self, driver):
        login_page = AdvertLoginPage(driver)
        profile_page = ProfilePage(driver)
        advert_page = AdvertPage(driver)

        login_page.open(config.get(cs.ENVIRONMENT, 'ADVERT_LOGIN_URL'))
        login_page.sign_in_via_advert()

        profile_page.open_advert_dashboard()

        advert_page.open_competition_prize_page()

    @allure.title("Create S&W Competition")
    @allure.severity("BLOCKER")
    def test_create_scan_and_win_competition_prize(self, driver):
        competition_prize_list_page = CompetitionPrizeListPage(driver)

        create_competition_prize_page = CreateCompetitionPrizePage(driver)
        competition_prize_details_page = CompetitionPrizeDetailsPage(driver)

        competition_prize_name = cs.COMPETITION_PRIZE_NAME

        competition_prize_list_page.open_create_competition_prize_page()

        create_competition_prize_page.create_competition_prize()

        competition_prize_details_page.activate_deactivate_competition_prize()

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "scanandwincompetitions", headers=headers)
        competitions = json.loads(response.text)['items']
        competition = next((item for item in competitions if item['title'] == competition_prize_name), None)
        assert competition is not None, "No competition"
        assert competition['isActive'], "Competition is not active"

    @allure.title("Edit S&W Competition")
    @allure.severity("CRITICAL")
    def test_edit_scan_and_win_competition_prize(self, driver):
        competition_prize_list_page = CompetitionPrizeListPage(driver)
        competition_prize_details_page = CompetitionPrizeDetailsPage(driver)
        competition_prize_edit_page = CompetitionPrizeEditPage(driver)

        competition_prize_name = cs.COMPETITION_PRIZE_NAME

        competition_prize_list_page.open_first_competition_prize()

        competition_prize_details_page.open_edit_competition_prize_page()

        competition_prize_edit_page.edit_competition_prize()

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "scanandwincompetitions", headers=headers)
        competitions = json.loads(response.text)['items']
        competition = next((item for item in competitions if item['title'] == competition_prize_name), None)
        assert competition is not None, "No competition"
        assert competition['isActive'], "Competition is not active"

    @allure.title("Activate-Deactivate S&W Competition")
    @allure.severity("CRITICAL")
    def test_activate_deactivate_scan_and_win_competition_prize(self, driver):
        competition_prize_list_page = CompetitionPrizeListPage(driver)
        competition_prize_details_page = CompetitionPrizeDetailsPage(driver)

        competition_prize_name = cs.COMPETITION_PRIZE_NAME

        competition_prize_list_page.activate_deactivate_competition_prize()
        competition_prize_list_page.activate_deactivate_competition_prize()
        competition_prize_list_page.open_first_competition_prize()

        competition_prize_details_page.activate_deactivate_competition_prize()
        competition_prize_details_page.activate_deactivate_competition_prize()

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "scanandwincompetitions", headers=headers)
        competitions = json.loads(response.text)['items']
        competition = next((item for item in competitions if item['title'] == competition_prize_name), None)
        assert competition is not None, "No competition"
        assert competition['isActive'], "Competition is not active"

    @allure.title("Delete S&W Competition")
    @allure.severity("CRITICAL")
    def test_delete_scan_and_win_competition_prize(self, driver):
        competition_prize_list_page = CompetitionPrizeListPage(driver)

        competition_prize_name = cs.COMPETITION_PRIZE_NAME

        competition_prize_list_page.delete_competition_prize()

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

        response = requests.get(url + "scanandwincompetitions", headers=headers)
        competitions = json.loads(response.text)['items']
        competition = next((item for item in competitions if item['title'] == competition_prize_name), None)
        assert competition is None, "Competition is not deleted"


@allure.epic("Test S&W Draw")
@pytest.mark.smoke
class TestScanAndWinDrawPrize:
    @pytest.fixture(autouse=True)
    def _init_pages(self, driver):
        login = AdminLoginPage(driver)
        admin = AdminDashboardPage(driver)
        admin_advert_page = AdminAdvertiserPage(driver)
        system_advertiser_credit_vouchers_page = SystemAdvertiserCreditVouchersPage(driver)

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin.click_advertiser_tab_button()

        admin_advert_page.log_in_via_system_advertiser()
        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)
        time.sleep(5)

        system_advertiser_credit_vouchers_page.open_draw_prize_list_page()

    @allure.title("Create S&W Draw")
    @allure.severity("BLOCKER")
    def test_create_scan_and_win_draw_prize(self, driver):
        system_advertiser_draw_prize_list_page = SystemAdvertiserDrawPrizeListPage(driver)
        system_advertiser_create_draw_prize_page = SystemAdvertiserCreateDrawPrizePage(driver)
        system_advertiser_draw_prize_details_page = SystemAdvertiserDrawPrizeDetailsPage(driver)

        draw_prize_name = cs.DRAW_PRIZE_NAME

        system_advertiser_draw_prize_list_page.open_create_draw_prize_page()

        system_advertiser_create_draw_prize_page.create_draw_prize()

        system_advertiser_draw_prize_details_page.activate_deactivate_draw_prize()

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_system_advert_token().replace("'", "")
        }

        response = requests.get(url + "scanandwinsecondarycompetitions", headers=headers)
        draw_prizes = json.loads(response.text)['items']
        draw_prize = next((item for item in draw_prizes if item['title'] == draw_prize_name), None)
        assert draw_prize is not None, "No draw"
        assert draw_prize['isActive'], "Draw is not active"

    @allure.title("Edit S&W Draw")
    @allure.severity("CRITICAL")
    def test_edit_scan_and_win_draw_prize(self, driver):
        system_advertiser_draw_prize_list_page = SystemAdvertiserDrawPrizeListPage(driver)
        system_advertiser_draw_prize_details_page = SystemAdvertiserDrawPrizeDetailsPage(driver)
        system_advertiser_draw_prize_edit_page = SystemAdvertiserDrawPrizeEditPage(driver)

        draw_prize_name = cs.DRAW_PRIZE_NAME

        system_advertiser_draw_prize_list_page.open_first_draw_prize()

        system_advertiser_draw_prize_details_page.open_edit_draw_prize_page()

        system_advertiser_draw_prize_edit_page.edit_draw_prize()

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_system_advert_token().replace("'", "")
        }

        response = requests.get(url + "scanandwinsecondarycompetitions", headers=headers)
        draw_prizes = json.loads(response.text)['items']
        draw_prize = next((item for item in draw_prizes if item['title'] == draw_prize_name), None)
        assert draw_prize is not None, "No draw"
        assert draw_prize['isActive'], "Draw is not active"

    @allure.title("Activate-Deactivate S&W Draw")
    @allure.severity("CRITICAL")
    def test_activate_deactivate_scan_and_win_draw_prize(self, driver):
        system_advertiser_draw_prize_list_page = SystemAdvertiserDrawPrizeListPage(driver)
        system_advertiser_draw_prize_details_page = SystemAdvertiserDrawPrizeDetailsPage(driver)

        draw_prize_name = cs.DRAW_PRIZE_NAME

        system_advertiser_draw_prize_list_page.activate_deactivate_draw_prize()
        system_advertiser_draw_prize_list_page.activate_deactivate_draw_prize()
        system_advertiser_draw_prize_list_page.open_first_draw_prize()

        system_advertiser_draw_prize_details_page.activate_deactivate_draw_prize()
        system_advertiser_draw_prize_details_page.activate_deactivate_draw_prize()

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_system_advert_token().replace("'", "")
        }

        response = requests.get(url + "scanandwinsecondarycompetitions", headers=headers)
        draw_prizes = json.loads(response.text)['items']
        draw_prize = next((item for item in draw_prizes if item['title'] == draw_prize_name), None)
        assert draw_prize is not None, "No draw"
        assert draw_prize['isActive'], "Draw is not active"

    @allure.title("Delete S&W Draw")
    @allure.severity("CRITICAL")
    def test_delete_scan_and_win_draw_prize(self, driver):
        system_advertiser_draw_prize_list_page = SystemAdvertiserDrawPrizeListPage(driver)

        draw_prize_name = cs.DRAW_PRIZE_NAME

        system_advertiser_draw_prize_list_page.delete_draw_prize()

        url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        headers = {
            "accept": "text/plain",
            "Authorization": get_system_advert_token().replace("'", "")
        }

        response = requests.get(url + "scanandwinsecondarycompetitions", headers=headers)
        draw_prizes = json.loads(response.text)['items']
        draw_prize = next((item for item in draw_prizes if item['title'] == draw_prize_name), None)
        assert draw_prize is None, "Draw is not deleted"
