import configparser

from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.retailer_pages.retailer_customer_orders_page import RetailerCustomerOrdersPage
from pages.retailer_pages.retailer_profile_page import RetailerProfilePage
from tests import constants as cs

"""
Test for redeem the voucher as retailer
"""
config = configparser.ConfigParser()
config.read('config.ini')


class TestRedeemTheVoucher:
    def test_redeem_the_voucher(self, driver):
        login = AdminLoginPage(driver)
        admin_dashboard_page = AdminDashboardPage(driver)
        retailer_profile_page = RetailerProfilePage(driver)
        retailer_customer_orders_page = RetailerCustomerOrdersPage(driver)

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin_dashboard_page.log_in_via_retailer_admin()

        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)

        retailer_profile_page.open_customer_orders_tab()

        retailer_customer_orders_page.click_scan_save_tab()
