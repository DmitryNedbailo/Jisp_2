"""
Run for delete all test data in Retailer dashboard
"""

import configparser
import json
import unittest

import requests

from tests import constants as cs
from tests.utils import retailer_auth

config = configparser.ConfigParser()
config.read('config.ini')


class TestDeleteRetailerData(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.url = config.get(cs.ENVIRONMENT, 'BASE_URL')
        cls.headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + retailer_auth()
        }

    def test_delete_printer(self):
        self.delete_printer()

    def test_delete_all_vouchers(self):
        self.delete_all_vouchers()

    def test_delete_all_campaigns(self):
        self.delete_all_campaigns()

    def test_delete_all_bundles(self):
        self.delete_all_bundles()

    def get_printer_info(self):
        response = requests.get(self.url + "/retailer/Printing/PrinterSettings", headers=self.headers)
        return response.json()

    def delete_printer(self):
        printer_info = self.get_printer_info()
        body = {
            "PrinterID": printer_info[0]['PrinterID'],
            "PlaceID": printer_info[0]['PlaceDetails']['PlaceID']
        }
        response = requests.delete(self.url + "/retailer/Printing/PrinterSettings", json=body, headers=self.headers)
        return response

    def get_all_vouchers(self):
        response = requests.get(self.url + "/retailer/Vouchers", headers=self.headers)
        all_vouchers = [i['VoucherID'] for i in json.loads(response.text)['Items']]
        return all_vouchers

    def delete_all_vouchers(self):
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + retailer_auth(),
            "authority": config.get(cs.ENVIRONMENT, 'AUTHORITY')
        }
        all_vouchers = self.get_all_vouchers()
        for voucher_id in all_vouchers:
            requests.delete(self.url + f"/retailer/Vouchers?voucherID={voucher_id}", headers=headers)

    def get_all_campaigns(self):
        response = requests.get(self.url + "/retailer/Campaigns/Light?settings.status=1&settings.type=1",
                                headers=self.headers)
        all_campaigns = [i['ID'] for i in json.loads(response.text)['Items']]
        return all_campaigns

    def delete_all_campaigns(self):
        all_campaigns = self.get_all_campaigns()
        for campaign_id in all_campaigns:
            requests.delete(self.url + f"/retailer/Campaigns/{campaign_id}", headers=self.headers)

    def get_all_bundles(self):
        response = requests.get(self.url + "/retailer/Bundles", headers=self.headers)
        all_bundles = [i['ID'] for i in json.loads(response.text)['Items']]
        return all_bundles

    def delete_all_bundles(self):
        all_bundles = self.get_all_bundles()
        for bundles_id in all_bundles:
            requests.delete(self.url + f"/retailer/Bundles/{bundles_id}", headers=self.headers)
