from selenium.webdriver.common.by import By

# Admin LogIn page

ADMIN_LOG_IN_BUTTON = (By.XPATH, "//log-in/form//button")
ADMIN_USERNAME_FIELD = (By.XPATH, "//input[contains(@name, 'email')]")
ADMIN_PASSWORD_FIELD = (By.XPATH, "//input[contains(@name, 'password')]")

# Admin Dashboard page

ADMIN_ADVERTISERS_TAB = (By.XPATH, "//a[contains(@href,'/admin/advertisers/all?page=1&order=desc')]")
SEARCH_BY_COMPANY_NAME_FIELD = (By.XPATH, "//input[contains(@placeholder, 'Company')]")
LOGIN_TYPE_BUTTON = (By.XPATH, "//retailers-list//td[7]/div//button")
NFRN_LOGIN_TYPE_BUTTON = (By.XPATH, "//a[@ng-click='ctrl.loginAsLight(retailer)'] ")

# Admin swagger page

ADMIN_SWAGGER_EMAIL_FIELD = (By.ID, "input_username")
ADMIN_SWAGGER_PASSWORD_FIELD = (By.ID, "input_password")
ADMIN_SWAGGER_LOGIN_BUTTON = (By.ID, "login")
ADMIN_USERS_METHOD = (By.ID, "endpointListTogger_AdminUsers")
METHOD = (By.XPATH, "//a[contains(text(), '/AdminUsers') and @href='#!/AdminUsers/AdminUsers_Get_0']")
ADMIN_USERS_METHOD_EXECUTE_BUTTON = (By.XPATH, "//li[1]//li[3]//div[2]/input")

# Advertiser swagger page

ADVERT_AUTHORIZE_BUTTON = (By.XPATH, "//button[contains(@class, 'btn authorize unlocked')]")
ADVERTISER_GATEWAY_API_CHECKBOX = (By.XPATH, "//p[@class='description']")
ADVERT_AUTHORIZE_CONFIRM_BUTTON = (By.XPATH, "//button[contains(@class, 'btn modal-btn auth authorize button')]")
ADVERT_AUTHORIZE_CLOSE_BUTTON = (By.XPATH, "//button[contains(@class, 'btn modal-btn auth btn-done button')]")
ADVERT_GET_CAMPAIGNS_METHOD_BUTTON = (By.ID, "operations-Campaigns-get_v1_campaigns")
ADVERT_GET_CAMPAIGNS_METHOD_TRY_IT_OUT_BUTTON = (By.XPATH, "//div[1]/div[2]/button")
ADVERT_GET_CAMPAIGNS_METHOD_EXECUTE_BUTTON = (By.XPATH, "//div[2]/div/div[2]/button[1]")
ADVERT_GET_TOKEN_FROM_CAMPAIGNS_METHOD = (By.XPATH, "//span[contains(text(), 'Authorization')]")

# System advertiser Credit vouchers page

SYSTEM_ADVERTISER_REPORTS_TAB = (By.XPATH, "//a[contains(@href,'/credit-reports')]")
SYSTEM_ADVERTISER_SCAN_AND_WIN_TAB = (By.XPATH, "//a[contains(@href,'/scan-and-win/draw-prize')]")

# Advertiser tab

ADVERTISER_TAB_COMPANY_NAME_FIELD = (By.XPATH, "//input[contains(@type, 'text') and @ng-disabled='ctrl.waiting']")
ADVERTISER_SEARCH_APPLY_BUTTON = (By.XPATH, "//button[contains(@class, 'btn btn-success btn-with-line')]")
ADVERTISER_SORT_REG_BUTTON = (
    By.XPATH, "//span[contains(@class, 'ng-binding sort-indicator') and text()='Registration']")
ADVERTISER_LOG_IN_VIA_ADVERTISER = (By.XPATH, "//advertiser-list//tr[1]/td[7]//button")

# Advertiser Log In page

ADVERT_USERNAME_FIELD = (By.ID, "Username")
ADVERT_PASSWORD_FILED = (By.ID, "Password")
ADVERT_LOG_IN_BUTTON = (By.XPATH, "//button[contains(@class, 'btn btn-primary')]")
ADVERT_SIGN_UP_BUTTON = (By.XPATH, "//a[contains(@href, '/account/register')]")

# Advertiser Sign Up page

ADVERT_FIRST_NAME_INPUT_FIELD = (By.ID, "FirstName")
ADVERT_LAST_NAME_INPUT_FIELD = (By.ID, "LastName")
ADVERT_EMAIL_ADDRESS_INPUT_FIELD = (By.ID, "Email")
ADVERT_MOBILE_NUMBER_INPUT_FIELD = (By.ID, "PhoneNumber")
ADVERT_PASSWORD_INPUT_FIELD = (By.ID, "Password")
ADVERT_CONFIRM_PASSWORD_INPUT_FIELD = (By.ID, "ConfirmPassword")
ADVERT_PRIVACY_POLICY_CHECKBOX = (By.XPATH, "//label[@for='PrivacyPolicyAccepted']")
ADVERT_TERMS_CONDITIONS_CHECKBOX = (By.XPATH, "//label[@for='TermsAndConditionsAccepted']")
ADVERT_CONFIRM_SIGN_UP_BUTTON = (By.XPATH, "//button[contains(@value, 'register') and @name='button']")

# Profile page

ADVERT_DASHBOARD_TAB = (By.XPATH, "//a[contains(text(), 'Advertiser Dashboard') and @class='nav-link']")

# Dashboard page

AR_ADVERT_TAB = (By.XPATH, "//a[contains(@href,'/ar-adverts')]")
REPORTS_TAB = (By.XPATH, "//a[contains(@href,'/reports')]")
SCAN_AND_WIN_TAB = (By.XPATH, "//a[contains(@href,'/scan-and-win/competition-prize')]")
ADVERT_GUIDE_TAB = (By.XPATH, "//a[contains(text(), 'Advert guide')]")
ADVERT_GUIDE_PRODUCT_CARD_EFFECTS_SUBTAB = (By.XPATH, "//div[contains(text(),'Product card effects')]")
ADVERT_GUIDE_IN_STORE_FORMATS_SUBTAB = (By.XPATH, "//div[contains(text(),'In-store formats')]")
ADVERT_GUIDE_IMAGE_PREMIUM = (By.XPATH, "//img[contains(@src, 'mobile_space_1.png')]")
ADVERT_GUIDE_IMAGE_SECONDARY = (By.XPATH, "//img[contains(@src, 'mobile_space_2.png')]")
ADVERT_GUIDE_IMAGE_STORES = (By.XPATH, "//img[contains(@src, 'mobile_space_3.png')]")
ADVERT_GUIDE_IMAGE_NATIVE = (By.XPATH, "//img[contains(@src, 'mobile_space_4.png')]")
ADVERT_GUIDE_IMAGE_OFFERS = (By.XPATH, "//img[contains(@src, 'mobile_space_5.png')]")
ADVERT_GUIDE_IMAGE_PRODUCTS = (By.XPATH, "//img[contains(@src, 'mobile_space_6.png')]")
ADVERT_GUIDE_MAGPIE_SHINE = (By.XPATH, "//img[contains(@src, 'shine_effect.png')]")
ADVERT_GUIDE_IMAGE_ANIMATED_STROKE = (By.XPATH, "//img[contains(@src, 'stroke_effect.png')]")
ADVERT_GUIDE_IMAGE_FULL_SCREEN = (By.XPATH, "//img[contains(@src, 'full-screen')]")
ADVERT_GUIDE_IMAGE_FULL_HALF_SCREEN = (By.XPATH, "//img[contains(@src, 'half-screen')]")

# Campaigns page

CREATE_NEW_CONSUMER_APP_CAMPAIGN_BUTTON = (By.XPATH, "//app-campaign-tabs//button")
CREATE_NEW_CONSUMER_APP_CAMPAIGN_NAME_FIELD_INPUT = (By.ID, "mat-input-1")
IN_STORE_SIGNAGE_BUTTON = (By.XPATH, "//a[contains(@href, '/campaigns/store-signage')]")
SELECT_FIRST_CONSUMER_APP_CAMPAIGN = (By.XPATH, "//app-campaigns-list//tbody/tr[1]")
EDIT_CONSUMER_APP_CAMPAIGN_BUTTON = (By.XPATH, "//app-campaigns-list//tbody/tr[1]/td[4]")
DELETE_CONSUMER_APP_CAMPAIGN_BUTTON = (By.XPATH, "//app-campaigns-list//tbody/tr[1]/td[5]")
CONFIRM_DELETE_CONSUMER_APP_CAMPAIGN_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")

# AR advert profile page

ADVERTISER_DASHBOARD_TAB = (By.XPATH, "//div[1]/nav//ul/li/a")

# AR advert details page

ADVERT_COMPANY_DETAILS_FIELD = (By.ID, "mat-input-0")
ADVERT_COMPANY_SAVE_BUTTON = (By.XPATH, "//button[@type='submit']")

# AR advert page

CREATE_AR_CAMPAIGNS = (By.XPATH, "//app-ar-adverts-tabs/div/div/button")
AR_VOUCHERS_TAB = (By.XPATH, "//a[@href='/ar-adverts/ar-vouchers']")
SELECT_FIRST_AR_CAMPAIGN = (By.XPATH, "//app-campaign-table//tbody/tr[1]")

# Scan&Win page

CREATE_NEW_COMPETITION_PRIZE_BUTTON = (By.XPATH, "//button[@routerlink='/scan-and-win/competition-prize/create']")
CREATE_NEW_DRAW_PRIZE_BUTTON = (By.XPATH, "//app-draw-prize-list//button")
SELECT_FIRST_COMPETITION_PRIZE = (By.XPATH, "//app-competition-prize-list//app-competition-prize-item[1]")
ACTIVATE_DEACTIVATE_COMPETITION_PRIZE_SWITCHER = (
    By.XPATH, "//app-competition-prize-item[1]//mat-slide-toggle")
DELETE_COMPETITION_PRIZE_BUTTON = (
    By.XPATH, "//app-competition-prize-item[1]//div[2]/div[2]/div[2]")
CONFIRM_DELETE_COMPETITION_PRIZE_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")

# Scan&Win => Create Competition prize page

COMPETITION_PRIZE_NAME_FIELD = (By.XPATH, "//input[@formcontrolname='title']")
COMPETITION_PRIZE_DESCRIPTION_FIELD = (By.XPATH, "//textarea[@formcontrolname='description']")
COMPETITION_PRIZE_END_DATE_FIELD = (By.XPATH, "//input[@formcontrolname='endDate']")
COMPETITION_PRIZE_NUMBER_OF_WINNERS_FIELD = (By.XPATH, "//input[@formcontrolname='maxWinners']")
COMPETITION_PRIZE_WIN_RATE_FIELD = (By.XPATH, "//input[@formcontrolname='winRate']")
COMPETITION_PRIZE_LOSS_TITLE_FIELD = (By.XPATH, "//input[@formcontrolname='lossTitle']")
COMPETITION_PRIZE_LOSS_MESSAGE_FIELD = (By.XPATH, "//input[@formcontrolname='message']")
COMPETITION_PRIZE_SCAN_AND_WIN_BARCODE_FIELD = (By.XPATH, "//input[@formcontrolname='0']")
COMPETITION_PRIZE_ADD_ADDITIONAL_BARCODE_BUTTON = (
    By.XPATH, "//div[text()='Add barcode']")
COMPETITION_PRIZE_WIN_CONGRATULATION_IMAGE_UPLOAD = (
    By.XPATH, "//app-resources-common//div[1]//input")
COMPETITION_PRIZE_LOSS_IMAGE_UPLOAD = (By.XPATH, "//app-resources-common//div[3]//input")
COMPETITION_PRIZE_WIN_COMPETITION_IMAGE_UPLOAD = (
    By.XPATH, "//app-resources-common//div[2]//input")
COMPETITION_PRIZE_LOSS_BACKGROUND_IMAGE_UPLOAD = (
    By.XPATH, "//app-resources-common//div[4]//input")
COMPETITION_PRIZE_SAVE_BUTTON = (By.XPATH, "//app-competition-prize-form//button[2]")

# Scan&Win => Competition prize details page

COMPETITION_PRIZE_ACTIVATE_DEACTIVATE_BUTTON = (By.XPATH, "//app-view-competition-prize//button")
COMPETITION_PRIZE_CONFIRMATION_ACTIVATE_DEACTIVATE_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")
COMPETITION_PRIZE_EDIT_BUTTON = (By.XPATH, "//div[text()='Edit prize']")

# Reports page

AR_VOUCHERS_REPORTS = (By.XPATH, "//a[@href='/reports/voucher']")
IN_STORE_SIGNAGE_REPORTS = (By.XPATH, "//a[@href='/reports/in-store-signage']")

# AR vouchers report page

CREATE_NEW_VOUCHER_REPORT_BUTTON = (By.XPATH, "//app-report-tabs//button")

# Consumer app report page

CREATE_NEW_CONSUMER_APP_REPORT_BUTTON = (By.XPATH, "//app-report-tabs//button")

# In-store signage report page

CREATE_NEW_IN_STORE_SIGNAGE_REPORT_BUTTON = (By.XPATH, "//app-report-tabs//button")

# In-store signage campaign page

CREATE_IN_STORE_SIGNAGE_CAMPAIGN_BUTTON = (By.XPATH, "//app-campaign-tabs//button")
IN_STORE_SIGNAGE_CAMPAIGN_NAME_FIELD = (By.ID, "mat-input-2")
IN_STORE_SIGNAGE_CAMPAIGN_CREATE_CONFIRM_BUTTON = (By.XPATH, "//app-create-edit-campaign-dialog//button[2]")
CREATE_IN_STORE_SIGNAGE_ADVERT_BUTTON = (By.XPATH, "//app-campaign-details//button")
EDIT_IN_STORE_SIGNAGE_CAMPAIGN_BUTTON = (By.XPATH, "//app-signage-list//tr[1]/td[4]/button")
DELETE_IN_STORE_SIGNAGE_CAMPAIGN_BUTTON = (By.XPATH, "//app-signage-list//tr[1]/td[5]/button")
CONFIRM_DELETE_IS_STORE_SIGNAGE_CAMPAIGN_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")
SELECT_FIRST_IN_STORE_SIGNAGE_CAMPAIGN = (By.XPATH, "//app-signage-list//tbody/tr[1]")
ACTIVATE_DEACTIVATE_IN_STORE_SIGNAGE_CAMPAIGN_SWITCHER = (
    By.XPATH, "//app-signage-list//tr[1]/td[1]//mat-slide-toggle")

# In-store signage campaign details page

SELECT_FIRST_IN_STORE_ADVERT = (By.XPATH, "//app-campaign-details//tbody/tr[1]")
ACTIVATE_DEACTIVATE_IN_STORE_ADVERT_SWITCHER = (By.XPATH, "//app-campaign-details//tr[1]/td[1]//mat-slide-toggle")
IN_STORE_SIGNAGE_CAMPAIGN_BUTTON = (By.XPATH, "//a[@href='/campaigns/store-signage']")

# In-store advert details page

EDIT_IN_STORE_ADVERT_BUTTON = (By.XPATH, "//span[text()='Edit flight']")
DELETE_IN_STORE_ADVERT_BUTTON = (By.XPATH, "//span[text()='Delete flight']")
CONFIRM_DELETE_IN_STORE_ADVERT_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")

# In-store signage create advert page

IN_STORE_SIGNAGE_ADVERT_NAME_INPUT_FIELD = (By.XPATH, "//input[@formcontrolname='title']")
IN_STORE_SIGNAGE_ADVERT_END_DATE = (By.XPATH, "//input[@formcontrolname='endDate']")
IN_STORE_SIGNAGE_ADVERT_SELECT_PRODUCT_BUTTON = (By.XPATH, "//span[text()='Select product']")
IN_STORE_SIGNAGE_ADVERT_SEARCH_PRODUCT_FIELD = (By.ID, "mat-input-6")
IN_STORE_SIGNAGE_ADVERT_SELECT_PRODUCT = (By.XPATH, "//app-select-product//li[1]")
IN_STORE_SIGNAGE_ADVERT_UPLOAD_VIDEO = (By.XPATH, "//app-image-video//input")
IN_STORE_SIGNAGE_ADVERT_TARGETING_LIST = (By.ID, "mat-select-value-1")
IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_BUTTON = (By.XPATH, "//span[text()='Distance targeting']")
IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_ADD_STORE_BUTTON = (By.XPATH, "//app-distance-targeting")
IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_SEARCH_STORE_FIELD = (
    By.XPATH, "//input[@placeholder='Search']")
IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_SEARCH_STORE_BUTTON = (
    By.XPATH, "//app-distance-targeting-dialog/div[1]/div/div")
IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_NEXT_BUTTON = (By.XPATH, "//span[text()='Next']")
IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_SELECT_STORES_FIELD = (By.ID, "mat-input-8")
IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_SELECT_STORE_BUTTON = (
    By.XPATH, "//span[text()='All stores']")
IN_STORE_SIGNAGE_ADVERT_DISTANCE_TARGETING_SAVE_STORE_BUTTON = (By.XPATH, "//app-select-store-chain//button[2]")
IN_STORE_SIGNAGE_ADVERT_CREATE_ADVERT_BUTTON = (By.XPATH, "//span[text()='Create advert']")
IN_STORE_SIGNAGE_ADVERT_SAVE_EDIT_ADVERT_BUTTON = (By.XPATH, "//span[text()='Save']")

# Consumer app flight page

CREATE_CONSUMER_APP_FLIGHT_BUTTON = (By.XPATH, "//span[contains(text(), ' Create flight ')]")
CREATE_CONSUMER_APP_FLIGHT_CONFIRM_BUTTON = (By.XPATH, "//app-create-edit-campaign-dialog//button[2]")
SELECT_FIRST_CONSUMER_APP_FLIGHT_CAMPAIGN = (By.XPATH, "//app-campaign-details//tbody/tr[1]")
ACTIVATE_DEACTIVATE_CONSUMER_APP_CAMPAIGN_SWITCHER = (
    By.XPATH, "//app-campaigns-list//tr[1]/td[1]//mat-slide-toggle")
ACTIVATE_DEACTIVATE_CONSUMER_APP_FLIGHT_CAMPAIGN_SWITCHER = (
    By.XPATH, "//app-campaign-details//tr[1]/td[1]//mat-slide-toggle")
CONSUMER_APP_BUTTON = (By.XPATH, "//a[@href='/campaigns/mobile-app']")

# Consumer app flight campaign details page

EDIT_CONSUMER_APP_FLIGHT_CAMPAIGN_BUTTON = (By.XPATH, "//span[text()='Edit flight']")
DELETE_CONSUMER_APP_FLIGHT_CAMPAIGN_BUTTON = (By.XPATH, "//span[text()='Delete flight']")
CONFIRM_DELETE_CONSUMER_APP_FLIGHT_CAMPAIGN_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")

# Create Consumer app flight page

CONSUMER_APP_FLIGHT_NAME_INPUT_FIELD = (By.XPATH, "//input[@formcontrolname='title']")
CONSUMER_APP_FLIGHT_END_DATE_SELECTION = (By.XPATH, "//input[@formcontrolname='endDate']")
CONSUMER_APP_FLIGHT_SELECT_PRODUCT_BUTTON = (By.XPATH, "//span[text()='Select product']")
CONSUMER_APP_FLIGHT_SELECT_PRODUCT_INPUT_FIELD = (By.ID, "mat-input-5")
CONSUMER_APP_FLIGHT_SELECT_PRODUCT = (By.XPATH, "//app-select-product//li[1]")
CONSUMER_APP_FLIGHT_SELECT_ADVERT = (By.XPATH, "//app-create-edit-flight//app-advert-list")
CONSUMER_APP_FLIGHT_UPLOAD_PREMIUM_IMAGE = (By.XPATH, "//app-create-edit-advert-dialog//div[3]/div[1]//input")
CONSUMER_APP_FLIGHT_UPLOAD_SECONDARY_IMAGE = (By.XPATH, "//app-create-edit-advert-dialog//div[3]/div[2]//input")
CONSUMER_APP_FLIGHT_SELECT_TARGETING = (By.XPATH, "//app-geo-distance-targeting/div/mat-form-field")
CONSUMER_APP_FLIGHT_SELECT_DISTANCE_TARGETING = (By.XPATH, "//span[text()='Distance targeting']")
CONSUMER_APP_FLIGHT_SELECT_STORE_TARGETING = (By.XPATH, "//app-distance-targeting")
CONSUMER_APP_FLIGHT_INPUT_STORE_NAME = (By.XPATH, "//input[@placeholder='Search']")
CONSUMER_APP_FLIGHT_ADD_VIDEO_BUTTON = (By.XPATH, "//app-create-edit-advert-dialog//button[2]")
CONSUMER_APP_FLIGHT_NEXT_STORE_TARGETING_BUTTON = (By.XPATH, "//span[text()='Next']")
CONSUMER_APP_FLIGHT_STORE_SEARCH_FIELD = (By.ID, "mat-input-7")
CONSUMER_APP_FLIGHT_SELECT_STORE_BUTTON = (By.XPATH, "//span[text()='All stores']")
CONSUMER_APP_FLIGHT_SAVE_STORE_BUTTON = (By.XPATH, "//app-select-store-chain//button[2]")
CONSUMER_APP_FLIGHT_CONFIRM_CREATE_FLIGHT_BUTTON = (By.XPATH, "//span[text()='Create flight']")
CONSUMER_APP_FLIGHT_CONFIRM_EDIT_FLIGHT_BUTTON = (By.XPATH, "//span[text()='Save']")

# Create vouchers report page

VOUCHERS_REPORT_NAME_FIELD_INPUT = (By.XPATH, "//input[@formcontrolname='title']")
SELECT_VOUCHERS_REPORT_BUTTON = (By.XPATH, "//span[text()='Select voucher']")
VOUCHERS_REPORT_SELECT_ALL_VOUCHERS_BUTTON = (By.XPATH, "//span[text()='All vouchers']")
VOUCHERS_REPORT_SAVE_VOUCHERS_BUTTON = (By.XPATH, "//app-select-vouchers-dialog//button[2]")
VOUCHERS_REPORT_GENERATE_REPORT_BUTTON = (By.XPATH, "//span[text()='Generate report']")

# Create consumer app report page

CONSUMER_APP_REPORT_NAME_INPUT_FIELD = (By.XPATH, "//input[@formcontrolname='title']")
SELECT_CONSUMER_APP_CAMPAIGN_BUTTON = (By.XPATH, "//span[text()='Select campaign']")
SELECT_ALL_CAMPAIGN_BUTTON = (By.XPATH, "//span[contains(text(), 'All campaigns ')]")
SELECT_CAMPAIGN_SAVE_BUTTON = (By.XPATH, "//app-select-campaigns-dialog//button[2]")
CONSUMER_APP_REPORT_GENERATE_REPORT_BUTTON = (By.XPATH, "//span[text()='Generate report']")

# Create In-store signage report page

IN_STORE_SIGNAGE_REPORT_NAME_INPUT_FIELD = (By.XPATH, "//input[@formcontrolname='title']")
SELECT_IN_STORE_SIGNAGE_CAMPAIGN_BUTTON = (By.XPATH, "//span[text()='Select campaign']")
SELECT_ALL_IN_STORE_SIGNAGE_CAMPAIGN_BUTTON = (By.XPATH, "//span[contains(text(), 'All campaigns ')]")
SELECT_IN_STORE_SIGNAGE_CAMPAIGN_SAVE_BUTTON = (By.XPATH, "//app-select-campaigns-dialog//button[2]")
IN_STORE_SIGNAGE_REPORT_GENERATE_REPORT_BUTTON = (By.XPATH, "//span[text()='Generate report']")

# System advertiser => Vouchers report page

SYSTEM_ADVERTISER_CREATE_NEW_REPORT_BUTTON = (By.XPATH, "//app-report-list//div/button")

# System advertiser => Create vouchers report page

CREDIT_VOUCHER_REPORT_NAME_INPUT_FIELD = (By.XPATH, "//input[@formcontrolname='title']")
SELECT_SYSTEM_ADVERTISER_VOUCHERS_REPORT_BUTTON = (By.XPATH, "//span[text()='Select voucher']")
SELECT_ALL_CREDIT_VOUCHERS_BUTTON = (By.XPATH, "//span[contains(text(), 'All vouchers')]")
SELECT_LOYALTY_VOUCHER_BUTTON = (By.XPATH, "//span[contains(text(), 'Loyalty ')]")
SELECT_ALL_CREDIT_VOUCHERS_SAVE_BUTTON = (By.XPATH, "//app-select-vouchers-dialog//button[2]")
SELECT_LOYALTY_VOUCHER_SAVE_BUTTON = (By.XPATH, "//app-select-vouchers-dialog//button[2]")
CREDIT_VOUCHERS_REPORT_GENERATE_REPORT_BUTTON = (By.XPATH, "//span[text()='Generate report']")

# System advertiser => Draw prizes list page

SELECT_FIRST_DRAW_PRIZE = (By.XPATH, "//app-draw-prize-list//app-draw-prize-item[1]")
ACTIVATE_DEACTIVATE_DRAW_PRIZE_SWITCHER = (By.XPATH, "//app-draw-prize-item[1]//mat-slide-toggle")
DRAW_PRIZE_DELETE_BUTTON = (By.XPATH, "//app-draw-prize-item[1]//div[2]/div[2]/div[2]")
DRAW_PRIZE_CONFIRM_DELETE_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")

# System advertiser => Create draw prize page

DRAW_PRIZE_NAME_FIELD = (By.XPATH, "//input[@formcontrolname='title']")
DRAW_PRIZE_DESCRIPTION_FIELD = (By.XPATH, "//textarea[@formcontrolname='description']")
DRAW_PRIZE_END_DATE_FIELD = (By.XPATH, "//input[@formcontrolname='endDate']")
DRAW_PRIZE_ENTRY_IMAGE_UPLOAD = (By.XPATH, "//app-resources-common/div/div[1]//input")
DRAW_PRIZE_IMAGE_UPLOAD = (By.XPATH, "//app-resources-common/div/div[2]//input")
DRAW_PRIZE_SAVE_PRIZE_BUTTON = (By.XPATH, "//span[text()='Save prize']")

# System advertiser => Scan&Win => Draw prize details page

DRAW_PRIZE_ACTIVATE_DEACTIVATE_BUTTON = (By.XPATH, "//app-view-draw-prize//button")
DRAW_PRIZE_CONFIRM_ACTIVATE_DEACTIVATE_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")
DRAW_PRIZE_EDIT_BUTTON = (By.XPATH, "//div[text()='Edit prize']")

# Ar Campaign details page

EDIT_AR_CAMPAIGN_BUTTON = (By.XPATH, "//span[text()='Edit AR campaign']")
DELETE_AR_CAMPAIGN_BUTTON = (By.XPATH, "//span[text()='Delete AR campaign']")
CONFIRM_DELETE_AR_CAMPAIGN_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")

# Create AR Campaigns page

CAMPAIGN_NAME_FIELD_INPUT = (By.XPATH, "//input[@formcontrolname='title']")
CAMPAIGN_START_DATE_SELECTION = (By.XPATH, "//input[@formcontrolname='startDate']")
CAMPAIGN_END_DATE_SELECTION = (By.XPATH, "//input[@formcontrolname='endDate']")
CAMPAIGN_TARGETING_BUTTON = (By.XPATH, "//span[text()='Add new']")
CAMPAIGN_STORE_POSTCODE_FIELD = (By.XPATH, "//input[@placeholder='Search']")
CAMPAIGN_STORE_SEARCH_NEXT_BUTTON = (By.XPATH, "//span[text()='Next']")
CAMPAIGN_STORE_SEARCH_FIELD = (By.ID, "mat-input-6")
CAMPAIGN_SELECT_STORE_BUTTON = (By.XPATH, "//span[text()='All stores']")
CAMPAIGN_SAVE_STORE_BUTTON = (By.XPATH, "//app-select-store-chain//button[2]")
CAMPAIGN_SAVE_BUTTON = (By.XPATH, "//span[contains(text(), ' Save ')]")

# AR Vouchers page

CREATE_NEW_BUTTON = (By.XPATH, "//app-ar-adverts-tabs//button")
SELECT_FIRST_AR_VOUCHER = (By.XPATH, "//app-ar-vouchers//div[2]/app-voucher-preview")
AR_VOUCHER_DELETE_BUTTON = (By.XPATH, "//div[2]/app-voucher-preview//div[2]/button")
AR_VOUCHER_CONFIRM_DELETE_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")

# Credit vouchers page

CREATE_NEW_CREDIT_VOUCHER_BUTTON = (By.XPATH, "//button[@routerlink='/credit-vouchers/voucher/create']")
SELECT_CREDIT_VOUCHER = (By.XPATH, "//div[contains(text(),'Credit_voucher_')]")

# Credit voucher details page

EDIT_CREDIT_VOUCHER_BUTTON = (By.XPATH, "//span[text()='Edit voucher']")
DELETE_CREDIT_VOUCHER_BUTTON = (By.XPATH, "//span[text()='Delete voucher']")
CONFIRM_DELETE_CREDIT_VOUCHER_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")

# Create credit voucher page

CREDIT_VOUCHER_NAME_FIELD_INPUT = (By.XPATH, "//input[@formcontrolname='title']")
CREDIT_VOUCHER_DESCRIPTION_INPUT_FIELD = (By.XPATH, "//textarea[@formcontrolname='description']")
CREDIT_VOUCHER_END_DATE_SELECTION = (By.XPATH, "//input[@formcontrolname='endDate']")
CREDIT_VOUCHER_DISCOUNT_INPUT_FIELD = (By.XPATH, "//input[@formcontrolname='discount']")
CREDIT_VOUCHER_IMG_UPLOAD_VOUCHER_IMAGE = (By.XPATH, "//div[1]/app-resource//input")
CREDIT_VOUCHER_IMG_UPLOAD_BACKGROUND_IMAGE = (By.XPATH, "//div[2]/app-resource//input")
CREDIT_VOUCHER_SAVE_DRAFT_BUTTON = (By.XPATH, "//span[contains(text(), ' Save draft ')]")
CREDIT_VOUCHER_PUBLISH_VOUCHER_BUTTON = (By.XPATH, "//button[contains(text(), ' Publish voucher ')]")
CREDIT_VOUCHER_PUBLISH_VOUCHER_CONFIRM_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")
CREDIT_VOUCHER_SAVE_EDIT_BUTTON = (By.XPATH, "//span[contains(text(), ' Save ')]")
# Create AR Voucher page

TERMS_OF_USE_LIST = (By.XPATH, "//mat-select[@formcontrolname='termsOfUseType']")
ONE_TIME_USE_BUTTON = (By.XPATH, "//span[contains(text(), ' One-time use ')]")
REDEEMABLE_ONCE_PER_DAY = (By.XPATH, "//span[contains(text(), ' Redeemable once per day ')]")
UNLIMITED_USE_BUTTON = (By.XPATH, "//span[contains(text(), ' Unlimited use ')]")
EXPIRES_IN_HOUR_AFTER_SCAN_CHECKBOX = (By.XPATH, "//mat-checkbox[@formcontrolname='expiresInAnHour']")
PRIORITY_CHECKBOX = (By.XPATH, "//mat-checkbox[@formcontrolname='isRemoteAllowed']")
VOUCHER_NAME_FIELD_INPUT = (By.XPATH, "//input[@formcontrolname='title']")
VOUCHER_DESCRIPTION_INPUT_FIELD = (By.XPATH, "//textarea[@formcontrolname='description']")
END_DATE_SELECTION = (By.XPATH, "//input[@formcontrolname='endDate']")
RRP_INPUT_FIELD = (By.XPATH, "//input[@formcontrolname='rrp']")
DISCOUNT_INPUT_FILED = (By.XPATH, "//input[@formcontrolname='discount']")
VOUCHER_BARCODE_INPUT_FIELD = (By.XPATH, "//input[@formcontrolname='barcode']")
SELECT_PRODUCT_BUTTON = (By.XPATH, "//span[text()='Select product']")
SELECT_PRODUCT_INPUT_FIELD = (By.ID, "mat-input-10")
SELECT_PRODUCT = (By.XPATH, "//app-select-product//li[1]")
VOUCHER_TARGETING_BUTTON = (By.XPATH, "//span[text()='Add new']")
VOUCHER_POSTCODE_FIELD = (By.XPATH, "//input[@placeholder='Search']")
VOUCHER_STORE_SEARCH_NEXT_BUTTON = (By.XPATH, "//span[text()='Next']")
VOUCHER_STORE_SEARCH_FIELD = (By.ID, "mat-input-12")
VOUCHER_SELECT_STORE_BUTTON = (By.ID, "mat-checkbox-3")
VOUCHER_SAVE_STORE_BUTTON = (By.XPATH, "//app-select-store-chain//button[2]")
IMG_UPLOAD_SMALL = (By.XPATH, "//div[1]/app-resource/div[4]//input")
IMG_UPLOAD_MEDIUM = (By.XPATH, "//div[2]/app-resource/div[4]//input")
IMG_UPLOAD_VOUCHER = (By.XPATH, "//div[3]/app-resource/div[4]//input")
IMG_UPLOAD_BACKGROUND = (By.XPATH, "//div[4]/app-resource/div[4]//input")
VOUCHER_SAVE_VOUCHER_BUTTON = (By.XPATH, "//span[contains(text(), ' Save draft ')]")
VOUCHER_SAVE_EDIT_BUTTON = (By.XPATH, "//span[contains(text(), ' Save ')]")

# Ar Voucher details page

NON_PUBLISH_AR_VOUCHER_PUBLISH_BUTTON = (By.XPATH, "//button[text()=' Publish voucher ']")
NON_PUBLISH_AR_VOUCHER_CONFIRM_PUBLISH_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")
EDIT_AR_VOUCHER_BUTTON = (By.XPATH, "//span[text()='Edit AR voucher']")

# Retailer dashboard => Profile page

MY_STORE_TAB = (By.XPATH, "//a[text()='My Store']")
JISP_MATERIALS_TAB = (By.XPATH, "//a[@href='/print-materials']")
BASKET_BUTTON = (By.XPATH, "//span[contains(@class, 'cart-icon')]")
INVOICING_AND_BILLING_BUTTON = (By.XPATH, "//a[contains(text(), 'Invoicing and Billing')]")
MY_STORE_VOUCHER_BUTTON = (By.XPATH, "//a[contains(@href,'/vouchers')]")
MY_STORE_OFFERS_BUTTON = (By.XPATH, "//a[text()='Offers']")
MY_STORE_BUNDLE_BUTTON = (By.XPATH, "//a[contains(@href,'/bundles')]")
MY_STORE_CAMPAIGN_BUTTON = (By.XPATH, "//a[contains(@href,'/campaigns')]")
MY_STORE_PRINTER_BUTTON = (By.XPATH, "//a[contains(@href,'/printer')]")
MY_STORE_SALES_SETTINGS_BUTTON = (By.XPATH, "//a[contains(@href,'/sales-settings')]")
BILLING_METHODS_BUTTON = (By.XPATH, "//div[@aria-posinset=3]")
CUSTOMER_ORDERS_TAB = (By.XPATH, "//a[text()='Customer Orders']")
EDIT_PROFILE_BUTTON = (By.XPATH, "//span[text()='Edit profile']")
PROFILE_BUTTON = (By.XPATH, "//app-header//button")

# Transaction Invoices page

AR_INVOICES_BUTTON = (By.XPATH, "//div[contains(text(), 'AR Invoices')]")

# Retailer dashboard => Customer orders page

CUSTOMER_ORDERS_SCAN_SAVE_TAB = (By.XPATH, "//a[@href='/jisppay/ar-vouchers']")
CUSTOMER_ORDERS_SCAN_SAVE_VOUCHER_CODE_INPUT_FIELD = (By.ID, "voucherInput")
CUSTOMER_ORDERS_SCAN_SAVE_VALIDATE_CODE_BUTTON = (By.XPATH, "//button[text()='Validate code']")

# Retailer dashboard => Voucher list page

VOUCHERS_LIST_CREATE_NEW_BUTTON = (By.XPATH, "//button[@routerlink='/vouchers/new']")
PUBLISH_VOUCHER_BUTTON = (By.XPATH, "//app-voucher-list//app-voucher[1]/div/button")
CONFIRM_PUBLISH_VOUCHER_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")
EDIT_VOUCHER_BUTTON = (By.XPATH, "//app-voucher-list//app-voucher[1]//button[1]")
DELETE_VOUCHER_BUTTON = (By.XPATH, "//app-voucher-list//app-voucher[1]//button[2]")
DELETE_VOUCHER_CONFIRM_BUTTON = (By.XPATH, "//app-confirm-dialog//button[2]")

# Retailer dashboard => Bundles page

CREATE_BUNDLE_BUTTON = (By.XPATH, "//button[@routerlink='create-bundle']")

# Retailer dashboard => Create Bundle page

BUNDLE_NAME_FIELD = (By.XPATH, "//input[contains(@formcontrolname, 'bundleName')]")
BUNDLE_PRICE_FIELD = (By.XPATH, "//input[contains(@formcontrolname, 'Price')]")
BUNDLE_DESCRIPTION_FIELD = (By.XPATH, "//textarea[contains(@formcontrolname, 'bundleDesc')]")
BUNDLE_VALID_FROM_FIELD = (By.XPATH, "//input[contains(@formcontrolname, 'from')]")
BUNDLE_VALID_UNTIL_FIELD = (By.XPATH, "//input[contains(@formcontrolname, 'until')]")
BUNDLE_COMPONENT_FIELD = (By.XPATH, "//input[contains(@formcontrolname, 'name')]")
BUNDLE_CREATE_COVER_IMAGE_BUTTON = (By.XPATH, "//div[contains(text(),'Create cover image')]")
BUNDLE_CREATE_COVER_IMAGE_SAVE_BUTTON = (By.XPATH, "//mat-dialog-container//button[2]")
BUNDLE_ADD_ITEMS_BUTTON = (By.XPATH, "//app-create-bundle//div[2]/button")
BUNDLE_ADD_ITEMS_FIRST_PRODUCT_LIST = (
    By.XPATH, "//div[@class='category clickable d-flex align-items-center ng-star-inserted']")
BUNDLE_ADD_ITEMS_SECOND_PRODUCT_LIST = (By.XPATH, "//div[contains(text(),'Beef')]")
BUNDLE_ADD_ITEMS_SELECT_ALL_PRODUCTS_CHECKBOX = (
    By.XPATH, "//div/div[1]/div[1]/div[2]/mat-checkbox")
BUNDLE_ADD_ITEMS_ADD_BUTTON = (By.XPATH, "//mat-dialog-container//button[2]")
BUNDLE_CREATE_BUTTON = (By.XPATH, "//app-create-bundle//button[2]")

# Retailer dashboard => Printer page

ADD_PRINTER_BUTTON = (By.XPATH, "//button[@routerlink='/printer/settings']")

# Retailer dashboard => Create Printer page

PRINTER_PATH_FIELD = (By.XPATH, "//input[@formcontrolname='PrinterPath']")
PRINTER_ACCESS_ID_FIELD = (By.XPATH, "//input[@formcontrolname='PrinterAccessID']")
PRINTER_SAVE_BUTTON = (By.XPATH, "//span[text()='Save']")

# Retailer dashboard => My Campaign page

MAKE_A_CAMPAIGN_BUTTON = (By.XPATH, "//button[@routerlink='/make-campaign']")

# Retailer dashboard => Campaign create page

TYPE_CAMPAIGN_BUTTON = (By.ID, "mat-select-4")
EVENT_TYPE_CAMPAIGN_BUTTON = (By.ID, "mat-option-16")
SPECIFY_YOUR_ORDER_FIELD = (By.XPATH, "//mat-select[@formcontrolname='offerTemplate']")
RETAILER_CAMPAIGN_SAVE_XXP_BUTTON = (By.XPATH, "//mat-option[@ng-reflect-value='2']")
RETAILER_EVENT_CAMPAIGN_NAME_FIELD = (By.XPATH, "//input[@formcontrolname='eventName']")
RETAILER_CAMPAIGN_PRODUCT_NAME_FIELD = (By.ID, "product")
RETAILER_CAMPAIGN_CUSTOM_OFFER_TEXT_FIELD = (By.ID, "description")
RETAILER_EVENT_CAMPAIGN_CUSTOM_CAMPAIGN_TEXT_FIELD = (By.XPATH, "//textarea[@formcontrolname='description']")
RETAILER_EVENT_CAMPAIGN_START_DATE = (By.XPATH, "//input[@formcontrolname='startTime']")
RETAILER_EVENT_CAMPAIGN_END_DATE = (By.XPATH, "//input[@formcontrolname='endDate']")
RETAILER_EVENT_CAMPAIGN_DETAILS_FIELD = (By.XPATH, "//textarea[@formcontrolname='terms']")
RETAILER_CAMPAIGN_ORIGINAL_PRICE_FIELD = (By.ID, "price1")
RETAILER_CAMPAIGN_OFFER_PRICE_FIELD = (By.ID, "price2")
RETAILER_CAMPAIGN_BARCODE_FIELD = (By.XPATH, "//input[@formcontrolname='ean']")
RETAILER_CAMPAIGN_CONTINUE_BUTTON = (By.XPATH, "//span[contains(text(), ' Continue ')]")

# Retailer dashboard => Sales Settings page

ACTIVATE_DEACTIVATE_SCAN_AND_GO_SWITCHER = (By.ID, "mat-slide-toggle-1")
ACTIVATE_DEACTIVATE_CLICK_AND_COLLECT_SWITCHER = (By.ID, "mat-slide-toggle-2")
ACTIVATE_DEACTIVATE_DELIVERY_SWITCHER = (By.ID, "mat-slide-toggle-3")
ACTIVATE_DEACTIVATE_CONFIRM_BUTTON = (By.XPATH, "//app-sales-settings-confirm-dialog//button[2]")
CHANGE_DELIVERY_METHOD_BUTTON = (By.XPATH, "//span[contains(text(), ' Change delivery method ')]")
SELECT_ZOOM_1HR_DELIVERY_BUTTON = (By.XPATH, "//span[contains(text(),'Select Zoom 1hr Delivery')]")
SELECT_GOPHR_DELIVERY_BUTTON = (By.XPATH, "//span[contains(text(),'Select Gophr')]")
SELECT_IN_HOUSE_DELIVERY_BUTTON = (By.XPATH, "//span[contains(text(),'Select in-house')]")
SAVE_SALES_SETTINGS_BUTTON = (By.XPATH, "//span[contains(text(),'Save Settings')]")

# Retailer dashboard => Purchase a Campaign

LAUNCH_CAMPAIGN_BUTTON = (By.XPATH, "//span[text()='Launch Campaign ']")
BACK_TO_MY_CAMPAIGN_BUTTON = (By.XPATH, "//button[@ng-reflect-router-link='/campaigns']")

# Retailer dashboard => My Product page

CREATE_NEW_OFFER_BUTTON = (By.XPATH, "//app-offer-list//button")
SELECT_OFFER_PRODUCT_BUTTON = (By.XPATH, "//span[text()='Select product']")
SEARCH_OFFER_PRODUCT_FIELD = (By.ID, "mat-input-6")
CONFIRM_SELECT_OFFER_PRODUCT = (By.XPATH, "//app-select-product//li[1]")
OFFER_START_DATE_FIELD = (By.XPATH, "//input[@formcontrolname='offerValidFrom']")
OFFER_END_DATE_FIELD = (By.XPATH, "//input[@formcontrolname='offerValidTo']")
OFFER_PRICE_FIELD = (By.XPATH, "//input[@ng-reflect-name='price2']")
OFFER_CHECKBOX_OFFER = (By.ID, "mat-checkbox-9-input")
OFFER_SAVE_BUTTON = (By.XPATH, "//span[text()='Save']")

# Retailer dashboard => Voucher create page

VOUCHER_MIN_ORDER_PRICE_FIELD = (By.XPATH, "//input[@formcontrolname='MinOrderPrice']")
VOUCHER_START_DATE_FIELD = (By.XPATH, "//input[@formcontrolname='StartDate']")
VOUCHER_END_DATE_FIELD = (By.XPATH, "//input[@formcontrolname='EndDate']")
VOUCHER_CODE_FIELD = (By.XPATH, "//input[@formcontrolname='VoucherCode']")
VOUCHER_CREATE_DRAFT_BUTTON = (By.XPATH, "//span[text()='Create draft']")

# Retailer dashboard => Jisp Materials page

FIRST_PRINT_MATERIALS_BUTTON = (By.XPATH, "//app-print-materials//div[1]/div[1]/img")
QUANTITY_MATERIALS_FIELD = (By.ID, "//input[@formcontrolname='quantity']")
ADD_TO_BASKET_MATERIALS_BUTTON = (By.XPATH, "//span[text()='Add to Basket']")
GO_TO_BASKET_BUTTON = (By.XPATH, "//a[@class='link']")

# Retailer dashboard => Basket page

BASKET_BUY_NOW_BUTTON = (By.XPATH, "//span[text()='Buy Now']")

# Retailer dashboard => Billing methods page

CREATE_NEW_BILLING_METHODS_BUTTON = (By.XPATH, "//span[contains(text(), ' Create New ')]")
BILLING_METHOD_CARD_NUMBER_INPUT_FIELD = (By.XPATH, "//input[@formcontrolname='cardNumber']")
BILLING_METHOD_EXPIRATION_DATE_INPUT_FIELD = (By.XPATH, "//input[@formcontrolname='expirationDate']")
BILLING_METHOD_CREATE_CONFIRMATION_BUTTON = (By.XPATH, "//span[text()='Create']")

# Retailer dashboard => Edit profile page

RETAILER_FIRST_NAME_FIELD = (By.XPATH, "//input[@formcontrolname='FirstName']")
RETAILER_LAST_NAME_FIELD = (By.XPATH, "//input[@formcontrolname='LastName']")
RETAILER_EMAIL_ADDRESS_FIELD = (By.XPATH, "//input[@formcontrolname='Email']")
RETAILER_CONTACT_PHONE_FIELD = (By.ID, "app-phone-number-0")
RETAILER_VERIFY_NUMBER_BUTTON = (By.XPATH, "//span[text()=' Verify number ']")
RETAILER_VERIFY_CODE_NUMBER_FIELD = (By.XPATH, "//input[@formcontrolname='VerificationCode']")
RETAILER_VERIFY_NUMBER_CONFIRM_BUTTON = (By.XPATH, "//span[text()='Verify']")
RETAILER_WHATSAPP_NUMBER_FIELD = (By.ID, "app-phone-number-1")
RETAILER_SORT_CODE_FIELD = (By.XPATH, "//input[@formcontrolname='SortCode']")
RETAILER_BANK_ACCOUNT_NUMBER_FIELD = (By.XPATH, "//input[@formcontrolname='BankAccount']")
RETAILER_BANK_ACCOUNT_NAME_FIELD = (By.XPATH, "//input[@formcontrolname='BusinessAccountName']")
RETAILER_VAT_NUMBER_FIELD = (By.XPATH, "//input[@formcontrolname='TaxNumber']")
RETAILER_COMPANY_NAME_FIELD = (By.XPATH, "//input[@formcontrolname='CompanyName']")
RETAILER_COMPANY_REGISTRATION_NUMBER_FIELD = (By.XPATH, "//input[@formcontrolname='Orgnr']")
RETAILER_BUSINESS_DESCRIPTION_FIELD = (By.XPATH, "//textarea[@formcontrolname='Story']")
RETAILER_ADDRESS_LINE_1_FIELD = (By.XPATH, "//input[@formcontrolname='AddressLine1']")
RETAILER_ADDRESS_LINE_2_FIELD = (By.XPATH, "//input[@formcontrolname='AddressLine2']")
RETAILER_TOWN_FIELD = (By.XPATH, "//input[@formcontrolname='Town']")
RETAILER_POSTCODE_FIELD = (By.XPATH, "//input[@formcontrolname='Postcode']")
RETAILER_STORE_CONTACT_NUMBER_FIELD = (By.ID, "app-phone-number-2")
RETAILER_WEBSITE_FIELD = (By.XPATH, "//input[@formcontrolname='WebsiteUrl']")
RETAILER_SAVE_BUTTON = (By.XPATH, "//span[text()='Save']")

# Asserts => Retailer

CHECK_NEW_BILLING_METHOD = (By.XPATH, "//app-payment-methods-list//div[2]/div[2]")
CHECK_RETAILER_LOGIN = (By.XPATH, "//app-profile/div/div/div/div[1]/div/button")
