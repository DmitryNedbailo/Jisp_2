import configparser
import json
import os
import platform
import sys
import uuid

import requests

from tests import constants as cs
from tests.get_token import get_token

voucher_code = cs.VOUCHER_CODE
offer_title = cs.OFFER_TITLE

config = configparser.ConfigParser()
config.read('config.ini')


def auth(func):
    def wrap():
        token = get_token()
        func(token)

    return wrap()


def get_admin_token():
    url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/admin/auth/token"
    body = {
        "grant_type": "password",
        "username": config.get(cs.ENVIRONMENT, 'ADMIN_USERNAME'),
        "password": config.get(cs.ENVIRONMENT, 'ADMIN_PASSWORD')
    }

    response = requests.post(url, body)
    assert response.status_code == 200, "get_admin_token: SWW"
    return "Bearer " + json.loads(response.text)["access_token"]


def get_all_adverts():
    url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/admin/Advertisers"
    headers = {
        "accept": "application/json",
        "X-Api-Version": "1.0",
        "Authorization": get_admin_token()
    }

    response = requests.get(url, headers=headers)
    assert response.status_code == 200, "get_all_adverts: SWW"
    return json.loads(response.text)["Items"][-1]["Id"]


def change_advert_status():
    url = config.get(cs.ENVIRONMENT, 'CHANGE_STATUS_ADVERT_URL_1') + str(get_all_adverts()) + "&newStatus=1"
    headers = {
        "accept": "application/json",
        "X-Api-Version": "1.0",
        "Authorization": get_admin_token()
    }

    response = requests.put(url, headers=headers)
    return response


def check_advert_status():
    url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/admin/Advertisers"
    headers = {
        "accept": "application/json",
        "X-Api-Version": "1.0",
        "Authorization": get_admin_token()
    }

    response = requests.get(url, headers=headers)
    return response


def shopper_auth():
    url_shopper_auth = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/shopper/auth/token"
    body = {
        "grant_type": "password",
        "password": config.get(cs.ENVIRONMENT, 'SHOPPER_PASSWORD'),
        "username": config.get(cs.ENVIRONMENT, 'SHOPPER_EMAIL')
    }

    response = requests.post(url_shopper_auth, body)
    assert response.status_code == 200, "SWW"
    return response.json()["access_token"]


def retailer_auth():
    retailer_auth_url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/retailer/auth/token"
    body = {
        "grant_type": "password",
        "username": config.get(cs.ENVIRONMENT, 'RETAILER_EMAIL_ADDRESS'),
        "password": config.get(cs.ENVIRONMENT, 'RETAILER_PASSWORD'),
        "client_id": "light"
    }

    response = requests.post(retailer_auth_url, body)
    assert response.status_code == 200, "SWW"
    return response.json()["access_token"]


def get_retailer_voucher_code():
    url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/retailer/Vouchers"
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + retailer_auth()
    }
    params = {
        "offset": 0,
        "limit": 10
    }
    response = requests.get(url, headers=headers, params=params)
    all_title = [item['VoucherCode'] for item in json.loads(response.text)['Items']]
    return all_title


def get_offers():
    url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/retailer/Products"
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + retailer_auth()
    }
    params = {
        "limit": 30,
        "offset": 0,
        "sortOrder": 1,
        "sortColumn": "EndOfferDate",
        "filterByItemType": False,
        "productState": 0,
        "withOffers": True
    }

    response = requests.get(url, headers=headers, params=params)
    all_title = [item['Title'] for item in json.loads(response.text)['Items']]
    return all_title


def get_last_billing_method():
    url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/retailer/PaymentMethods"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + retailer_auth()
    }

    response = requests.get(url, headers=headers)
    return response.json()[-1]['ExpirationDate']


def get_last_event_campaign():
    url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/retailer/Campaigns/Light?settings.status=1&settings.type=2"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + retailer_auth()
    }

    response = requests.get(url, headers=headers)
    return response.json()['Items'][-1]['Name']


def get_last_offer_campaign():
    url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/retailer/Campaigns/Light?settings.status=1&settings.type=1"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + retailer_auth()
    }

    response = requests.get(url, headers=headers)
    return response.json()['Items'][-1]['Name']


def get_retailer_phone_number():
    url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/retailer/Profile"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + retailer_auth()
    }

    response = requests.get(url, headers=headers)
    return response.json()['PhoneNumber']['PhoneNumberWithoutCode']


def get_last_retailer_bundle():
    url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/retailer/Bundles"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + retailer_auth()
    }

    response = requests.get(url, headers=headers)
    return response.json()['Items'][0]['Name']


def get_retailer_printer_path():
    url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/retailer/Printing/PrinterSettings"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + retailer_auth()
    }

    response = requests.get(url, headers=headers)
    return response.json()[0]['PrinterPath']


def get_retailer_delivery_type_and_delivery_enabled():
    url = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/retailer/OrderSettings"
    headers = {
        "Accept": "application/json",
        "Authorization": "Bearer " + retailer_auth()
    }

    response = requests.get(url, headers=headers)
    return response.json()['Items'][0]


def generate_uuids(num_uuids):
    uuids = []
    for i in range(num_uuids):
        uuids.append(str(uuid.uuid4()))
    return uuids


# Retailer API

def get_voucher_for_inventory():
    url_barcode = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/shopper/Ar/Vouchers/Find"
    headers = {
        "Authorization": "Bearer " + shopper_auth()
    }
    params = {
        "sku": config.get(cs.ENVIRONMENT, 'INVENTORY_SKU'),
        "placeID": config.get(cs.ENVIRONMENT, 'INVENTORY_PLACE_ID'),
        "lon": "0",
        "lat": "0"
    }

    response = requests.get(url_barcode, headers=headers, params=params)
    assert response.status_code == 200, "Error while get voucher"
    return response.json()


def add_voucher_to_wallet():
    url_add_voucher = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/shopper/Ar/Vouchers/AddToWallet"
    headers = {
        "Authorization": "Bearer " + shopper_auth()
    }
    body = {
        "Lon": 0,
        "ArVoucherId": get_voucher_for_inventory()['Id'],
        "PlaceID": config.get(cs.ENVIRONMENT, 'INVENTORY_PLACE_ID'),
        "Lat": 0
    }
    response = requests.post(url_add_voucher, body, headers=headers)
    assert response.status_code == 200, "Error while add voucher to wallet"
    return response.json()["RedemptionCode"]


def get_inventory_token():
    url = config.get(cs.ENVIRONMENT, 'INVENTORY_MICROSOFT_LINE_URL')
    headers = {
        "Content-Type": "application/x-www-form-urlencoded"
    }
    payload = {
        "grant_type": config.get(cs.ENVIRONMENT, 'INVENTORY_GRANT_TYPE'),
        "client_id": config.get(cs.ENVIRONMENT, 'INVENTORY_CLIENT_ID'),
        "client_secret": config.get(cs.ENVIRONMENT, 'INVENTORY_CLIENT_SECRET'),
        "scope": config.get(cs.ENVIRONMENT, 'INVENTORY_SCOPE')
    }

    response = requests.get(url, data=payload, headers=headers)
    assert response.status_code == 200, "Error auth"
    return response.json()['access_token']


def environment_search():
    allure_dir = None
    for arg in sys.argv:
        if arg.startswith('--alluredir='):
            allure_dir = arg.split('=')[1]

    if config.get(cs.ENVIRONMENT, 'ENVIRONMENT') == 'stage':
        file_path = os.path.join(allure_dir, 'environment.properties')
        with open(file_path, 'w') as file:
            file.write('Stand=Stage\n'
                       'Browser=Chrome\n')

    elif config.get(cs.ENVIRONMENT, 'ENVIRONMENT') == 'demo':
        file_path = os.path.join(allure_dir, 'environment.properties')
        with open(file_path, 'w') as file:
            file.write('Stand=Demo\n'
                       'Browser=Chrome\n')
    else:
        print('Allure directory not found')


def get_os_and_os_version():
    allure_dir = None
    for arg in sys.argv:
        if arg.startswith('--alluredir='):
            allure_dir = arg.split('=')[1]
    # Get operating system name
    os_name = platform.system()
    os_version = platform.mac_ver()[0]

    if os_name == 'Darwin':
        file_path = os.path.join(allure_dir, 'environment.properties')
        with open(file_path, 'a') as file:
            file.write('OS: macOS\n'
                       'macOS version: ' + os_version)

    elif os_name == 'Windows':
        file_path = os.path.join(allure_dir, 'environment.properties')
        with open(file_path, 'a') as file:
            file.write('OS: Windows\n'
                       'Windows version: ' + os_version)
    else:
        pass
