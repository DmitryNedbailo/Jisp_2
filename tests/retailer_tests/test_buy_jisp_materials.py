import configparser

import allure
import pytest

from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.retailer_pages.retailer_basket_page import RetailerBasketPage
from pages.retailer_pages.retailer_buy_jisp_materials_page import RetailerBuyJispMaterialsPage
from pages.retailer_pages.retailer_jisp_materials_list_page import RetailerJispMaterialsListPage
from pages.retailer_pages.retailer_profile_page import RetailerProfilePage
from tests import constants as cs

"""
Test for buy jisp materials by retailer
"""

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Buy Jisp Materials")
@pytest.mark.retailer
class TestBuyJispMaterials:
    @allure.title("Buy JIsp Materials")
    @allure.severity("NORMAL")
    def test_buy_jisp_materials(self, driver):
        login = AdminLoginPage(driver)
        admin_dashboard_page = AdminDashboardPage(driver)
        retailer_profile_page = RetailerProfilePage(driver)
        retailer_jisp_materials_list_page = RetailerJispMaterialsListPage(driver)
        retailer_buy_jisp_materials_page = RetailerBuyJispMaterialsPage(driver)
        retailer_basket_page = RetailerBasketPage(driver)

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin_dashboard_page.log_in_via_retailer_admin()

        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)

        retailer_profile_page.open_jisp_materials_tab()

        retailer_jisp_materials_list_page.click_first_print_materials_button()

        retailer_buy_jisp_materials_page.add_materials_to_basket()

        retailer_basket_page.click_buy_now_button()

        current_url = driver.current_url

        assert current_url == config.get(cs.ENVIRONMENT, 'RETAILER_PAYMENT_SUCCESSFUL'), "Payment failed"
