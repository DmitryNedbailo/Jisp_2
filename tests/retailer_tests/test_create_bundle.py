import configparser

import allure
import pytest

from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.retailer_pages.retailer_bundle_list_page import RetailerBundleListPage
from pages.retailer_pages.retailer_create_bundle_page import RetailerCreateBundlePage
from pages.retailer_pages.retailer_profile_page import RetailerProfilePage
from tests import constants as cs
from tests.utils import get_last_retailer_bundle

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Create Bundle")
@pytest.mark.retailer
class TestCreateBundle:
    @allure.title("Test Create Bundle")
    @allure.severity("CRITICAL")
    def test_create_bundle(self, driver):
        login = AdminLoginPage(driver)
        admin_dashboard_page = AdminDashboardPage(driver)
        retailer_profile_page = RetailerProfilePage(driver)
        retailer_bundle_list_page = RetailerBundleListPage(driver)
        retailer_create_bundle_page = RetailerCreateBundlePage(driver)

        bundle_name = cs.BUNDLE_NAME

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin_dashboard_page.log_in_via_retailer_admin()

        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)

        retailer_profile_page.open_retailer_bundle_list_page()

        retailer_bundle_list_page.open_create_bundle_page()

        retailer_create_bundle_page.create_bundle(bundle_name=bundle_name)

        new_bundle = get_last_retailer_bundle()

        assert bundle_name == new_bundle, "No bundle"
