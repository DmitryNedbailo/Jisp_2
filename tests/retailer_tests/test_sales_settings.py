import configparser

import allure
import pytest

from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.retailer_pages.retailer_profile_page import RetailerProfilePage
from pages.retailer_pages.retailer_sales_settings_page import RetailerSalesSettingPage
from tests import constants as cs
from tests.utils import get_retailer_delivery_type_and_delivery_enabled

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Sales Settings")
@pytest.mark.retailer
class TestSalesSettings:
    @allure.title("Test Sales Settings")
    @allure.severity("CRITICAL")
    def test_sales_settings(self, driver):
        login = AdminLoginPage(driver)
        admin_dashboard_page = AdminDashboardPage(driver)
        retailer_profile_page = RetailerProfilePage(driver)
        retailer_sales_settings_page = RetailerSalesSettingPage(driver)

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin_dashboard_page.log_in_via_retailer_admin()

        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)

        retailer_profile_page.open_retailer_sales_settings_page()

        retailer_sales_settings_page.activate_deactivate_settings_and_change_delivery_method()

        check_delivery = get_retailer_delivery_type_and_delivery_enabled()

        assert check_delivery['DeliveryType'] == 1, "Error"
        assert check_delivery['ShoppingFlows']['Delivery']['Enabled'], "Error"
