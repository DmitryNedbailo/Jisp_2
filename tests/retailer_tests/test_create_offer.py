import configparser

import allure
import pytest

from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.retailer_pages.retailer_my_product_page import RetailerMyProductPage
from pages.retailer_pages.retailer_profile_page import RetailerProfilePage
from tests import constants as cs
from tests.utils import get_offers

"""
Test for create new offer by retailer
"""

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Create Offer")
@pytest.mark.retailer
class TestCreateOffer:
    @allure.title("Create Offer")
    @allure.severity("CRITICAL")
    def test_create_new_offer(self, driver):
        login = AdminLoginPage(driver)
        admin_dashboard_page = AdminDashboardPage(driver)
        retailer_profile_page = RetailerProfilePage(driver)
        retailer_my_product_page = RetailerMyProductPage(driver)

        offer_title = cs.OFFER_TITLE

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin_dashboard_page.log_in_via_retailer_admin()

        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)

        retailer_profile_page.open_retailer_offer_list_page()

        retailer_my_product_page.create_new_retailer_offer()

        get_all_offers = get_offers()

        assert offer_title in get_all_offers, "No offer"
