import configparser

import allure
import pytest

from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.retailer_pages.retailer_create_voucher_page import RetailerCreateVoucherPage
from pages.retailer_pages.retailer_edit_voucher_page import RetailerEditVoucherPage
from pages.retailer_pages.retailer_profile_page import RetailerProfilePage
from pages.retailer_pages.retailer_voucher_list_page import RetailerVoucherListPage
from tests import constants as cs
from tests.utils import get_retailer_voucher_code

"""
Test for create voucher by retailer
"""

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Voucher")
@pytest.mark.retailer
class TestVoucher:
    @pytest.fixture(autouse=True)
    def _init_pages(self, driver):
        login = AdminLoginPage(driver)
        admin_dashboard_page = AdminDashboardPage(driver)
        retailer_profile_page = RetailerProfilePage(driver)

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin_dashboard_page.log_in_via_retailer_admin()

        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)

        retailer_profile_page.open_retailer_voucher_list_page()

    @allure.title("Create Voucher")
    @allure.severity("CRITICAL")
    def test_create_voucher(self, driver):
        retailer_voucher_list_page = RetailerVoucherListPage(driver)
        retailer_create_voucher_page = RetailerCreateVoucherPage(driver)

        voucher_code = cs.VOUCHER_CODE

        retailer_voucher_list_page.open_create_new_voucher_page()

        retailer_create_voucher_page.create_voucher()

        retailer_voucher_list_page.publish_voucher()

        all_vouchers_title = get_retailer_voucher_code()

        assert voucher_code in all_vouchers_title, "No voucher"

    @allure.title("Edit Voucher")
    @allure.severity("CRITICAL")
    def test_edit_voucher(self, driver):
        retailer_voucher_list_page = RetailerVoucherListPage(driver)
        retailer_edit_voucher_page = RetailerEditVoucherPage(driver)

        voucher_code = cs.VOUCHER_CODE

        retailer_voucher_list_page.open_edit_voucher_page()

        retailer_edit_voucher_page.edit_voucher(voucher_code=voucher_code)

        all_vouchers_title = get_retailer_voucher_code()

        assert voucher_code in all_vouchers_title, "No voucher"

    @allure.title("Delete Voucher")
    @allure.severity("CRITICAL")
    def test_delete_voucher(self, driver):
        retailer_voucher_list_page = RetailerVoucherListPage(driver)

        retailer_voucher_list_page.delete_voucher()
