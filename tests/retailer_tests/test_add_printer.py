import configparser

import allure
import pytest

from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.retailer_pages.retailer_add_printer_page import RetailerAddPrinterPage
from pages.retailer_pages.retailer_printer_page import RetailerPrinterPage
from pages.retailer_pages.retailer_profile_page import RetailerProfilePage
from tests import constants as cs
from tests.utils import get_retailer_printer_path

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Add Printer")
@pytest.mark.retailer
class TestAddPrinter:
    @allure.title("Add Printer")
    @allure.severity("MINOR")
    def test_add_printer(self, driver):
        login = AdminLoginPage(driver)
        admin_dashboard_page = AdminDashboardPage(driver)
        retailer_profile_page = RetailerProfilePage(driver)
        retailer_printer_page = RetailerPrinterPage(driver)
        retailer_add_printer_page = RetailerAddPrinterPage(driver)

        printer_path = cs.PRINTER_PATH

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin_dashboard_page.log_in_via_retailer_admin()

        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)

        retailer_profile_page.open_retailer_printer_page()

        retailer_printer_page.open_add_printer_page()

        retailer_add_printer_page.add_printer(path=printer_path)

        retailer_printer_path = get_retailer_printer_path()

        assert printer_path == retailer_printer_path, "Printer not added"
