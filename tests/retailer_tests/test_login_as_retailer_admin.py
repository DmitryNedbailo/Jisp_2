import configparser

import allure
import pytest

from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from tests import constants as cs

"""
Test for login as retailer with admin rights
"""

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Log In Retailer Admin")
@pytest.mark.retailer
class TestLoginAsRetailerAdmin:
    @allure.title("Log In Retailer Admin")
    @allure.severity("BLOCKER")
    def test_login_as_retailer_admin(self, driver):
        login = AdminLoginPage(driver)
        admin_dashboard_page = AdminDashboardPage(driver)

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin_dashboard_page.log_in_via_retailer_admin()

        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)

        current_url = driver.current_url

        assert current_url == config.get(cs.ENVIRONMENT, 'RETAILER_PROFILE_URL'), "Unsuccessful login"
