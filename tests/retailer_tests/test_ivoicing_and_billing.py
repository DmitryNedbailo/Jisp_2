import configparser

import allure
import pytest

from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.retailer_pages.retailer_profile_page import RetailerProfilePage
from tests import constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Invoicing and Billing")
@pytest.mark.retailer
class TestInvoicingAndBilling:
    @allure.title("Test Invoicing and Billing")
    @allure.severity("NORMAL")
    def test_invoicing_and_billing(self, driver):
        login = AdminLoginPage(driver)
        admin_dashboard_page = AdminDashboardPage(driver)
        retailer_profile_page = RetailerProfilePage(driver)

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin_dashboard_page.log_in_via_retailer_admin()

        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)

        retailer_profile_page.open_transaction_invoices_page()

        current_url = driver.current_url

        assert current_url == config.get(cs.ENVIRONMENT, 'RETAILER_TRANSACTION_INVOICES_URL')

        retailer_profile_page.open_ar_invoices_page()

        current_url = driver.current_url

        assert current_url == config.get(cs.ENVIRONMENT, 'RETAILER_AR_INVOICES_URL')
