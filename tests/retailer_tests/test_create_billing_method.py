import configparser

import allure
import pytest

from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.retailer_pages.retailer_billing_methods_page import RetailerBillingMethodsPage
from pages.retailer_pages.retailer_profile_page import RetailerProfilePage
from tests import constants as cs
from tests.utils import get_last_billing_method

"""
Test for create new billing method by retailer
"""

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Create Billing Method")
@pytest.mark.retailer
class TestCreateBillingMethod:
    @allure.title("Create Billing Method")
    @allure.severity("NORMAL")
    def test_create_new_billing_method(self, driver):
        login = AdminLoginPage(driver)
        admin_dashboard_page = AdminDashboardPage(driver)
        retailer_profile_page = RetailerProfilePage(driver)
        retailer_billing_methods_page = RetailerBillingMethodsPage(driver)

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin_dashboard_page.log_in_via_retailer_admin()

        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)

        retailer_profile_page.click_billing_methods_button()

        retailer_billing_methods_page.create_new_billing_method()

        new_billing_method = get_last_billing_method()

        assert new_billing_method == config.get(cs.ENVIRONMENT, 'CARD_EXPIRATION_DATE'), "No new billing method"
