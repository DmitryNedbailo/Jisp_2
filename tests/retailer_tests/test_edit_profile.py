import configparser

import allure
import pytest

from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.retailer_pages.retailer_edit_profile_page import RetailerEditProfilePage
from pages.retailer_pages.retailer_profile_page import RetailerProfilePage
from tests import constants as cs
from tests.utils import get_retailer_phone_number

"""
Test for edit retailer profile
"""
config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Edit Profile")
@pytest.mark.retailer
class TestEditProfile:
    @allure.title("Edit Profile")
    @allure.severity("CRITICAL")
    def test_edit_profile(self, driver):
        login = AdminLoginPage(driver)
        admin_dashboard_page = AdminDashboardPage(driver)
        retailer_profile_page = RetailerProfilePage(driver)
        retailer_edit_profile_page = RetailerEditProfilePage(driver)

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin_dashboard_page.log_in_via_retailer_admin()

        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)

        retailer_profile_page.open_edit_profile_page()

        retailer_edit_profile_page.edit_retailer_profile()

        retailer_phone_number = get_retailer_phone_number()

        assert retailer_phone_number == cs.RETAILER_CONTACT_PHONE, "Profile editing error"
