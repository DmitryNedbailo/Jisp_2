import configparser

import allure
import pytest

from pages.admin_pages.admin_dashboard_page import AdminDashboardPage
from pages.admin_pages.admin_login_page import AdminLoginPage
from pages.retailer_pages.retailer_make_a_campaign_page import RetailerMakeACampaignPage
from pages.retailer_pages.retailer_my_campaign_page import RetailerMyCampaignPage
from pages.retailer_pages.retailer_profile_page import RetailerProfilePage
from tests import constants as cs
from tests.utils import get_last_offer_campaign

config = configparser.ConfigParser()
config.read('config.ini')


@allure.epic("Test Create Offer Campaign")
@pytest.mark.retailer
class TestCreateOfferCampaign:
    @allure.title("Create Offer Campaign")
    @allure.severity("CRITICAL")
    def test_create_new_offer_campaign(self, driver):
        login = AdminLoginPage(driver)
        admin_dashboard_page = AdminDashboardPage(driver)
        retailer_profile_page = RetailerProfilePage(driver)
        retailer_my_campaign_page = RetailerMyCampaignPage(driver)
        retailer_make_a_campaign_page = RetailerMakeACampaignPage(driver)

        login.open(config.get(cs.ENVIRONMENT, 'ADMIN_LOGIN_URL'))
        login.sign_in_via_admin()

        admin_dashboard_page.log_in_via_retailer_admin()

        window_2 = driver.window_handles[1]
        driver.switch_to.window(window_2)

        retailer_profile_page.open_retailer_campaign_list_page()

        retailer_my_campaign_page.open_make_a_campaign_page()

        retailer_make_a_campaign_page.create_new_retailer_offer_campaign()

        new_offer_campaign = get_last_offer_campaign()

        assert new_offer_campaign == cs.CAMPAIGN_PRODUCT_NAME
