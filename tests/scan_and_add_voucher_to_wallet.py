import configparser

import requests

from tests import constants as cs

config = configparser.ConfigParser()
config.read('config.ini')


def shopper_auth():
    url_shopper_auth = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/shopper/auth/token"
    body = {
        "grant_type": "password",
        "password": config.get(cs.ENVIRONMENT, 'SHOPPER_PASSWORD'),
        "username": config.get(cs.ENVIRONMENT, 'SHOPPER_EMAIL')
    }

    response = requests.post(url_shopper_auth, body)
    assert response.status_code == 200, "shopper_auth: SWW"
    return response.json()


def get_voucher(barcode):
    url_barcode = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/shopper/Ar/Vouchers/Find"
    headers = {
        "Authorization": "Bearer " + shopper_auth()["access_token"]
    }
    params = {
        "sku": barcode,
        "placeID": config.get(cs.ENVIRONMENT, 'PLACE_ID'),
        "lon": "0",
        "lat": "0"
    }

    response = requests.get(url_barcode, headers=headers, params=params)
    assert response.status_code == 200, "scan_voucher: SWW"
    return response.json()


def add_voucher_to_wallet(barcode):
    url_add_voucher = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/shopper/Ar/Vouchers/AddToWallet"
    headers = {
        "Authorization": "Bearer " + shopper_auth()["access_token"]
    }
    body = {
        "Lon": 0,
        "ArVoucherId": get_voucher(barcode)['Id'],
        "PlaceID": config.get(cs.ENVIRONMENT, 'PLACE_ID'),
        "Lat": 0
    }

    response = requests.post(url_add_voucher, body, headers=headers)
    assert response.status_code == 200, "add_voucher_to_wallet: SWW"
    return response.json()["RedemptionCode"]
