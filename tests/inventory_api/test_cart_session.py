import configparser
import unittest

import requests

from tests import constants as cs
from tests.utils import get_inventory_token, add_voucher_to_wallet

config = configparser.ConfigParser()
config.read('config.ini')


class TestCartSession(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.url = config.get(cs.ENVIRONMENT, 'BASE_URL')
        cls.headers = {
            "Authorization": "Bearer " + get_inventory_token(),
            "RetailerID": config.get(cs.ENVIRONMENT, 'INVENTORY_RETAILER_ID')
        }
        
    def setUp(self):
        self.session_id = self.create_voucher_session()

    def test_cart_session(self):
        self.assign_voucher()
        self.redeem_voucher_session()

    def create_voucher_session(self):
        url = self.url + "/inventory/VoucherSessions"
        body = {
            "PlaceID": config.get(cs.ENVIRONMENT, 'INVENTORY_PLACE_ID'),
            "CartItems": [{
                "SKU": config.get(cs.ENVIRONMENT, 'INVENTORY_SKU'),
                "Quantity": 1,
                "Price": 5
            }]
        }

        response = requests.post(url, json=body, headers=self.headers)
        assert response.status_code == 200, "Error while create the session"
        return response.json()['SessionID']

    def assign_voucher(self):
        url = self.url + "/inventory/VoucherSessions/Voucher"
        body = {
            "RedemptionCode": add_voucher_to_wallet(),
            "SessionID": self.session_id
        }

        response = requests.post(url, json=body, headers=self.headers)
        assert response.status_code == 200, "Error while assign voucher to session"
        return response.json()

    def redeem_voucher_session(self):
        url = self.url + "/inventory/VoucherSessions/Redeem"
        body = {
            "SessionID": self.session_id
        }

        response = requests.post(url, body, headers=self.headers)
        assert response.status_code == 200, "Error while redeem voucher session"
