import configparser
import random
import unittest

import requests

from tests import constants as cs
from tests.utils import get_inventory_token

config = configparser.ConfigParser()
config.read('config.ini')

session_id = random.randrange(1000, 10000)


class TestSessionDoesNotExist(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.url = config.get(cs.ENVIRONMENT, 'BASE_URL')
        cls.headers = {
            "Authorization": "Bearer " + get_inventory_token(),
            "RetailerID": config.get(cs.ENVIRONMENT, 'INVENTORY_RETAILER_ID')
        }

    def test_session_does_not_exist(self):
        self.get_voucher_session()
        self.assign_voucher()
        self.redeem_voucher_session()
        self.update_voucher_session()
        self.delete_voucher_from_session()
        self.delete_voucher_session()

    def get_voucher_session(self):
        url = self.url + f"/inventory/VoucherSessions/{random.randrange(1000, 10000)}"

        response = requests.get(url, headers=self.headers)
        assert response.status_code == 404, "Something went wrong"
        assert response.json()['Message'] == "Session does not exist", "Wrong Message"
        assert response.json()['ExceptionMessage'] == "Session does not exist", "Wrong ExceptionMessage"

    def assign_voucher(self):
        url = self.url + "/inventory/VoucherSessions/Voucher"
        body = {
            "RedemptionCode": "111-111-111",
            "SessionID": session_id
        }

        response = requests.post(url, json=body, headers=self.headers)
        assert response.status_code == 404, "Something went wrong"
        assert response.json()['Message'] == "Session does not exist", "Wrong Message"
        assert response.json()['ExceptionMessage'] == "Session does not exist", "Wrong ExceptionMessage"

    def redeem_voucher_session(self):
        url = self.url + "/inventory/VoucherSessions/Redeem"
        body = {
            "SessionID": session_id
        }

        response = requests.post(url, json=body, headers=self.headers)
        assert response.status_code == 404, "Something went wrong"
        assert response.json()['Message'] == "Session does not exist", "Wrong Message"
        assert response.json()['ExceptionMessage'] == "Session does not exist", "Wrong ExceptionMessage"

    def update_voucher_session(self):
        url = self.url + "/inventory/VoucherSessions"
        body = {
            "SessionID": session_id,
            "CartItems": [{
                "SKU": config.get(cs.ENVIRONMENT, 'INVENTORY_SKU'),
                "Quantity": 2,
                "Price": 15
            }]
        }

        response = requests.put(url, json=body, headers=self.headers)
        assert response.status_code == 404, "Something went wrong"
        assert response.json()['Message'] == "Session does not exist", "Wrong Message"
        assert response.json()['ExceptionMessage'] == "Session does not exist", "Wrong ExceptionMessage"

    def delete_voucher_from_session(self):
        url = self.url + "/inventory/VoucherSessions/Voucher"
        body = {
            "VoucherID": 1111,
            "SessionID": session_id
        }

        response = requests.delete(url, json=body, headers=self.headers)
        assert response.status_code == 404, "Something went wrong"
        assert response.json()['Message'] == "Session does not exist", "Wrong Message"
        assert response.json()['ExceptionMessage'] == "Session does not exist", "Wrong ExceptionMessage"

    def delete_voucher_session(self):
        url = self.url + f"/inventory/VoucherSessions/{session_id}"

        response = requests.delete(url, headers=self.headers)
        assert response.status_code == 404, "Something went wrong"
        assert response.json()['Message'] == "Session does not exist", "Wrong Message"
        assert response.json()['ExceptionMessage'] == "Session does not exist", "Wrong ExceptionMessage"
