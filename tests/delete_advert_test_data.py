"""
Run for delete all test data in Advertiser dashboard
"""
import configparser
import json
import unittest

import requests

from tests import constants as cs
from tests.get_system_advert_token import get_system_advert_token
from tests.get_token import get_token

config = configparser.ConfigParser()
config.read('config.ini')


class TestDeleteAdvertData(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        cls.headers = {
            "accept": "text/plain",
            "Authorization": get_token().replace("'", "")
        }

    def test_delete_all_vouchers(self):
        self.delete_all_vouchers()

    def test_delete_all_campaign(self):
        self.delete_all_campaigns()

    def test_delete_all_reports(self):
        self.delete_all_reports()

    def test_delete_all_ar_reports(self):
        self.delete_all_ar_reports()

    def test_delete_all_voucher_campaigns(self):
        self.delete_all_voucher_campaigns()

    def test_delete_all_in_store_campaigns(self):
        self.delete_all_in_store_campaigns()

    def test_delete_all_in_store_reports(self):
        self.delete_all_in_store_reports()

    def test_delete_all_competition_prizes(self):
        self.delete_all_competition_prizes()

    def get_all_vouchers(self):
        response = requests.get(self.url + "vouchers", headers=self.headers)
        all_id = [i['id'] for i in json.loads(response.text)['items']]
        assert response.status_code == 200, "test_get_all_vouchers: SWW"
        return all_id

    def delete_all_vouchers(self):
        all_vouchers_id = self.get_all_vouchers()
        for voucher_id in all_vouchers_id:
            url = self.url + f"vouchers/{voucher_id}"
            requests.delete(url, headers=self.headers)

    def get_all_campaigns(self):
        response = requests.get(self.url + "campaigns", headers=self.headers)
        all_campaigns_id = [i["id"] for i in json.loads(response.text)["items"]]
        assert response.status_code == 200, "test_get_all_campaigns: SWW"
        return all_campaigns_id

    def delete_all_campaigns(self):
        all_campaigns = self.get_all_campaigns()
        for campaigns_id in all_campaigns:
            url = self.url + f"campaigns/{campaigns_id}"
            requests.delete(url, headers=self.headers)

    def get_all_reports(self):
        response = requests.get(self.url + "reporting", headers=self.headers)
        all_reports_id = [i["uid"] for i in json.loads(response.text)["items"]]
        assert response.status_code == 200, "test_get_all_reports: SWW"
        return all_reports_id

    def delete_all_reports(self):
        all_reports = self.get_all_reports()
        for reports_id in all_reports:
            url = self.url + f"reporting?reportUid={reports_id}"
            requests.delete(url, headers=self.headers)

    def get_all_ar_reports(self):
        response = requests.get(self.url + "reporting/ar", headers=self.headers)
        all_reports_id = [i["uid"] for i in json.loads(response.text)["items"]]
        assert response.status_code == 200, "test_get_all_ar_reports: SWW"
        return all_reports_id

    def delete_all_ar_reports(self):
        all_reports = self.get_all_ar_reports()
        for reports_id in all_reports:
            url = self.url + f"reporting/ar?reportUid={reports_id}"
            requests.delete(url, headers=self.headers)

    def get_all_voucher_campaigns(self):
        response = requests.get(self.url + "vouchercampaigns?Limit=100", headers=self.headers)
        all_voucher_campaigns_id = [i["id"] for i in json.loads(response.text)["items"]]
        assert response.status_code == 200, "test_get_all_voucher_campaigns: SWW"
        return all_voucher_campaigns_id

    def delete_all_voucher_campaigns(self):
        all_voucher_campaigns = self.get_all_voucher_campaigns()
        for campaigns_id in all_voucher_campaigns:
            url = self.url + f"vouchercampaigns/delete/{campaigns_id}"
            requests.delete(url, headers=self.headers)

    def get_all_in_store_campaigns(self):
        response = requests.get(self.url + "campaigns?Limit=100&Offset=0&AdvertClientType=2", headers=self.headers)
        all_in_store_campaigns = [i["id"] for i in json.loads(response.text)["items"]]
        assert response.status_code == 200, "test_get_all_in_store_campaigns: SWW"
        return all_in_store_campaigns

    def delete_all_in_store_campaigns(self):
        all_in_store_campaigns = self.get_all_in_store_campaigns()
        for campaigns_id in all_in_store_campaigns:
            url = self.url + f"campaigns/{campaigns_id}"
            requests.delete(url, headers=self.headers)

    def get_all_in_store_reports(self):
        response = requests.get(self.url + "reporting?AdvertClientType=2", headers=self.headers)
        all_reports_id = [i["uid"] for i in json.loads(response.text)["items"]]
        assert response.status_code == 200, "test_get_all_in_store_reports: SWW"
        return all_reports_id

    def delete_all_in_store_reports(self):
        all_in_store_reports = self.get_all_in_store_reports()
        for reports_id in all_in_store_reports:
            url = self.url + f"reporting?reportUid={reports_id}"
            requests.delete(url, headers=self.headers)

    def get_all_competition_prizes(self):
        response = requests.get(self.url + "scanandwincompetitions", headers=self.headers)
        all_competition_prizes_id = [i["id"] for i in json.loads(response.text)["items"]]
        assert response.status_code == 200, "test_get_all_competition_prizes: SWW"
        return all_competition_prizes_id

    def delete_all_competition_prizes(self):
        all_competition_prizes = self.get_all_competition_prizes()
        for competition_prizes_id in all_competition_prizes:
            url = self.url + f"scanandwincompetitions/{competition_prizes_id}"
            requests.delete(url, headers=self.headers)


class TestDeleteTestDataSystemAdvert(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.url = config.get(cs.ENVIRONMENT, 'SERVICES_URL')
        cls.headers = {
            "accept": "text/plain",
            "Authorization": get_system_advert_token().replace("'", "")
        }

    def test_delete_all_draw_prizes(self):
        self.delete_all_draw_prizes()

    def get_all_draw_prizes(self):
        response = requests.get(self.url + "scanandwinsecondarycompetitions", headers=self.headers)
        all_draw_prizes = [i["id"] for i in json.loads(response.text)["items"]]
        assert response.status_code == 200, "test_get_all_draw_prizes: SWW"
        return all_draw_prizes

    def delete_all_draw_prizes(self):
        all_draw_prizes = self.get_all_draw_prizes()
        for draw_prizes_id in all_draw_prizes:
            url = self.url + f"scanandwinsecondarycompetitions/{draw_prizes_id}"
            requests.delete(url, headers=self.headers)
