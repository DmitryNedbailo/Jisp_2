import configparser

import requests

from tests import constants as cs
from .scan_and_add_voucher_to_wallet import add_voucher_to_wallet

config = configparser.ConfigParser()
config.read('config.ini')


def retailer_auth():
    url_retailer_auth = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/retailer/auth/token"
    body = {
        "grant_type": "password",
        "username": config.get(cs.ENVIRONMENT, 'RETAILER_EMAIL_ADDRESS'),
        "password": config.get(cs.ENVIRONMENT, 'RETAILER_PASSWORD')
    }

    response = requests.post(url_retailer_auth, body)
    assert response.status_code == 200, "retailer_auth: SWW"
    return response.json()["access_token"]


def get_one_time_login_code():
    url_one_time_login_code = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/retailer/JispPayStaff/OneTimeLoginCode"
    body = {
        "PlaceID": config.get(cs.ENVIRONMENT, 'PLACE_ID')
    }
    headers = {
        "Authorization": "Bearer " + retailer_auth()
    }

    response = requests.post(url_one_time_login_code, body, headers=headers)
    assert response.status_code == 200, "get_one_time_login_code: SWW"
    return response.json()


def customer_orders_auth():
    url_customer_orders_auth = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/jisppay/auth/token"
    body = {
        "grant_type": "sales_assistant_one_time_login_code_auth",
        "login_code": get_one_time_login_code()
    }
    headers = {
        "Authorization": "Bearer " + retailer_auth()
    }

    response = requests.post(url_customer_orders_auth, body, headers=headers)
    assert response.status_code == 200, "customer_orders_auth: SWW"
    return response.json()["access_token"]


def redemption_voucher(barcode):
    url_redemption_voucher = config.get(cs.ENVIRONMENT, 'BASE_URL') + "/jisppay/ArVouchers/Redeem"
    body = {
        "Code": add_voucher_to_wallet(barcode)
    }
    headers = {
        "Authorization": "Bearer " + customer_orders_auth()
    }

    response = requests.post(url_redemption_voucher, body, headers=headers)
    assert response.status_code == 200, "redemption_voucher: SWW"
    return response.json()
