import random
import time
from datetime import datetime, timedelta

from selenium.webdriver import Keys

timestamp = str(time.time())
end_date = datetime.now() + timedelta(days=10)

# To change the environment you need to specify 'stage' or 'demo'

ENVIRONMENT = "demo"

# Admin advertiser tab page

SYSTEM_ADVERTISER_COMPANY_NAME = "Jisp"

# Advert details page

NEW_ADVERT_COMPANY_NAME = "New_Company_" + timestamp

# Campaigns page

CONSUMER_APP_CAMPAIGN_NAME = "Consumer_App_Campaign_" + timestamp

# Create Consumer app flight page

CONSUMER_APP_FLIGHT_NAME = "Consumer_App_Flight_" + timestamp
CONSUMER_APP_FLIGHT_END_DATE = end_date.strftime('%d%m%Y')
FLIGHT_ADVERT_VIDEO = "/fixtures/consumer_app_gif/HomescreenPremium.gif"
FLIGHT_PREMIUM_IMAGE = "/fixtures/consumer_app_gif/HomescreenPremium.gif"
FLIGHT_SECONDARY_IMAGE = "/fixtures/consumer_app_gif/HomescreenSecondary.gif"

# Create AR Voucher page

VOUCHER_UNLIMITED_NAME = "Unlimited_" + timestamp
VOUCHER_UNLIMITED_60_MIN_NAME = "Unlimited_60_min_" + timestamp
VOUCHER_ONE_TIME_USE_NAME = "One_time_" + timestamp
VOUCHER_ONE_TIME_USE_60_MIN_NAME = "One_time_60_min_" + timestamp
VOUCHER_REDEEMABLE_ONCE_PER_DAY_NAME = "Redeemable_once_per_day_" + timestamp
VOUCHER_REDEEMABLE_ONCE_PER_DAY_60_MIN_NAME = "Redeemable_once_per_day_60_min_" + timestamp
VOUCHER_PRIORITY_UNLIMITED_NAME = "Priority_unlimited_" + timestamp
VOUCHER_PRIORITY_UNLIMITED_60_MIN_NAME = "Priority_unlimited_60_min_" + timestamp
VOUCHER_PRIORITY_ONE_TIME_USE_NAME = "Priority_one_time_" + timestamp
VOUCHER_PRIORITY_ONE_TIME_USE_60_MIN_NAME = "Priority_one_time_60_min_" + timestamp
VOUCHER_PRIORITY_REDEEMABLE_ONCE_PER_DAY_NAME = "Priority_redeemable_once_per_day_" + timestamp
VOUCHER_PRIORITY_REDEEMABLE_ONCE_PER_DAY_60_MIN_NAME = "Priority_redeemable_once_per_day_60_min_" + timestamp
VOUCHER_END_DATE = end_date.strftime('%d%m%Y')
DESCRIPTION = "Description Description Description Description Description Description Description Description " \
              "Description Description Description Description Description Description Description Description " \
              "Description Description Description Description Description Description Description Description " \
              "Description Description Description Description "
RRP = "1"
DISCOUNT = "1"

SMALL_LEEDS_1 = "/fixtures/voucher_images/SmallLeeds_1.png"
SMALL_LEEDS_2 = "/fixtures/voucher_images/SmallLeeds_2.png"
SMALL_LEEDS_3 = "/fixtures/voucher_images/SmallLeeds_3.png"
SMALL_LEEDS_4 = "/fixtures/voucher_images/SmallLeeds_4.png"

MEDIUM_LEEDS_1 = "/fixtures/voucher_images/MediumLeeds_1.png"
MEDIUM_LEEDS_2 = "/fixtures/voucher_images/MediumLeeds_2.png"
MEDIUM_LEEDS_3 = "/fixtures/voucher_images/MediumLeeds_3.png"
MEDIUM_LEEDS_4 = "/fixtures/voucher_images/MediumLeeds_4.png"

VOUCHER_LEEDS_1 = "/fixtures/voucher_images/VoucherLeeds_1.png"
VOUCHER_LEEDS_2 = "/fixtures/voucher_images/VoucherLeeds_2.png"
VOUCHER_LEEDS_3 = "/fixtures/voucher_images/VoucherLeeds_3.png"
VOUCHER_LEEDS_4 = "/fixtures/voucher_images/VoucherLeeds_4.png"

BACKGROUND_LEEDS_1 = "/fixtures/voucher_images/BackgroundLeeds_1.png"
BACKGROUND_LEEDS_2 = "/fixtures/voucher_images/BackgroundLeeds_2.png"
BACKGROUND_LEEDS_3 = "/fixtures/voucher_images/BackgroundLeeds_3.png"
BACKGROUND_LEEDS_4 = "/fixtures/voucher_images/BackgroundLeeds_4.png"

# Scan&Win => Create competition prize page

COMPETITION_PRIZE_NAME = "Competition_prize_" + timestamp
COMPETITION_PRIZE_END_DATE = end_date.strftime('%d%m%Y')
COMPETITION_PRIZE_NUMBER_OF_WINNERS = "5"
COMPETITION_PRIZE_WIN_RATE = "10"
COMPETITION_PRIZE_WIN_MESSAGE = "Congratulation! You're won!"
COMPETITION_PRIZE_LOSS_TITLE = "Loss Test Title"
COMPETITION_PRIZE_LOSS_MESSAGE = "Sorry :( But you didn't win"
WIN_CONGRATULATION_IMAGE = "/fixtures/scan_and_win/win_congratulation.png"
WIN_COMPETITION_IMAGE = "/fixtures/scan_and_win/win_competition.png"
LOSS_IMAGE = "/fixtures/scan_and_win/loss.png"
LOSS_BACKGROUND_IMAGE = "/fixtures/scan_and_win/loss_background.png"
DRAW_ENTRY_IMAGE = "/fixtures/scan_and_win/draw_entry.png"
DRAW_PRIZE_IMAGE = "/fixtures/scan_and_win/draw_prize.png"

# Scan&Win => Create draw prize page

DRAW_PRIZE_NAME = "Draw_prize_" + timestamp
DRAW_PRIZE_END_DATE = end_date.strftime('%d%m%Y')

# Create credit voucher page

CREDIT_VOUCHER_NAME = "Credit_voucher_" + timestamp

# Create AR Campaign page

CAMPAIGN_NAME = "Campaign_" + timestamp
CAMPAIGN_START_DATE = datetime.now().strftime('%d%m%Y')
CAMPAIGN_END_DATE = end_date.strftime('%d%m%Y')

# Create vouchers report page

VOUCHERS_REPORT_NAME = "Vouchers_report_" + timestamp

# Create consumer app report page

CONSUMER_APP_REPORT_NAME = "Consumer_app_" + timestamp

# Create In-store signage campaign page

IN_STORE_SIGNAGE_CAMPAIGN_NAME = "In_store_campaign_" + timestamp

# Create In-store signage advert page

IN_STORE_SIGNAGE_ADVERT_NAME = "In_store_advert_" + timestamp
IN_STORE_SIGNAGE_ADVERT_END_DATE = end_date.strftime('%d%m%Y')
IN_STORE_SIGNAGE_ADVERT_HALF_SCREEN_VIDEO = "/fixtures/in_store_signage_advert_video/Half_screen_video.mp4"
IN_STORE_SIGNAGE_ADVERT_FULL_SCREEN_VIDEO = "/fixtures/in_store_signage_advert_video/Full_screen_video.mp4"

# Create In-store signage report page

IN_STORE_SIGNAGE_REPORT_NAME = "In-store_signage_" + timestamp

# Create system advertiser vouchers report page

CREDIT_VOUCHERS_REPORT_NAME = "Credit_voucher_report_" + timestamp
LOYALTY_VOUCHER_REPORT_NAME = "Loyalty_voucher_report_" + timestamp

# Retailer dashboard => Create voucher page

MIN_ORDER_PRICE = "1"
VOUCHER_START_DATE = datetime.now().strftime('%d%m%Y')
VOUCHER_CODE = str(random.randrange(9999999))

# Retailer dashboard => Buy Jisp Materials pageQUANTITY_MATERIALS

QUANTITY_MATERIALS = "1"

# Retailer dashboard => Billing methods page

BILLING_METHOD_CARD_NUMBER = "4111111111111111"
BILLING_METHOD_EXPIRATION_DATE = "11/33"

# Retailer dashboard => Offers

OFFER_START_DATE = datetime.now().strftime('%d%m%Y')
OFFER_END_DATE = end_date.strftime('%d%m%Y')
OFFER_PRICE = "1"
OFFER_TITLE = "Discover the Choice 4 Beef Quarter Pounders"

# Retailer dashboard => Bundle

BUNDLE_NAME = "Test_Bundle_" + timestamp
BUNDLE_PRICE = 2
BUNDLE_COMPONENT_NAME = "Test_Bundle_Component"

# Retailer dashboard => Add Printer page

PRINTER_PATH = str(random.randrange(999999999))
PRINTER_ACCESS_ID = str(random.randrange(999999999))

# Retailer dashboard => Campaign

CAMPAIGN_PRODUCT_NAME = "Test_Product_Name"
CAMPAIGN_ORIGINAL_PRICE = 1
CAMPAIGN_OFFER_PRICE = 0.1
CAMPAIGN_EVENT_START_DATE = datetime.now().strftime('%d%m%Y')
CAMPAIGN_EVENT_END_DATE = end_date.strftime('%d%m%Y')

# Retailer dashboard => Edit profile

RETAILER_FIRST_NAME = "Retailer_Test_First_Name"
RETAILER_LAST_NAME = "Retailer_Test_Last_Name"
RETAILER_CONTACT_PHONE = "1111" + str(random.randrange(9999))
RETAILER_WHATSAPP_NUMBER = "11110101"
VERIFY_NUMBER_CODE = "1111"
RETAILER_SORT_CODE = "23456456"
RETAILER_BANK_ACCOUNT_NUMBER = "3456789765"
RETAILER_BANK_ACCOUNT_NAME = "Account Name"
RETAILER_VAT_NUMBER = "12345678"
RETAILER_COMPANY_REGISTRATION_NUMBER = "Company registration number"
RETAILER_BUSINESS_DESCRIPTION = "Description Description Description Description Description Description "
RETAILER_ADDRESS_LINE_1 = "London"
RETAILER_ADDRESS_LINE_2 = "London"
RETAILER_TOWN = "London"
RETAILER_POSTCODE = "SE17"
RETAILER_STORE_CONTACT_NUMBER = "11110101"
RETAILER_WEBSITE = "www.website.com"

TEXT_SELECTION = (Keys.COMMAND + "A")
# TEXT_SELECTION = (Keys.CONTROL + "A")
# Windows == (Keys.CONTROL + "A")
# macOS == (Keys.COMMAND + "A")

START_DATE = datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%fZ')
END_DATE = end_date.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
